<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\EshopBundle\Entity\Currency;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Query\Expr\Base;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AbstractEntityController extends AbstractListController
{
    /**
     * @var Entity
     */
    protected $entity;
    /**
     * @param Request $request
     * @param ValidatorInterface $validator
     * @Rest\Post("", name="create")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function save(Request $request, ValidatorInterface $validator)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, 'Empty body');
        }
        $this->entity->load($data);
        $valid = BaseController::validate($this->entity, $validator);
        if (!empty($valid))
            return $valid;
        $this->em->persist($this->entity);
        $this->em->flush();
        return $this->entity;
    }
}
