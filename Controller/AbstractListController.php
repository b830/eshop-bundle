<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


abstract class AbstractListController extends AbstractFOSRestController
{

    /**
     * @var ServiceEntityRepository
     */
    protected $repository;

    /**
     * @Rest\Get("", name="list")
     * @Rest\View(serializerGroups={"list"})
     */
    public function list()
    {
        return $this->repository->findBy($this->getListCriteria(), $this->getListOrdering());
    }

    /**
     * @Rest\Get("/{id}", name="get")
     * @Rest\View(serializerGroups={"detail"})
     * @param int $id
     */
    public function getEntity(int $id)
    {
        $entity = $this->repository->find($id);
        if (!$entity) {
            ErrorMessages::message(ErrorMessages::NOT_FOUND);
        }

        return $entity;
    }

    protected function getListCriteria()
    {
        return [];
    }

    protected function getListOrdering()
    {
        return [];
    }
}
