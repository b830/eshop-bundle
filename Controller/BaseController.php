<?php

namespace App\Akip\EshopBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use phpDocumentor\Reflection\Element;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BaseController extends AbstractFOSRestController
{
    const JSON_DATE_FORMAT = 'Y-m-d\TH:i:s.v\Z';

    protected $filter = [];
    protected $order = [];

    public function __construct()
    {

    }

    /**
     * @param ParamFetcherInterface $pf
     * @param $paramName
     * @return \DateTime|null
     */
    protected function getDateTimeFromParam($data)
    {
        $date = \DateTime::createFromFormat(self::JSON_DATE_FORMAT, $data);
        if ($date === false) {
            return null;
        }
        return $date;
    }

    public function checkLocale($locales)
    {
        $existLocales = explode("|", $_ENV['LOCALES']);
        foreach ($locales as $locale) {
            if (!in_array($locale, $existLocales)) {
                throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, "Locale [{$locale}] is not allowed. Allowed locales are - [{$_ENV['LOCALES']}]");
            }
        }
    }

    public function getLocalesList()
    {
        return $existLocales = explode("|", $_ENV['LOCALES']);
    }

    public function getDefaultLocale()
    {
        return $_ENV['DEFAULT_LOCALE'];
    }

    public function normalize ($string) {
        $table = array(
            'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
            'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
            'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
            'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
            'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r',
        );

        return strtr($string, $table);
    }

    public function slugGeneratetor($slug){
        $slug = explode(" ", $slug);
        $slug = join("-", $slug);
        $slug = strtolower($this->normalize($slug));
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '', $slug);
        return $slug;
    }

    static function validate($entity, ValidatorInterface $validator)
    {
        $errors = $validator->validate($entity);
        if (count($errors)) {

            $errorMessages = [];
            /**
             * @var $e ConstraintViolation
             */
            foreach ($errors as $e) {
                $errorMessages[$e->getPropertyPath()][] = $e->getMessage();
            }

            return (new JsonResponse($errorMessages, 422));
        }
    }

    protected function loadTranslationsFromArray($data, $translation, $validator)
    {
        $translationArray = array();
        if (isset($data)) {
            foreach ($data as $key => $item) {
                $translation->load($key, $item);
                $valid = BaseController::validate($translation, $validator);
                if (!empty($valid))
                    return $valid;

                array_push($translationArray, $translation);
            }
            return $translationArray;
        }
        return [];
    }

    protected function listResponse($data, $totalResultsCount, $filteredResultsCount, $limit, $offset)
    {
        if (count($data) <= 0)
            $data = array();
        return [
            'meta' => [
                'totalCount' => $totalResultsCount,
                'filteredCount' => $filteredResultsCount,
                'limit' => $limit,
                'offset' => $offset,
            ],
            'results' => $data
        ];
    }


    protected function getOrderBy($clientOrder)
    {
        if (!$clientOrder || !is_array($clientOrder)) {
            return [];
        }

        $order = [];

        foreach ($clientOrder as $co) {
            $order[$co['column']] = strtoupper(isset($co['dir']) ? $co['dir'] : 'asc');
        }

        return $order;
    }

    protected function getFilter($name)
    {
        if (isset($this->filter[$name]) && !empty($this->filter[$name])) {
            return $this->filter[$name];
        }
        return false;
    }

}
