<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Brand;
use App\Akip\EshopBundle\Entity\BrandTranslation;
use App\Akip\EshopBundle\Entity\Country;
use App\Akip\EshopBundle\Entity\Customer;
use App\Akip\EshopBundle\Entity\CustomerAddress;
use App\Akip\EshopBundle\Repository\BrandRepository;
use App\Akip\EshopBundle\Repository\CustomerRepository;
use App\Akip\FileManagerBundle\Entity\File;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class CustomerController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/brand", name="brand_")
 */
class BrandController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * CustomerController constructor.
     * @param EntityManagerInterface $em
     * @param ValidatorInterface $validator
     */
    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->validator = $validator;
    }

    /**
     * @param ParamFetcherInterface $pf
     * @param CustomerRepository $customerRepository
     * @Rest\Get("", name="list")
     * @Rest\View(serializerGroups={"list"})
     *
     * @Rest\QueryParam(name="limit", default="10")
     * @Rest\QueryParam(name="offset", default="0")
     *
     * @Rest\QueryParam(name="search")
     * @Rest\QueryParam(name="filter", map=true)
     * @Rest\QueryParam(name="order", map=true)
     */
    public function list(ParamFetcherInterface $pf, BrandRepository $repository)
    {
        $total = count($repository->findAll());
        $this->filter = $pf->get('filter');
        $this->order = $this->getOrderBy($pf->get('order'));

        $criteria = Criteria::create();

        if ($pf->get('search')) {
            $criteria->andWhere($repository->getSearchCriteria($pf->get('search')));
        }
        // ORDER BY
        $criteria->orderBy($this->order);

        $filteredCount = $repository->matching($criteria)->count();
        // LIMIT, OFFSET
        $criteria->setFirstResult($pf->get('offset'));
        $criteria->setMaxResults($pf->get('limit'));

        $data = $repository->matching($criteria);

        $filteredCount = count($data);
        return $this->listResponse($data, $total, $filteredCount, $pf->get('limit'), $pf->get('offset'));
    }

    /**
     * @Rest\Get("/all", name="get_all")
     * @Rest\View(serializerGroups={"list"})
     * @param Brand|null $brand
     * @return Brand[]
     */
    public function getAllBrands(BrandRepository $repository)
    {
        return $repository->findAll();
    }

    /**
     * @Rest\Get("/{id}", name="get")
     * @Rest\View(serializerGroups={"detail"})
     * @param Brand|null $brand
     * @return Brand
     */
    public function getBrand(Brand $brand = null)
    {
        if (!$brand)
            ErrorMessages::message(ErrorMessages::BRAND_NOT_FOUND);
        return $brand;
    }

    /**
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @Rest\Post("", name="create")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function save(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        if (isset($data['uuid'])) {
            if (!empty($data['uuid'])) {
                $photo = $this->em->getRepository(File::class)->find($data['uuid']);
                if (!$photo)
                    ErrorMessages::message(ErrorMessages::PHOTO_NOT_FOUND);
            } else {
                $photo = null;
            }
        } else {
            $photo = null;
        }
        $brand = new Brand();
        $brand->load($data, $photo);
        $testSlug = $this->em->getRepository(Brand::class)->findOneBy(['slug' => $data['slug']]);
        if ($testSlug) {
            ErrorMessages::message(ErrorMessages::SLUG_EXISTS);
        }
        $valid = BaseController::validate($brand, $this->validator);
        if (!empty($valid)) {
            return $valid;
        }
        $this->checkLocale(array_keys($data['translations']));
        $this->em->persist($brand);
        $this->em->flush();
        foreach ($data['translations'] as $key => $translation) {
            $brandTranslation = new BrandTranslation();
            $brandTranslation->load($key, $translation, $brand);
            $valid = BaseController::validate($brandTranslation, $this->validator);
            if (!empty($valid)) {
                return $valid;
            }
            $brand->addTranslation($brandTranslation);
            $this->em->persist($brandTranslation);
        }
        $this->em->flush();
        return $brand;
    }

    /**
     * @param Request $request
     * @param PasswordEncoderInterface $passwordEncoder
     * @param Customer|null $customer
     * @Rest\Put("/{id}", name="update")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function update(Request $request, Brand $brand = null)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        if (isset($data['uuid'])) {
            if (!empty($data['uuid'])) {
                $photo = $this->em->getRepository(File::class)->find($data['uuid']);
                if (!$photo)
                    ErrorMessages::message(ErrorMessages::PHOTO_NOT_FOUND);
            } else {
                $photo = null;
            }
        } else {
            $photo = null;
        }
        $brand->load($data, $photo);
        $testSlug = $this->em->getRepository(Brand::class)->findOneBy(['slug' => $data['slug']]);
        if ($testSlug && ($testSlug !== $brand)) {
            ErrorMessages::message(ErrorMessages::SLUG_EXISTS);
        }
        $valid = BaseController::validate($brand, $this->validator);
        if (!empty($valid)) {
            return $valid;
        }
        $this->checkLocale(array_keys($data['translations']));
        foreach ($brand->getTranslationsObj() as $translation) {
            $brand->removeTranslation($translation);
        }
        $this->em->flush();
        foreach ($data['translations'] as $key => $translation) {
            $brandTranslation = new BrandTranslation();
            $brandTranslation->load($key, $translation, $brand);
            $valid = BaseController::validate($brandTranslation, $this->validator);
            if (!empty($valid)) {
                return $valid;
            }
            $brand->addTranslation($brandTranslation);
            $this->em->persist($brandTranslation);
        }
        $this->em->flush();
        return $brand;
    }

    /**
     * @Rest\Delete("/{id}", name="delete")
     * @Rest\View(statusCode=204)
     *
     * @param Brand|null $product
     */
    public function delete(Brand $brand = null)
    {
        $this->em->remove($brand);
        $this->em->flush();
    }
}
