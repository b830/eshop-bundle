<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\CmsBundle\Entity\Menu;
use App\Akip\CmsBundle\Entity\MenuItem;
use App\Akip\CmsBundle\Repository\MenuItemRepository;
use App\Akip\EshopBundle\Entity\Category;
use App\Akip\EshopBundle\Entity\CategoryTranslation;
use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Parameter;
use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\ProductCategory;
use App\Akip\EshopBundle\Entity\ProductVariant;
use App\Akip\EshopBundle\Repository\CategoryRepository;
use App\Akip\EshopBundle\Repository\ProductRepository;
use App\Akip\EshopBundle\Repository\ProductVariantRepository;
use App\Akip\FileManagerBundle\Entity\File;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use phpDocumentor\Reflection\DocBlock\Tags\Param;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use function Symfony\Component\DependencyInjection\Loader\Configurator\expr;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class CategoryController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/category", name="category_")
 */
class CategoryController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * CategoryController constructor.
     * @param EntityManagerInterface $em
     * @param ValidatorInterface $validator
     */
    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->validator = $validator;
    }


    /**
     * @param CategoryRepository $cr
     * @param ParamFetcherInterface $pf
     * @return Category[]
     * @Rest\Get("", name="list")
     * @Rest\View(serializerGroups={"list"})
     *
     * @Rest\QueryParam(name="limit", default="10")
     * @Rest\QueryParam(name="offset", default="0")
     *
     * @Rest\QueryParam(name="search")
     * @Rest\QueryParam(name="filter", map=true)
     * @Rest\QueryParam(name="order", map=true)
     */
    public function list(CategoryRepository $cr, ParamFetcherInterface $pf)
    {
        // ONLY MAIN categories
        $total = count($cr->findBy(['parent' => null]));

        $this->filter = $pf->get('filter');
        $this->order = $this->getOrderBy($pf->get('order'));

        $criteria = Criteria::create();

        if ($pf->get('search')) {
            $criteria->andWhere($cr->getSearchCriteria($pf->get('search')));
        }
        // FILTERS
        if ($this->getFilter('enabled')) {
            $criteria->andWhere(Criteria::expr()->eq('enabled', $this->getFilter('enabled')));
        }
        // ORDER BY
        $criteria->orderBy($this->order);

        // ONLY MAIN categories
        $criteria->andWhere(Criteria::expr()->eq('parent', null));
//        $criteria->andWhere(Criteria::expr()->eq('id', 343421));
//        $criteria->orderBy(['id' => 'ASC']);

        $filteredCount = $cr->matching($criteria)->count();

        // LIMIT, OFFSET
        $criteria->setFirstResult($pf->get('offset'));
        $criteria->setMaxResults($pf->get('limit'));
        /**
         * @var Category
         */
        $categories = $cr->findBy(['parent' => null], ['sort' => 'ASC']);
        foreach ($categories as $category) {
            $data[] = $cr->build($category, $cr->getMaxDpth($category->getMain()));
        }
        return $this->listResponse($data, $total, $filteredCount, $pf->get('offset'), $pf->get('limit'));
    }

    /**
     * @Rest\Get("/all", name="list_of_all_categories")
     * @Rest\View(serializerGroups={"list"})
     *
     * @param CategoryRepository $repository
     * @return Category[]
     */
    public function allCategories(CategoryRepository $repository)
    {
        $categories = $repository->findBy(['parent' => null], ['sort' => 'ASC']);
        foreach ($categories as $category) {
            $data[] = $repository->build($category, $repository->getMaxDpth($category->getMain()));
        }
        return $data;
    }

    /**
     * @Rest\Get("/heureka-categories", name="get_heureka_categories")
     * @Rest\View(serializerGroups={"list"})
     *
     */
    public function getHeurekaCategories(HttpClientInterface $client)
    {
        $response = $client->request('GET', 'https://www.heureka.cz/direct/xml-export/shops/heureka-sekce.xml', [
                'headers' => [
                    'Content-Type' => 'application/xml'
                ]
            ]
        );
        if ($response->getStatusCode() === 200) {
            $xml = simplexml_load_string($response->getContent());
            $json = json_encode($xml);
            $array = json_decode($json,TRUE);
            $items = [];
            array_walk_recursive($array['CATEGORY'], function ($item, $index) use (&$items) {
                if ($index === 'CATEGORY_FULLNAME') {
                    $items[] = $item;
                }
            }, $items);
            return $items;
        } else {
            ErrorMessages::message(ErrorMessages::HEUREKA_LOAD_FAIL);
        }
    }

    /**
     * @Rest\Get("/zbozi-categories", name="get_zbozi_categories")
     * @Rest\View(serializerGroups={"list"})
     *
     */
    public function getZboziCategories(HttpClientInterface $client)
    {
        $response = $client->request('GET', 'https://www.zbozi.cz/static/categories.json', [
                'headers' => [
                    'Content-Type' => 'application/json'
                ]
            ]
        );
        if ($response->getStatusCode() === 200) {
            $array = json_decode($response->getContent(),TRUE);
            $items = [];
            array_walk_recursive($array, function ($item, $index) use (&$items) {
                if ($index === 'categoryText') {
                    $items[] = $item;
                }
            }, $items);
            return $items;
        } else {
            ErrorMessages::message(ErrorMessages::GOOGLE_MERCHANTS_LOAD_FAIL);
        }
    }

    /**
     * @Rest\Get("/google-categories", name="get_google_categories")
     * @Rest\View(serializerGroups={"list"})
     *
     */
    public function getGoogleCategories(HttpClientInterface $client)
    {
        $response = $client->request('GET', 'https://www.google.com/basepages/producttype/taxonomy-with-ids.cs-CZ.txt', [
                'headers' => [
                    'Content-Type' => 'text/plain'
                ]
            ]
        );
        if ($response->getStatusCode() === 200) {
            $data = explode(' - ', $response->getContent());
            $data = preg_replace('/\\n\d+/', '', $data);
            array_shift($data);
            return $data;
        } else {
            ErrorMessages::message(ErrorMessages::ZBOZICZ_LOAD_FAIL);
        }
    }

    /**
     * @param Category $category
     * @param CategoryRepository $cr
     * @Rest\Get("/{id}", name="get")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function getCategory(CategoryRepository $cr, Category $category = null)
    {
        if (!$category)
            ErrorMessages::message(ErrorMessages::CATEGORY_NOT_FOUND);
//        return $category->getTranslations();
        $maxDepth = $cr->getMaxDpth($category->getMain());
        $data = $cr->build($category, $maxDepth);
        return $data;
    }

    /**
     * @param Request $request
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @Rest\Post("", name="create")
     * @Rest\View(serializerGroups={"detail"})
     *
     */
    public function save(Request $request, CategoryRepository $cr)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $category = new Category();
        if (isset($data['uuid'])) {
            if (!empty($data['uuid'])) {
                $photo = $this->em->getRepository(File::class)->find($data['uuid']);
                if (!$photo)
                    ErrorMessages::message(ErrorMessages::PHOTO_NOT_FOUND);
                $category->setIcon($photo);
            } else {
                $category->setIcon(null);
            }
        }
        $category = $category->load($data, $category);

        $valid = BaseController::validate($category, $this->validator);
        if (!empty($valid)) {
            return $valid;
        }


        if (isset($data['parameters']) && !empty($data['parameters'])) {
            $this->addParameters($data['parameters'], $cr, $category);
        }
        $this->checkLocale(array_keys($data['translations']));

        if (isset($data['translations'])) {
            foreach ($data['translations'] as $key => $translation) {
                $catTran = new CategoryTranslation();
                $catTran = $catTran->load($key, $translation, $category);

                $testSlug = $this->em->getRepository(CategoryTranslation::class)->findOneBy(['slug' => $translation['slug']]);
                if ($testSlug) {
                    ErrorMessages::message(ErrorMessages::SLUG_EXISTS);
                }

                $this->em->persist($category);
                $this->em->flush();

                $category->addTranslation($catTran);
                $this->em->persist($catTran);
                $this->em->flush();

            }
        }
        if (isset($data['children'])) {
            $this->read($category, $data['children'], $this->validator, $category);
        }

        /**
         * @var $cr CategoryRepository
         */
        $cr = $this->em->getRepository(Category::class);
        $cr->rebuildTree($category->getMain());

        $category->getTranslations();
        return $this->getCategory($cr, $category);
    }

    /**
     * @param Request $request
     * @param Category|null $category
     * @Rest\Put("/{id}", name="update")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function update(Request $request, CategoryRepository $cr, Category $category = null)
    {
        if (!$category)
            ErrorMessages::message(ErrorMessages::CATEGORY_NOT_FOUND);
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $this->addParameters($data['parameters'], $cr, $category);
        $this->updateCategory($data, $category);
        if (isset($data['uuid'])) {
            if (!empty($data['uuid'])) {
                $photo = $this->em->getRepository(File::class)->find($data['uuid']);
                if (!$photo)
                    ErrorMessages::message(ErrorMessages::PHOTO_NOT_FOUND);
                $category->setIcon($photo);
            } else {
                $category->setIcon(null);
            }
        }
        $this->em->flush();
        $category->getTranslations();
        $category->getTranslations()->getValues();
        return $this->getCategory($cr, $category);
    }

    public function read(Category $mainCategory, $data, ValidatorInterface $validator, $parent = null)
    {
        foreach ($data as $item) {
            $cat = new Category();
            if (isset($item['uuid'])) {
                if (!empty($data['uuid'])) {
                    $photo = $this->em->getRepository(File::class)->find($item['uuid']);
                    if (!$photo)
                        ErrorMessages::message(ErrorMessages::PHOTO_NOT_FOUND);
                    $cat->setIcon($photo);
                } else {
                    $cat->setIcon(null);
                }
            }
            $cat = $cat->load($item, $mainCategory, $parent);
            $valid = BaseController::validate($cat, $this->validator);
            if (!empty($valid))
                return $valid;
            $this->em->persist($cat);
            $this->em->flush();
            $this->checkLocale(array_keys($item['translations']));
            if (isset($data['translations'])) {
                foreach ($data['translations'] as $key => $translation) {
                    $catTran = new CategoryTranslation();
                    $catTran = $catTran->load($key, $translation, $cat);
                    $testSlug = $this->em->getRepository(CategoryTranslation::class)->findOneBy(['slug' => $translation['slug']]);
                    if ($testSlug) {
                        ErrorMessages::message(ErrorMessages::SLUG_EXISTS);
                    }
                    $this->em->persist($cat);
                    $this->em->flush();

                    $this->em->persist($catTran);
                    $this->em->flush();
                    $cat->addTranslation($catTran);
                }
            }
            if ($parent)
                $parent->addCategoryItem($cat);
            if (!empty($item['children'])) {
                $this->read($mainCategory, $item['children'], $validator, $cat);
            }
        }
    }

    private $sort = 1;

    /**
     * @Rest\Post("/update", name="update_all_categories")
     * @Rest\View(serializerGroups={"list"})
     *
     * @param Request $request
     */
    public function updateAll(Request $request, CategoryRepository $cr, ParamFetcherInterface $pf)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        foreach ($data as $item) {
            //kategorie je na 1. urovni
            $tmpCategory = $cr->find($item['id']);

            //pokud kategorie puvodne nebyla hlavni, tak ji prenastavime a nakopirujeme parametry z puvodni hlavni kategorie
            if ($tmpCategory->getMain() !== $tmpCategory) {
                $params = $tmpCategory->getMain()->getParameter();
                foreach ($params as $parameter) {
                    $tmpCategory->addParameter($parameter);
                }
                $tmpCategory->setMain($tmpCategory);
                $tmpCategory->setParent(null);
                $this->em->flush();
            }
            $tmpCategory->setSort($this->sort);
            $cr->rebuildTree($tmpCategory->getMain());
            $this->em->flush();
            if (!empty($item['children'])) {
                $this->recurseRead($tmpCategory, $item['children'], $tmpCategory);
                $cr->rebuildTree($tmpCategory->getMain());
                $this->em->flush();
            }
            $this->sort++;
        }
        $this->em->flush();
    }

    private function recurseRead($mainCategory, $data, $parent = null)
    {
        foreach ($data as $item) {
            $this->sort++;
            $cat = $this->getDoctrine()->getRepository(Category::class)->find($item['id']);
            if ($cat->getMain() === $cat) {
                foreach ($cat->getParameter() as $parameter) {
                    $cat->removeParameter($parameter);
                    $this->em->flush();
                }
            }
            $cat->setMain($mainCategory);
            $cat->setParent($parent);
            $cat->setSort($this->sort);
            $main[] = $mainCategory;
            $this->em->flush();
            if (!empty($item['children'])) {
                $this->recurseRead($mainCategory, $item['children'], $cat);
            }
        }
    }

    private function updateCategory($data, $category)
    {
//        $category->load($data, $category->getMain());

        $valid = BaseController::validate($category, $this->validator);
        if (!empty($valid))
            return $valid;
        /** @var Category $category */
        $category->setName($data['name']);
        $category->setHeurekaCategory($data['heurekaCategory']);
        $category->setGoogleCategory($data['googleCategory']);
        $category->setZboziCategory($data['zboziCategory']);
        $this->checkLocale(array_keys($data['translations']));

        foreach ($data['translations'] as $key => $translation) {
            $catTran = new CategoryTranslation();
            $catTran = $catTran->load($key, $translation, $category);
            $translationArray[] = $catTran;
        }

        foreach ($category->getTranslations() as $translation) {
            $category->removeTranslation($translation);
            $this->em->flush();
        }


        foreach ($translationArray as $catTran) {
            $valid = BaseController::validate($catTran, $this->validator);
            if (!empty($valid)) {
                throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, $valid->getContent());
            }
            $this->em->persist($catTran);
            $category->addTranslation($catTran);
            $this->em->flush();
        }
    }

    /**
     * @param Category $category
     * @Rest\Delete("/{id}", name="delete")
     * * @Rest\View(serializerGroups={"list", "detail"})
     */
    public function delete(Category $category)
    {
        if (!$category)
            ErrorMessages::message(ErrorMessages::CATEGORY_NOT_FOUND);
//        return 'xx';
        foreach ($category->getProductCategory() as $productCategory) {
            if ($productCategory->getIsMain()) {
                ErrorMessages::message(ErrorMessages::MAIN_CATEGORY_DELETE);
            }
        }
//        if ($category->getProductCategory()) {
//            throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, 'Category has product - cannot be deleted');
//        }
//        foreach ($category->getProductCategory() as $productCategory) {
//            if (!$productCategory->getIsMain()) {
//                $this->em->remove($productCategory);
//            }
//        }
        $this->em->remove($category);
        $this->em->flush();
    }

    private function addParameters($data, CategoryRepository $cr, Category $category = null)
    {

        //pridavani parametru vzdy do MAIN categorie (prvni ve strome)
        if ($category !== $category->getMain())
            $category = $cr->find($category->getMain()->getId());
        foreach ($category->getParameter() as $parameter) {
            $category->removeParameter($parameter);
            $this->em->flush();
        }
        if (is_array($data)) {
            foreach ($data as $item) {
                $param = $this->getDoctrine()->getRepository(Parameter::class)->find($item['id']);

                if (!$param)
                    ErrorMessages::message(ErrorMessages::CATEGORY_NOT_FOUND);
                $category->addParameter($param);
                $this->em->flush();
            }
        } else {
            $param = $this->getDoctrine()->getRepository(Parameter::class)->find($data);
            $category->addParameter($param);
            $this->em->flush();
        }
    }

    /**
     * @param $paramId int
     * @param CategoryRepository $cr
     * @param Category|null $category
     * @return void
     * @Rest\View(StatusCode = 204)
     * @Rest\Delete("/{id}/parameters/{paramId}", name="parameter_delete")
     */
    public function deleteParameters($paramId, CategoryRepository $cr, Category $category = null)
    {
        if (!$category)
            ErrorMessages::message(ErrorMessages::CATEGORY_NOT_FOUND);

        if ($category !== $category->getMain())
            $category = $cr->find($category->getMain()->getId());

        $param = $this->getDoctrine()->getRepository(Parameter::class)->find($paramId);

        if (!$param)
            ErrorMessages::message(ErrorMessages::PARAMETER_NOT_FOUND);
        else {
            $category->removeParameter($param);
            $this->em->flush();
        }

    }


    /**
     * @Rest\Get("/{id}/products", name="product_list")
     * @Rest\View(serializerGroups={"list"})
     * @param ProductVariantRepository $pvr
     * @param ParamFetcherInterface $pf
     * @param Category $category
     *
     * @return array
     * @Rest\QueryParam(name="limit", default="10")
     * @Rest\QueryParam(name="offset", default="0")
     *
     * @Rest\QueryParam(name="search")
     * @Rest\QueryParam(name="filter", map=true)
     * @Rest\QueryParam(name="order", map=true)
     */
    public function productList(ProductVariantRepository $pvr, ParamFetcherInterface $pf, Category $category = null)
    {
        //pouze hlavni varianty v kategorii
        if (!$category)
            ErrorMessages::message(ErrorMessages::CATEGORY_NOT_FOUND);

        $total = count($pvr->findBy(['product' => $category->getProductCategory()->getValues(), 'main' => true]));

        $this->filter = $pf->get('filter');
        $this->order = $this->getOrderBy($pf->get('order'));

        $criteria = Criteria::create();
        //TODO: opravit vyhledavani
        if ($pf->get('search')) {
            $criteria->andWhere($pvr->getSearchCriteria($pf->get('search')));
        }

        // ORDER BY
//        $criteria->orderBy($this->order);
//        exit(print_r($criteria));

//        var_dump($criteria);
//        die();
//        ONLY MAIN VARIANTS, VARIANTS IN CURRENT CATEGORY
        $criteria->andWhere(Criteria::expr()->eq('main', true));
        $criteria->andWhere(Criteria::expr()->in('product', $category->getProductCategory()->getValues()));

        $filteredCount = $pvr->matching($criteria)->count();

        // LIMIT, OFFSET
        $criteria->setFirstResult($pf->get('offset'));
        $criteria->setMaxResults($pf->get('limit'));


        $data = $pvr->matching($criteria);

        // nacteni dat
        $data->first();

//        return $data->count();
        return $this->listResponse($data, $total, $filteredCount, $pf->get('limit'), $pf->get('offset'));
    }
}
