<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\EshopBundle\Repository\CountryRepository;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * @Rest\Route("/api/country", name="country_")
*/
class CountryController extends AbstractListController
{
    /**
     * CountryController constructor.
     * @param CountryRepository $countryRepository
     */
    public function __construct(CountryRepository $countryRepository)
    {
        $this->repository = $countryRepository;
    }
}
