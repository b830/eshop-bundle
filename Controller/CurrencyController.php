<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\EshopBundle\Entity\Currency;
use App\Akip\EshopBundle\Repository\CurrencyRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class CurrencyController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/currency", name="currency_")
 */
class CurrencyController extends AbstractEntityController
{
    /**
     * @var EntityManagerInterface
     */
    public $em;

    /**
     * CurrencyController constructor.
     * @param CurrencyRepository $currencyRepository
     * @param EntityManagerInterface $em
     */
    public function __construct(CurrencyRepository $currencyRepository, EntityManagerInterface $em)
    {
        $this->repository = $currencyRepository;
        $this->em = $em;
        $this->entity = new Currency();
    }

}
