<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Country;
use App\Akip\EshopBundle\Entity\Customer;
use App\Akip\EshopBundle\Entity\CustomerAddress;
use App\Akip\EshopBundle\Repository\CustomerRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use function Sodium\add;

/**
 * Class CustomerAddressController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/customer", name="customer_address_")
 */
class CustomerAddressController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * CustomerController constructor.
     * @param EntityManagerInterface $em
     * @param ValidatorInterface $validator
     */
    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->validator = $validator;
    }

    /**
     * @Rest\Get("/{id}/address", name="list")
     * @Rest\View(serializerGroups={"list"})
     * @param Customer|null $customer
     */
    public function list(Customer $customer = null)
    {
        if (!$customer)
            ErrorMessages::message(ErrorMessages::CUSTOMER_NOT_FOUND);
        return $customer->getAddresses();
    }

    /**
     * @param $customerId
     * @param $customerAddressId
     * @Rest\Get("/{customerId}/address/{customerAddressId}", name="get")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function getAddress($customerId, $customerAddressId)
    {
        $address = $this->getDoctrine()->getRepository(CustomerAddress::class)->findOneBy(['customer' => $customerId, 'id' => $customerAddressId]);
        if (!$address)
            ErrorMessages::message(ErrorMessages::NOT_FOUND);
        return $address;
    }

    /**
     * @param $customerId
     * @param $customerAddressId
     * @Rest\Get("/customer/address", name="get")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function getCurrenctCustomerAddress()
    {

//        if (!$address)
//            ErrorMessages::message(ErrorMessages::NOT_FOUND);
//        return $address;
    }


    /**
     * @param Request $request
     * @param Customer|null $customer
     * @Rest\Post("/{id}/address", name="create")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function save(Request $request, Customer $customer = null)
    {
        if (!$customer)
            ErrorMessages::message(ErrorMessages::CUSTOMER_NOT_FOUND);
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $customerAddress = new CustomerAddress();
        $customerAddress->setCustomer($customer);
        if ($data['country']) {
            $country = $this->getDoctrine()
                ->getRepository(Country::class)
                ->find($data['country']);
            if (!$country)
                ErrorMessages::message(ErrorMessages::COUNTRY_NOT_FOUND);
            $customerAddress->setCountry($country);
        }
        $customerAddress->load($data);
        $valid = BaseController::validate($customer, $this->validator);
        if (!empty($valid))
            return $valid;
        $this->em->persist($customerAddress);
        $this->em->flush();
        return $customerAddress;
    }

    /**
     * @param Request $request
     * @param $customerId
     * @param $customerAddressId
     * @return string
     * @Rest\Put("/{customerId}/address/{customerAddressId}", name="update")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function update(Request $request, $customerId, $customerAddressId)
    {
        $address = $this->getAddress($customerId, $customerAddressId);
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        if ($data['country']) {
            $country = $this->getDoctrine()
                ->getRepository(Country::class)
                ->find($data['country']);
            if (!$country)
                ErrorMessages::message(ErrorMessages::COUNTRY_NOT_FOUND);
            $address->setCountry($country);
        }
        $address->load($data);
        $valid = BaseController::validate($address, $this->validator);
        if (!empty($valid))
            return $valid;
        $this->em->persist($address);
        $this->em->flush();
        return $address;
    }

    /**
     * @param $customerId
     * @param $customerAddressId
     * @Rest\Delete("/{customerId}/address/{customerAddressId}", name="delete")
     * @Rest\View(StatusCode = 204)
     */
    public function delete($customerId, $customerAddressId)
    {
        $address = $this->getAddress($customerId, $customerAddressId);
        $this->em->remove($address);
        $this->em->flush();
    }
}
