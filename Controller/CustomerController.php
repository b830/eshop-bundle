<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Country;
use App\Akip\EshopBundle\Entity\Customer;
use App\Akip\EshopBundle\Entity\CustomerAddress;
use App\Akip\EshopBundle\Repository\CustomerRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class CustomerController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/customer", name="customer_")
 */
class CustomerController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * CustomerController constructor.
     * @param EntityManagerInterface $em
     * @param ValidatorInterface $validator
     */
    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->validator = $validator;
    }

    /**
     * @param ParamFetcherInterface $pf
     * @param CustomerRepository $customerRepository
     * @Rest\Get("", name="list")
     * @Rest\View(serializerGroups={"list"})
     *
     * @Rest\QueryParam(name="limit", default="10")
     * @Rest\QueryParam(name="offset", default="0")
     *
     * @Rest\QueryParam(name="search")
     * @Rest\QueryParam(name="filter", map=true)
     * @Rest\QueryParam(name="order", map=true)
     */
    public function list(ParamFetcherInterface $pf, CustomerRepository $customerRepository)
    {
        $total = count($customerRepository->findAll());
        $this->filter = $pf->get('filter');
        $this->order = $this->getOrderBy($pf->get('order'));

        $criteria = Criteria::create();

        if ($pf->get('search')) {
            $criteria->andWhere($customerRepository->getSearchCriteria($pf->get('search')));
        }
        // FILTERS
        if ($this->getFilter('is_company')) {
            $criteria->andWhere(Criteria::expr()->eq('is_company', $this->getFilter('is_company')));
        }
        // ORDER BY
        $criteria->orderBy($this->order);

        $filteredCount = $customerRepository->matching($criteria)->count();
        // LIMIT, OFFSET
        $criteria->setFirstResult($pf->get('offset'));
        $criteria->setMaxResults($pf->get('limit'));

        $data = $customerRepository->matching($criteria);

        $filteredCount = count($data);
        return $this->listResponse($data, $total, $filteredCount, $pf->get('limit'), $pf->get('offset'));
    }

    /**
     * @Rest\Get("/{id}", name="get")
     * @Rest\View(serializerGroups={"detail"})
     * @param Customer|null $customer
     * @return Customer
     */
    public function getCustomer(Customer $customer = null)
    {
        if (!$customer)
            ErrorMessages::message(ErrorMessages::CUSTOMER_NOT_FOUND);
        return $customer;
    }

    /**
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @Rest\Post("", name="register")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function save(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $customer = new Customer();
        $customer->load($data);
        if (isset($data['password'])) {
            $customer->setPassword(
                $passwordEncoder->encodePassword($customer, $data['password'])
            );
        } else{
            $customer->setPassword(
                $passwordEncoder->encodePassword($customer, random_bytes(6))
            );
        }
        if ($data['country']) {
            $country = $this->getDoctrine()
                ->getRepository(Country::class)
                ->find($data['country']);
            if (!$country)
                ErrorMessages::message(ErrorMessages::COUNTRY_NOT_FOUND);
            $customer->setCountry($country);
        }
        $valid = BaseController::validate($customer, $this->validator);
        if (!empty($valid))
            return $valid;
        $this->em->persist($customer);
        $this->em->flush();
        return $customer;
    }

    /**
     * @Rest\Route("/api/customer/logout", name="customer_logout", methods={"GET"})
     */
    public function logout(Request $request)
    {
//        $cookie = new \Symfony\Component\HttpFoundation\Cookie('Bearer', 'xxxx', '1010-05-05', '/', null, null);
        $response = new Response();
        $response->headers->setCookie(new \Symfony\Component\HttpFoundation\Cookie('Bearer', 'xxxx', '1010-05-05', '/', null, null));
        $response->headers->setCookie(new \Symfony\Component\HttpFoundation\Cookie('RefreshToken', 'xxxx', '1010-05-05', '/', null, null));
//        $response->headers->setCookie(new \Symfony\Component\HttpFoundation\Cookie('PHPSESSID', 'xxxx', '1010-05-05', '/', null, null));
        return $response;
    }

    /**
     * @param Request $request
     * @param PasswordEncoderInterface $passwordEncoder
     * @param Customer|null $customer
     * @Rest\Put("/{id}", name="update")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function update(Request $request, UserPasswordEncoderInterface $passwordEncoder, Customer $customer = null)
    {
        if (!$customer)
            ErrorMessages::message(ErrorMessages::CUSTOMER_NOT_FOUND);
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $customer->load($data);
        if (isset($data['password'])) {
            $customer->setPassword(
                $passwordEncoder->encodePassword($customer, $data['password'])
            );
        }
        if ($data['country']) {
            $country = $this->getDoctrine()
                ->getRepository(Country::class)
                ->find($data['country']);
            if (!$country)
                ErrorMessages::message(ErrorMessages::COUNTRY_NOT_FOUND);
            $customer->setCountry($country);
        }
        $valid = BaseController::validate($customer, $this->validator);
        if (!empty($valid))
            return $valid;
        $this->em->persist($customer);
        $this->em->flush();
        return $customer;
    }

}
