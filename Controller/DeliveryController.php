<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Brand;
use App\Akip\EshopBundle\Entity\BrandTranslation;
use App\Akip\EshopBundle\Entity\Country;
use App\Akip\EshopBundle\Entity\Customer;
use App\Akip\EshopBundle\Entity\CustomerAddress;
use App\Akip\EshopBundle\Entity\Delivery;
use App\Akip\EshopBundle\Entity\DeliveryPrice;
use App\Akip\EshopBundle\Entity\Payment;
use App\Akip\EshopBundle\Entity\Vat;
use App\Akip\EshopBundle\Repository\BrandRepository;
use App\Akip\EshopBundle\Repository\CustomerRepository;
use App\Akip\EshopBundle\Repository\DeliveryRepository;
use App\Akip\EshopBundle\Repository\PaymentRepository;
use App\Akip\FileManagerBundle\Entity\File;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class CustomerController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/delivery", name="delivery_")
 */
class DeliveryController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * CustomerController constructor.
     * @param EntityManagerInterface $em
     * @param ValidatorInterface $validator
     */
    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->validator = $validator;
    }

    /**
     * @param ParamFetcherInterface $pf
     * @param PaymentRepository $repository
     * @return array
     * @Rest\Get("", name="list")
     * @Rest\View(serializerGroups={"list"})
     *
     * @Rest\QueryParam(name="limit", default="10")
     * @Rest\QueryParam(name="offset", default="0")
     *
     * @Rest\QueryParam(name="search")
     * @Rest\QueryParam(name="filter", map=true)
     * @Rest\QueryParam(name="order", map=true)
     */
    public function list(ParamFetcherInterface $pf, DeliveryRepository $repository)
    {
        $total = count($repository->findAll());
        $this->filter = $pf->get('filter');
        $this->order = $this->getOrderBy($pf->get('order'));

        $criteria = Criteria::create();

        if ($pf->get('search')) {
            $criteria->andWhere($repository->getSearchCriteria($pf->get('search')));
        }
        // ORDER BY
        $criteria->orderBy($this->order);

        $filteredCount = $repository->matching($criteria)->count();
        // LIMIT, OFFSET
        $criteria->setFirstResult($pf->get('offset'));
        $criteria->setMaxResults($pf->get('limit'));

        $data = $repository->matching($criteria);

        $filteredCount = count($data);
        return $this->listResponse($data, $total, $filteredCount, $pf->get('limit'), $pf->get('offset'));
    }

    /**
     * @Rest\Get("/all", name="get_all")
     * @Rest\View(serializerGroups={"list"})
     * @param DeliveryRepository $repository
     * @return \App\Akip\EshopBundle\Entity\Delivery[]
     */
    public function getAllDeliveries(DeliveryRepository $repository)
    {
        return $repository->findAll();
    }

    /**
     * @Rest\Get("/{id}", name="get")
     * @Rest\View(serializerGroups={"detail", "deliveryDetail"})
     * @param Delivery|null $delivery
     * @return Delivery|null
     */
    public function getDelivery(Delivery $delivery = null)
    {
        if (!$delivery)
            ErrorMessages::message(ErrorMessages::DELIVERY_NOT_FOUND);
        $delivery->checkOptions();
        $this->em->flush();
        return $delivery;
    }

    /**
     * @Rest\Put("/{id}", name="update")
     * @Rest\View(serializerGroups={"detail", "deliveryDetail"})
     * @param Delivery|null $delivery
     */
    public function update(Request $request, Delivery $delivery = null)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $delivery->load($data);
        $testSlug = $this->em->getRepository(Delivery::class)->findOneBy(['slug' => $data['slug']]);
        if ($testSlug && ($testSlug !== $delivery)) {
            ErrorMessages::message(ErrorMessages::SLUG_EXISTS);
        }
        $valid = BaseController::validate($delivery, $this->validator);
        if (!empty($valid)) {
            return $valid;
        }
        $payments = [];
        foreach ($data['payments'] as $paymentId) {
            $payment = $this->em->getRepository(Payment::class)->find($paymentId);
            $payments[] = $payment;
        }
        foreach ($delivery->getPayments() as $payment) {
            $payment->removeDelivery($delivery);
            $delivery->removePayment($payment);
        }
        $this->em->flush();
        foreach ($payments as $payment) {
            /** @var Payment $payment */
            if (!$payment->getDeliveries()->contains($delivery)) {
                $payment->addDelivery($delivery);
            }
            if (!$delivery->getPayments()->contains($payment)) {
                $delivery->addPayment($payment);
            }
        }
        foreach ($delivery->getDeliveryPrices() as $index => $deliveryPrice) {
            if (count($data['deliveryPrices']) < 1) {
                $delivery->removeDeliveryPrice($deliveryPrice);
                $this->em->remove($deliveryPrice);
            }
            if (array_search($deliveryPrice->getId(), array_column($data['deliveryPrices'], 'id')) === false) {
                $delivery->removeDeliveryPrice($deliveryPrice);
                $this->em->remove($deliveryPrice);
            }
        }
        foreach ($data['deliveryPrices'] as $deliveryPriceData) {
            if (isset($deliveryPriceData['id'])) {
                $deliveryPrice = $this->em->getRepository(DeliveryPrice::class)->find($deliveryPriceData['id']);
            } else {
                $deliveryPrice = new DeliveryPrice();
                $delivery->addDeliveryPrice($deliveryPrice);
            }
            /** @var Vat $vat */
            $vat = $this->em->getRepository(Vat::class)->find($deliveryPriceData['vatId']);
            $deliveryPrice->load($deliveryPriceData, $vat);
        }
        $this->em->persist($delivery);
        $this->em->flush();
        return $delivery;
    }

    /**
     * @Rest\Delete("/{id}", name="delete")
     * @Rest\View(statusCode=204)
     *
     * @param Delivery|null $delivery
     */
    public function delete(Delivery $delivery = null)
    {
        $this->em->remove($delivery);
        $this->em->flush();
    }
}
