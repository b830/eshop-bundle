<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\EshopBundle\Entity\Flag;
use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Repository\FlagRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class FlagController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/flag", name="flag_")
 */
class FlagController extends AbstractEntityController
{
    /**
     * @var EntityManagerInterface
     */
    public $em;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * CountryController constructor.
     * @param FlagRepository $flagRepository
     * @param EntityManagerInterface $em
     * @param ValidatorInterface $validator
     */
    public function __construct(FlagRepository $flagRepository, EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $this->repository = $flagRepository;
        $this->entity = new Flag();
        $this->em = $em;
        $this->validator = $validator;
    }

}
