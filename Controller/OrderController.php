<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Country;
use App\Akip\EshopBundle\Entity\Customer;
use App\Akip\EshopBundle\Entity\Order;
use App\Akip\EshopBundle\Entity\Flag;
use App\Akip\EshopBundle\Entity\OrderPaymentLog;
use App\Akip\EshopBundle\Entity\OrderProduct;
use App\Akip\EshopBundle\Entity\OrderStatus;
use App\Akip\EshopBundle\Entity\ProductVariant;
use App\Akip\EshopBundle\Entity\Vat;
use App\Akip\EshopBundle\Repository\OrderRepository;
use App\Akip\MailerBundle\Services\Mailer;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use phpDocumentor\Reflection\Element;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use function Symfony\Component\String\u;

/**
 * Class OrderController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/order", name="order_")
 */
class OrderController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * OrderController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * @Rest\Get("", name="list")
     * @Rest\View(serializerGroups={"list"})
     *
     * @Rest\QueryParam(name="limit", default="10")
     * @Rest\QueryParam(name="offset", default="0")
     *
     * @Rest\QueryParam(name="search")
     * @Rest\QueryParam(name="filter", map=true)
     * @Rest\QueryParam(name="order", map=true)
     *
     * @param OrderRepository $repository
     * @param ParamFetcherInterface $pf
     * @return array
     */
    public function list(OrderRepository $repository, ParamFetcherInterface $pf)
    {
        $total = count($repository->findAll());
        $this->filter = $pf->get('filter');
        $this->order = $this->getOrderBy($pf->get('order'));

        $criteria = Criteria::create();

        if ($pf->get('search')) {
            $criteria->andWhere($repository->getSearchCriteria($pf->get('search')));
        }
        // FILTERS
        if ($this->getFilter('orderStatus')) {
            $orderStatus = $this->em->getRepository(OrderStatus::class)->find($this->getFilter('orderStatus'));
            $criteria->andWhere(Criteria::expr()->eq('orderStatus', $orderStatus));
        }
        // FILTERS
        if ($this->getFilter('paymentDate') && $this->getFilter('paymentDate') == true) {
            $criteria->andWhere(Criteria::expr()->neq('paymentDate', null));
        }

        // ORDER BY
        $criteria->orderBy($this->order);

        $filteredCount = $repository->matching($criteria)->count();

        // LIMIT, OFFSET
        $criteria->setFirstResult($pf->get('offset'));
        $criteria->setMaxResults($pf->get('limit'));

        $data = $repository->matching($criteria);

        $filteredCount = count($data);
        return $this->listResponse($data, $total, $filteredCount, $pf->get('offset'), $pf->get('limit'));

    }

    /**
     * @Rest\Get("/orders-count", name="orders_count")
     * @Rest\View(serializerGroups={"list"})
     */
    public function getOrdersCount(OrderRepository $repository): array
    {
        $orderStatuses = $this->em->getRepository(OrderStatus::class)->findAll();
        $data = [];
        foreach ($orderStatuses as $orderStatus) {
            $orders = $repository->findBy(['orderStatus' => $orderStatus]);
            $data[$orderStatus->getSlug()] = count($orders);
        }
        $criteria = Criteria::create();
        $criteria->andWhere(Criteria::expr()->neq('paymentDate', null));
        $data['paid'] = $repository->matching($criteria)->count();
        return $data;
    }

    /**
     * @Rest\Get("/{id}", name="get")
     * @Rest\View(serializerGroups={"orderDetail", "detail"})
     *
     * @param Order $order
     * @return Order
     */
    public function getOrder(Order $order)
    {
        if (!$order)
            ErrorMessages::message(ErrorMessages::ORDER_NOT_FOUND);
        return $order;
    }

    /**
     * @Rest\Put("/{id}", name="update")
     * @Rest\View(serializerGroups={"orderDetail"})
     *
     * @param Request $request
     * @param Order $order
     * @param Mailer $mailer
     * @return Order|JsonResponse
     */
    public function update(Request $request, Order $order, Mailer $mailer)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $order->load($data);
        
        /** @var OrderStatus $newOrderStatus */
        $newOrderStatus = $this->em->getRepository(OrderStatus::class)->find($data['orderStatus']['id']);

        if (isset($data['country']['id'])) {
            /** @var Country $country */
            $country = $this->em->getRepository(Country::class)->find($data['country']['id']);
        } else {
            $country = null;
        }
        $order->setCountry($country);
        /** @var OrderProduct $orderProduct */
        foreach ($order->getProductVariants() as $index => $orderProduct) {
            if (count($data['productVariants']) < 1) {
                $orderProduct->getProductVariant()->setInStock($orderProduct->getCount() + $orderProduct->getProductVariant()->getInStock());
                $order->removeProductVariant($orderProduct);
                $this->em->remove($orderProduct);
            }
//            foreach ($data['productVariants'] as $productVariant) {
                if (array_search($orderProduct->getId(), array_column($data['productVariants'], 'id')) === false) {
                    $orderProduct->getProductVariant()->setInStock($orderProduct->getCount() + $orderProduct->getProductVariant()->getInStock());
                    $order->removeProductVariant($orderProduct);
                    $this->em->remove($orderProduct);
                }
//            }
        }
        foreach ($data['productVariants'] as $index => $variantData) {
            if ($variantData['id'] === null) {
                /** @var ProductVariant $productVariant */
                $productVariant = $this->em->getRepository(ProductVariant::class)->find($variantData['productVariantId']);
                $orderProduct = new OrderProduct();

                $orderProduct->setProductVariant($productVariant);
                $orderProduct->setCount($variantData['count']);
                $orderProduct->setPrice($productVariant->getPrice()['Kč']['vat'] === true ? $productVariant->getPrice()['Kč']['withoutVat'] : $productVariant->getPrice()['Kč']['withVat']);
                $orderProduct->setActualPrice($productVariant->getPrice()['Kč']['vat'] === true ? $productVariant->getPrice()['Kč']['actualWithoutVat'] : $productVariant->getPrice()['Kč']['actualWithVat']);
                $orderProduct->setVat($productVariant->getPrice()['Kč']['vat']);
                $vat = $this->em->getRepository(Vat::class)->find($productVariant->getPrice()['Kč']['vatId']);
                $orderProduct->setVat2($vat);
                $order->addProductVariant($orderProduct);
                $oldProductCount = 0;
            } else {
                /** @var OrderProduct $orderProduct */
                $orderProduct = $this->em->getRepository(OrderProduct::class)->find($variantData['id']);
                $oldProductCount = $orderProduct->getCount();
                $orderProduct->setCount($variantData['count']);
            }
            $countDiff = $oldProductCount - $variantData['count'];
            $orderProduct->getProductVariant()->setInStock($countDiff + $orderProduct->getProductVariant()->getInStock());
        }
        
        if ($newOrderStatus->getSlug() !== $order->getOrderStatus()->getSlug()) {
            $this->em->getRepository(Order::class)->updateOrderStatus($order, $newOrderStatus);
        }
        $this->em->persist($order);
        $this->em->flush();
        return $order;
    }

    /**
     * @Rest\Put("/{id}/mark-as-payed", name="mark_as_payed")
     * @Rest\View(serializerGroups={"orderDetail", "detail"})
     *
     * @param OrderRepository $repository
     * @param Order|null $order
     * @return Order
     */
    public function markOrderAsPayed(OrderRepository $repository,Order $order = null)
    {
        if (!$order) {
            ErrorMessages::message(ErrorMessages::ORDER_NOT_FOUND);
        }
        $successPay = $this->em->getRepository(OrderPaymentLog::class)->findOneBy(['status' => OrderPaymentLog::PAID]);
        if ($order->getPaymentDate() !== null && $successPay) {
            ErrorMessages::message(ErrorMessages::ORDER_IS_PAYED);
        }
        $newPaymentLog = new OrderPaymentLog();
        $newPaymentLog->setPrice($order->getFullPrice());
        $newPaymentLog->setStatus(OrderPaymentLog::PAID);
        $newPaymentLog->setPaymentType($order->getPayment());
        $newPaymentLog->setPaymentNumber($order->getNextPaymentNumber());
        $repository->updateOrderPaymentDate($order,new \DateTime());
        $order->addPaymentLog($newPaymentLog);
        $this->em->flush();
        return $order;
    }

}
