<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\EshopBundle\Entity\OrderStatus;
use App\Akip\EshopBundle\Repository\OrderStatusRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class OrderStatusController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/order-status", name="order-status_")
 */
class OrderStatusController extends AbstractEntityController
{
    /**
     * @var EntityManagerInterface
     */
    public $em;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * CountryController constructor.
     * @param OrderStatusRepository $orderStatusRepository
     * @param EntityManagerInterface $em
     * @param ValidatorInterface $validator
     */
    public function __construct(OrderStatusRepository $orderStatusRepository, EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $this->repository = $orderStatusRepository;
        $this->entity = new OrderStatus();
        $this->em = $em;
        $this->validator = $validator;
    }

}
