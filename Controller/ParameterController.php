<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\EshopBundle\Entity\Category;
use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Parameter;
use App\Akip\EshopBundle\Entity\ParameterTranslation;
use App\Akip\EshopBundle\Entity\ParameterValue;
use App\Akip\EshopBundle\Entity\ParameterValueTranslation;
use App\Akip\EshopBundle\Repository\CategoryRepository;
use App\Akip\EshopBundle\Repository\ParameterRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface as EntityManagerInterfaceAlias;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use phpDocumentor\Reflection\DocBlock\Tags\Param;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ParameterController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/parameter", name="parameter_")
 */
class ParameterController extends BaseController
{
    const TYPE_INT = 'int';
    const TYPE_FLOAT = 'float';
    const TYPE_BOOLEAN = 'boolean';
    const TYPE_COLOR = 'color';
    const TYPE_CHOICE = 'choice';
    const TYPE_MULTI_CHOICE = 'multi_choice';

    /**
     * @var EntityManagerInterfaceAlias
     */
    private $em;

    /**
     * ParameterController constructor.
     * @param EntityManagerInterfaceAlias $em
     */
    public function __construct(EntityManagerInterfaceAlias $em)
    {

        $this->em = $em;
    }

    /**
     * @param ParameterRepository $repository
     * @param ParamFetcherInterface $pf
     * @return array
     * @Rest\Get("", name="list")
     * @Rest\View(serializerGroups={"list"})
     *
     * @Rest\QueryParam(name="limit", default="10")
     * @Rest\QueryParam(name="offset", default="0")
     *
     * @Rest\QueryParam(name="search")
     * @Rest\QueryParam(name="filter", map=true)
     * @Rest\QueryParam(name="order", map=true)
     */
    public function list(ParameterRepository $repository, ParamFetcherInterface $pf)
    {
        $total = count($repository->findAll());
        $this->filter = $pf->get('filter');
        $this->order = $this->getOrderBy($pf->get('order'));

        $criteria = Criteria::create();

        if ($pf->get('search')) {
            $criteria->andWhere($repository->getSearchCriteria($pf->get('search')));
        }
        // FILTERS
//        if ($this->getFilter('is_company')) {
//            $criteria->andWhere(Criteria::expr()->eq('is_company', $this->getFilter('is_company')));
//        }
        // ORDER BY
        $criteria->orderBy($this->order);

        $filteredCount = $repository->matching($criteria)->count();

        // LIMIT, OFFSET
        $criteria->setFirstResult($pf->get('offset'));
        $criteria->setMaxResults($pf->get('limit'));

        $data = $repository->matching($criteria);

        $filteredCount = count($data);
        return $this->listResponse($data, $total, $filteredCount, $pf->get('offset'), $pf->get('limit'));
    }

    /**
     * @Rest\Get("/types", name="types_array")
     * @Rest\View({"list"})
     */
    public function getTypes()
    {
        return [self::TYPE_INT, self::TYPE_FLOAT, self::TYPE_COLOR, self::TYPE_BOOLEAN, self::TYPE_CHOICE, self::TYPE_MULTI_CHOICE];
    }
    /**
     * @Rest\Get("/all", name="all")
     * @Rest\View(serializerGroups={"list"})
     * @param ParameterRepository $repository
     * @return Parameter[]
     */
    public function getAllParameters(ParameterRepository $repository)
    {
        $products = $repository->findBy([], ['sort' => 'ASC']);
        return $products;
    }

    /**
     * @Rest\Get("/{id}", name="get")
     * @Rest\View(serializerGroups={"detail"})
     * @param Parameter|null $parameter
     * @return mixed
     */
    public function getEntity(Parameter $parameter = null)
    {
        if (!$parameter)
            ErrorMessages::message(ErrorMessages::PARAMETER_NOT_FOUND);
        return $parameter;
    }


    /**
     * @Rest\Get("/category/{categoryId}", name="in_category")
     * @Rest\View(serializerGroups={"detail"})
     * @param CategoryRepository $categoryRepository
     * @param $categoryId int
     * @return Parameter[]|\Doctrine\Common\Collections\Collection
     *
     */
    public function categoryParams(CategoryRepository $categoryRepository, $categoryId = -1)
    {
        $category = $categoryRepository->find($categoryId);
        if (!$category)
            ErrorMessages::message(ErrorMessages::CATEGORY_NOT_FOUND);
        return $category->getParameter();
    }


    /**
     * @param ValidatorInterface $validator
     * @param Request $request
     * @Rest\Post("", name="create")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function save(ValidatorInterface $validator, Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);

        }
        $parameter = new Parameter();
        $parameter->setName($data['name']);
        $parameter->setType($data['type']);
        $parameter->setSort(0);

        if (isset($data['sort']))
            $parameter->setSort($data['sort']);

        $valid = BaseController::validate($parameter, $validator);
        if (!empty($valid))
            return $valid;

        $this->em->persist($parameter);
        $this->em->flush();

        //vytvoreni defaultniho prekladu
        $defaultTranslation = new ParameterTranslation();
        $defaultTranslation->setParameter($parameter);
        $defaultTranslation->setName($parameter->getName());
        $defaultTranslation->setSlug($data['slug']);
        $defaultTranslation->setLocale($this->getDefaultLocale());

        $this->em->persist($defaultTranslation);
        $this->em->flush();
        return $parameter;
    }



    /**
     * @Rest\Put("/{id}", name="update")
     * @Rest\View(serializerGroups={"detail"})
     *
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param Parameter $parameter
     */
    public function update(Request $request, ValidatorInterface $validator, Parameter $parameter = null)
    {
        if (!$parameter)
            ErrorMessages::message(ErrorMessages::PARAMETER_NOT_FOUND);
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $parameter->setName($data['name']);
        $parameter->setType($data['type']);
        $parameter->setSort($data['sort']);

        if (isset($data['sort']))
            $parameter->setSort($data['sort']);

        $valid = BaseController::validate($parameter, $validator);
        if (!empty($valid))
            return $valid;


        if (isset($data['translations'])) {
            foreach ($data['translations'] as $key => $item) {
                $translation = new ParameterTranslation();
                $translation->load($key, $item);
                $translationsArray[] = $translation;
            }
        }

        $this->checkLocale(array_keys($data['translations']));
        foreach ($parameter->getTranslations() as $translation) {
            $this->em->remove($translation);
            $this->em->flush();
        }

        $this->em->persist($parameter);
        $this->em->flush();
        foreach ($translationsArray as $translation) {
            $this->em->persist($translation);
            $parameter->addTranslation($translation);
            $this->em->flush();
        }
//        $parameter->getTranslations();
        return $parameter;
    }

    /**
     * @Rest\Delete("/{id}", name="delete")
     * @Rest\View(statusCode=204)
     * @param Parameter|null $parameter
     */
    public function delete(Parameter $parameter = null)
    {
        if (!$parameter)
            ErrorMessages::message(ErrorMessages::PARAMETER_NOT_FOUND);
        $this->em->remove($parameter);
        $this->em->flush();
    }

}
