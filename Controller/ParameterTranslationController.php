<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Parameter;
use App\Akip\EshopBundle\Entity\ParameterTranslation;
use App\Akip\EshopBundle\Entity\ParameterValue;
use App\Akip\EshopBundle\Entity\ParameterValueTranslation;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ParameterTranslationController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/parameter", name="parameter_translation_")
 */
class ParameterTranslationController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;


    /**
     * ParameterController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param ValidatorInterface $validator
     * @param Request $request
     * @param Parameter|null $parameter
     * @Rest\Post("/{id}/translation", name="add_translations")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function addTranslations(ValidatorInterface $validator, Request $request, Parameter $parameter = null)
    {
        if (!$parameter)
            ErrorMessages::message(ErrorMessages::PARAMETER_NOT_FOUND);
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $this->checkLocale(array_keys($data));
        foreach ($data as $key => $item) {
            $paramTranslation = new ParameterTranslation();
            $paramTranslation->load($key, $item);
            $parameter->addTranslation($paramTranslation);
            $valid = BaseController::validate($paramTranslation, $validator);
            if (!empty($valid))
                return $valid;
            $this->em->persist($paramTranslation);
            $this->em->flush();

        }
        return $parameter;
    }

    /**
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param ParameterTranslation|null $parameterTranslation
     * @Rest\Put("/translation/{id}")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function update(Request $request, ValidatorInterface $validator ,ParameterTranslation $parameterTranslation = null)
    {
        if (!$parameterTranslation)
            ErrorMessages::message(ErrorMessages::PARAMETER_TRANSLATION_NOT_FOUND);
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data)
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        $this->checkLocale(array_keys($data));
        foreach ($data as $key => $item){
            $parameterTranslation->load($key, $item);
            $valid = BaseController::validate($parameterTranslation, $validator);
            if (!empty($valid))
                return $valid;
            $this->em->flush();
        }
        return $parameterTranslation;
    }

    /**
     * @param Parameter|null $parameter
     * @Rest\Get("/{id}/translation", name="list")
     * @Rest\View(serializerGroups={"list"})
     */
    public function list(Parameter $parameter = null)
    {
        if (!$parameter)
            ErrorMessages::message(ErrorMessages::PARAMETER_NOT_FOUND);

        return $parameter->getTranslations();
    }
}
