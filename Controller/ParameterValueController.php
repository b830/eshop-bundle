<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Parameter;
use App\Akip\EshopBundle\Entity\ParameterValue;
use App\Akip\EshopBundle\Entity\ParameterValueTranslation;
use App\Akip\EshopBundle\Entity\ProductParameter;
use App\Akip\EshopBundle\Entity\ProductVariantParameter;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ParameterValueController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/parameter", name="parameter_value_")
 */
class ParameterValueController extends BaseController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ParameterController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * @Rest\Post("/{id}/value", name="add_values")
     * @param ValidatorInterface $validator
     * @param Request $request
     * @param Parameter|null $parameter
     * @Rest\View(serializerGroups={"detail"})
     */
    public function save(ValidatorInterface $validator, Request $request, Parameter $parameter = null)
    {
        if (!$parameter)
            ErrorMessages::message(ErrorMessages::PARAMETER_NOT_FOUND);
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        if (!$parameter->checkTypes())
            ErrorMessages::message(ErrorMessages::PARAMETER_CANNOT_HAVE_VALUES);
        $paramValue = new ParameterValue();
        $paramValue->setParameter($parameter);
        $paramValue->load($data);

        $this->checkLocale(array_keys($data['translations']));

        $valid = BaseController::validate($paramValue, $validator);
        if (!empty($valid))
            return $valid;

        $this->em->persist($paramValue);
        $this->em->flush();

        foreach ($data['translations'] as $key => $item) {
            $paramValueTrans = new ParameterValueTranslation();
            $paramValueTrans->load($key, $item);
            $paramValueTrans->setParameterValue($paramValue);

            $valid = BaseController::validate($paramValueTrans, $validator);
            if (!empty($valid))
                return $valid;


            $this->em->persist($paramValueTrans);
            $this->em->flush();

            $paramValue->addTranslation($paramValueTrans);
        }
        return $paramValue;
    }

    /**
     * @param ParameterValue $parameterValue
     * @Rest\Delete("/value/{id}", name="delete")
     * @Rest\View(statusCode=204)
     */
    public function delete(ParameterValue $parameterValue = null)
    {
        if (!$parameterValue)
            ErrorMessages::message(ErrorMessages::PARAMETER_VALUE_NOT_FOUND);
        $productParameterValues = $this->em->getRepository(ProductParameter::class)->findBy(['parameterValue' => $parameterValue]);
        if (count($productParameterValues) > 0) {
            ErrorMessages::message(ErrorMessages::VALUE_CANNOT_BE_DELETED, '', ', product with this value ' . $productParameterValues[0]->getProduct()->getName());
        }
        $productVariantParameterValues = $this->em->getRepository(ProductVariantParameter::class)->findBy(['parameterValue' => $parameterValue]);
        if (count($productVariantParameterValues) > 0) {
            ErrorMessages::message(ErrorMessages::VALUE_CANNOT_BE_DELETED, '', ', product variant with this value ' . $productVariantParameterValues[0]->getProductVariant()->getName());
        }
        $this->em->flush();
        $this->em->remove($parameterValue);
        $this->em->flush();

    }

    /**
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param ParameterValue|null $parameterValue
     * @Rest\Put("/value/{id}", name="update")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function update(Request $request, ValidatorInterface $validator, ParameterValue $parameterValue = null)
    {
        if (!$parameterValue)
            ErrorMessages::message(ErrorMessages::PARAMETER_VALUE_NOT_FOUND);

        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $parameterValue->load($data);
        $valid = BaseController::validate($parameterValue, $validator);
        if (!empty($valid))
            return $valid;

        $this->checkLocale(array_keys($data['translations']));

        foreach ($parameterValue->get_Translations() as $paramTrans)
            $parameterValue->removeTranslation($paramTrans);

        foreach ($data['translations'] as $key => $item) {
            $paramValueTrans = new ParameterValueTranslation();
            $paramValueTrans->load($key, $item);
            $paramValueTrans->setParameterValue($parameterValue);

            $valid = BaseController::validate($paramValueTrans, $validator);
            if (!empty($valid))
                return $valid;


            $this->em->persist($paramValueTrans);
            $this->em->flush();

            $parameterValue->addTranslation($paramValueTrans);
        }
        return $parameterValue;
    }

    /**
     * @param Parameter|null $parameter
     * @Rest\Get("/{id}/value", name="list")
     * @Rest\View(serializerGroups={"list"})
     */
    public function list(Parameter $parameter = null)
    {
        if (!$parameter)
            ErrorMessages::message(ErrorMessages::PARAMETER_NOT_FOUND);

        return $parameter->getValues();
    }
}
