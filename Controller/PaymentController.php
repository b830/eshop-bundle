<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Delivery;
use App\Akip\EshopBundle\Entity\Payment;
use App\Akip\EshopBundle\Entity\Vat;
use App\Akip\EshopBundle\Repository\PaymentRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class CustomerController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/payment", name="payment_")
 */
class PaymentController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * CustomerController constructor.
     * @param EntityManagerInterface $em
     * @param ValidatorInterface $validator
     */
    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->validator = $validator;
    }

    /**
     * @param ParamFetcherInterface $pf
     * @param PaymentRepository $repository
     * @return array
     * @Rest\Get("", name="list")
     * @Rest\View(serializerGroups={"list"})
     *
     * @Rest\QueryParam(name="limit", default="10")
     * @Rest\QueryParam(name="offset", default="0")
     *
     * @Rest\QueryParam(name="search")
     * @Rest\QueryParam(name="filter", map=true)
     * @Rest\QueryParam(name="order", map=true)
     */
    public function list(ParamFetcherInterface $pf, PaymentRepository $repository)
    {
        $total = count($repository->findAll());
        $this->filter = $pf->get('filter');
        $this->order = $this->getOrderBy($pf->get('order'));

        $criteria = Criteria::create();

        if ($pf->get('search')) {
            $criteria->andWhere($repository->getSearchCriteria($pf->get('search')));
        }
        // ORDER BY
        $criteria->orderBy($this->order);

        $filteredCount = $repository->matching($criteria)->count();
        // LIMIT, OFFSET
        $criteria->setFirstResult($pf->get('offset'));
        $criteria->setMaxResults($pf->get('limit'));

        $data = $repository->matching($criteria);

        $filteredCount = count($data);
        return $this->listResponse($data, $total, $filteredCount, $pf->get('limit'), $pf->get('offset'));
    }

    /**
     * @Rest\Get("/all", name="get_all")
     * @Rest\View(serializerGroups={"list"})
     * @param PaymentRepository $repository
     * @return \App\Akip\EshopBundle\Entity\Payment[]
     */
    public function getAllPayments(PaymentRepository $repository)
    {
        return $repository->findAll();
    }

    /**
     * @Rest\Get("/{id}", name="get")
     * @Rest\View(serializerGroups={"detail", "paymentDetail"})
     * @param Payment|null $payment
     * @return Payment|null
     */
    public function getPayment(Payment $payment = null)
    {
        if (!$payment)
            ErrorMessages::message(ErrorMessages::PAYMENT_NOT_FOUND);
        $payment->checkOptions();
        $this->em->flush();
        return $payment;
    }

    /**
     * @param Request $request
     * @param Payment|null $payment
     * @Rest\Put("/{id}", name="update")
     * @Rest\View(serializerGroups={"detail", "paymentDetail"})
     */
    public function update(Request $request, Payment $payment = null)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        /** @var Vat $vat */
        $vat = $this->em->getRepository(Vat::class)->find($data['vatId']);
        $payment->load($data, $vat);
        $testSlug = $this->em->getRepository(Payment::class)->findOneBy(['slug' => $data['slug']]);
        if ($testSlug && ($testSlug !== $payment)) {
            ErrorMessages::message(ErrorMessages::SLUG_EXISTS);
        }
        $valid = BaseController::validate($payment, $this->validator);
        if (!empty($valid)) {
            return $valid;
        }
        $deliveries = [];
        foreach ($data['deliveries'] as $deliveryId) {
            $delivery = $this->em->getRepository(Delivery::class)->find($deliveryId);
            $deliveries[] = $delivery;
        }
        foreach ($payment->getDeliveries() as $delivery) {
            $delivery->removePayment($payment);
            $payment->removeDelivery($delivery);
        }
//        foreach ($data['options'] as $option ){
//            if (isset($option['slug']) && $option['slug'] === 'bank-account2') {
//                return $option;
//            }
//        }
//        return;
        $this->em->flush();
        foreach ($deliveries as $delivery) {
            /** @var Delivery $delivery */
            if (!$delivery->getPayments()->contains($payment)) {
                $delivery->addPayment($payment);
            }
            if (!$payment->getDeliveries()->contains($delivery)) {
                $payment->addDelivery($delivery);
            }
        }
        $this->em->persist($payment);
        $this->em->flush();
        return $payment;
    }

    /**
     * @Rest\Delete("/{id}", name="delete")
     * @Rest\View(statusCode=204)
     *
     * @param Payment|null $payment
     */
    public function delete(Payment $payment = null)
    {
        $this->em->remove($payment);
        $this->em->flush();
    }
}
