<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Currency;
use App\Akip\EshopBundle\Entity\Price;
use App\Akip\EshopBundle\Entity\Vat;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class PriceController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/product/variant/price", name="product_variant_price_")
 */
class PriceController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * PriceController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Rest\Put("/{id}", name="update")
     * @Rest\View(serializerGroups={"detail"})
     * @param Request $request
     * @param Price|null $price
     */
    public function update(Request $request, Price $price = null)
    {
        if (!$price)
            ErrorMessages::message(ErrorMessages::PRICE_NOT_FOUND);
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data)
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        $vat = $this->getDoctrine()->getRepository(Vat::class)->find($data['vatId']);
        if (!$vat)
            ErrorMessages::message(ErrorMessages::NOT_FOUND);

        // PERSPEKTIVA: jine meny nez koruny
        if (isset($data['currencyId'])){
            $currency = $this->getDoctrine()->getRepository(Currency::class)->find($data['currencyId']);
            if (!$currency)
                ErrorMessages::message(ErrorMessages::NOT_FOUND);
        } else {
            // koruna
            $currency = $this->getDoctrine()->getRepository(Currency::class)->find(1);
        }
        //nacteni dat - problem symfony
        $currency->getId();

        $price->load($data, $currency, $vat);
        return $price;
    }
}
