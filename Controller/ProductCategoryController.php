<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Category;
use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\ProductCategory;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class ProductCategoryController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/product", name="product_category_")
 */
class ProductCategoryController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ProductCategoryController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Rest\Get("/{id}/category", name="list")
     * @Rest\View(serializerGroups={"list"})
     *
     * @param Product|null $product
     * @return ProductCategory[]|object[]
     *
     */
    public function list(Product $product = null)
    {
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);

        return $product->getItemsCategory();
    }

    /**
     * @Rest\Post("/{id}/category", name="add")
     * @Rest\View(serializerGroups={"list"})
     *
     * @param Request $request
     * @param Product|null $product
     */
    public function save(Request $request, Product $product = null)
    {
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);

        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data)
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);

        if (in_array($data['mainCat'], $data['cat']))
            throw new HttpException(Response::HTTP_NOT_FOUND, "Duplicity category id - [{$data['mainCat']}]");

        // Remove all rows from product_category table
        foreach ($product->getItemsCategory() as $productCategory) {
            $this->em->remove($productCategory);
        }

        // duplicate primary key fix
        $this->em->flush();

        $mainCat = $this->getDoctrine()->getRepository(Category::class)->find($data['mainCat']['id']);
        if (!$mainCat)
            ErrorMessages::message(ErrorMessages::CATEGORY_NOT_FOUND);

        $productMainCat = new ProductCategory();
        $productMainCat->setIsMain(true);
        $productMainCat->setCategory($mainCat);
        $productMainCat->setProduct($product);
        $productMainCat->setSort($data['mainCat']['sort']);

        $product->addItemsCategory($productMainCat);

        foreach ($data['cat'] as $item) {
            $category = $this->getDoctrine()->getRepository(Category::class)->find($item['id']);

            if (!$category)
                ErrorMessages::message(ErrorMessages::CATEGORY_NOT_FOUND);

            $productCat = new ProductCategory();
            $productCat->setCategory($category);
            $productCat->setSort($item['sort']);
            $product->addItemsCategory($productCat);
        }

        $this->em->flush();
        return $product->getItemsCategory()->getValues();
    }
}
