<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\CmsBundle\Entity\MenuItem;
use App\Akip\CmsBundle\Entity\Page;
use App\Akip\EshopBundle\Entity\Brand;
use App\Akip\EshopBundle\Entity\Category;
use App\Akip\EshopBundle\Entity\CategoryTranslation;
use App\Akip\EshopBundle\Entity\Currency;
use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Flag;
use App\Akip\EshopBundle\Entity\Price;
use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\ProductAccessory;
use App\Akip\EshopBundle\Entity\ProductCategory;
use App\Akip\EshopBundle\Entity\ProductJoin;
use App\Akip\EshopBundle\Entity\ProductParameter;
use App\Akip\EshopBundle\Entity\ProductRelated;
use App\Akip\EshopBundle\Entity\ProductTranslation;
use App\Akip\EshopBundle\Entity\ProductVariant;
use App\Akip\EshopBundle\Entity\ProductVariantTranslation;
use App\Akip\EshopBundle\Entity\Sign;
use App\Akip\EshopBundle\Entity\Vat;
use App\Akip\EshopBundle\Repository\CategoryRepository;
use App\Akip\EshopBundle\Repository\ProductAccessoryRepository;
use App\Akip\EshopBundle\Repository\ProductRelatedRepository;
use App\Akip\EshopBundle\Repository\ProductRepository;
use App\Akip\FileManagerBundle\Controller\FileController;
use App\Akip\FileManagerBundle\Entity\File;
use App\Akip\FileManagerBundle\Entity\Folder;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Request\ParamFetcherInterface;
use http\Client;
use phpDocumentor\Reflection\Element;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;


/**
 * Class ProductController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/product", name="product_")
 *
 */
class ProductController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ProductController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Rest\Get("", name="list")
     * @Rest\View(serializerGroups={"list"})
     *
     * @Rest\QueryParam(name="limit", default="10")
     * @Rest\QueryParam(name="offset", default="0")
     *
     * @Rest\QueryParam(name="search")
     * @Rest\QueryParam(name="filter", map=true)
     * @Rest\QueryParam(name="order", map=true)
     * @param ProductRepository $pr
     * @param ParamFetcherInterface $pf
     * @return Product[]
     */
    public function list(ProductRepository $pr, ParamFetcherInterface $pf, ProductRepository $repository)
    {
        $total = count($repository->findAll());
        $this->filter = $pf->get('filter');
        $this->order = $this->getOrderBy($pf->get('order'));

        $criteria = Criteria::create();

        if ($pf->get('search')) {
            $criteria->andWhere($repository->getSearchCriteria($pf->get('search')));
        }
        // FILTERS
        if ($this->getFilter('enabled')) {
            $criteria->andWhere(Criteria::expr()->eq('enabled', $this->getFilter('enabled')));
        }
        // ORDER BY
        $criteria->orderBy($this->order);

        $filteredCount = $repository->matching($criteria)->count();

        // LIMIT, OFFSET
        $criteria->setFirstResult($pf->get('offset'));
        $criteria->setMaxResults($pf->get('limit'));

        $data = $repository->matching($criteria);

        $filteredCount = count($data);
        return $this->listResponse($data, $total, $filteredCount, $pf->get('offset'), $pf->get('limit'));
    }

    /**
     * @param ValidatorInterface $validator
     * @param Request $request
     * @Rest\Post("", name="create")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function save(ValidatorInterface $validator, Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }

        $testSlug = $this->em->getRepository(ProductTranslation::class)->findOneBy(['slug' => $data['slug']]);
        if ($testSlug) {
            ErrorMessages::message(ErrorMessages::SLUG_EXISTS);
        }
        /**
         * @var $mainCat Category
         */
        $mainCat = $this->getDoctrine()->getRepository(Category::class)->find($data['mainCat']);
        if (!$mainCat)
            ErrorMessages::message(ErrorMessages::CATEGORY_NOT_FOUND);

        $product = new Product();

        $product->setName($data['name']);
        $product->setEnabled(true);

        if (isset($data['sort']))
            $product->setSort($data['sort']);

        if (isset($data['homePageSort']))
            $product->setHomePageSort($data['homePageSort']);

        if (isset($data['withVariant']))
            $product->setWithVariant($data['withVariant']);


        $mainCatProduct = new ProductCategory();
        $mainCatProduct->setCategory($mainCat);
        $product->addItemsCategory($mainCatProduct);
        if ($mainCat->getParent() !== null) {
            $catProduct = new ProductCategory();
            $catProduct->setCategory($mainCat->getParent());
            $product->addItemsCategory($catProduct);
        }

        $valid = BaseController::validate($product, $validator);
        if (!empty($valid))
            return $valid;

        $this->em->persist($product);
        $this->em->flush();

        $productCat = $this->getDoctrine()->getRepository(ProductCategory::class)->findOneBy(['category' => $mainCat, 'product' => $product]);
        $productCat->setIsMain(true);
        $this->em->flush();


        // vytvoreni prazdne varianty
        $this->generateDefaultVariant($product, $validator, null, $data['slug'], $data['variantName']);

        //vytvoreni defaultniho prekladu
        $trans = new ProductTranslation();
        $trans->setProduct($product);
        $this->generateDefaultTranslation($trans, $product, $data['slug'], $validator);

        if ($mainCat->getParameter()->isEmpty())
            return $product;
        //nakopirovani defaultnich parametru z kategorie do parametru produktu
        foreach ($mainCat->getMain()->getParameter() as $parameter) {
            $productParam = new ProductParameter();
            $productParam->setProduct($product);
            $productParam->setParameter($parameter);
            $this->em->persist($productParam);
            $this->em->flush();
        }
        return $product;
    }

    private function generateDefaultTranslation($defaultTranslation, $parentEntity, $slug, $validator, $description = '', $shortDescription = '')
    {
//        $testSlug = $this->em->getRepository(ProductTranslation::class)->findOneBy(['slug' => $slug]);
//        if ($testSlug) {
//            ErrorMessages::message(ErrorMessages::SLUG_EXISTS);
//        }
        $defaultTranslation->setName($parentEntity->getName());
        $defaultTranslation->setSlug($slug);
        $defaultTranslation->setLocale($this->getDefaultLocale());
        $defaultTranslation->setDescription($description);
        $defaultTranslation->setShortDescription($shortDescription);

//        $valid = BaseController::validate($defaultTranslation, $validator);
//        if (!empty($valid))
//            return $valid;

        $this->em->persist($defaultTranslation);
        $this->em->flush();
    }

    private function generateDefaultVariant(Product $product, $validator, $hire_price = null, $slug = '', $name = '')
    {
        $defaulVariant = new ProductVariant();
        $defaulVariant->setProduct($product);
        $defaulVariant->setMain(true);
        $defaulVariant->setEnabled(true);
        $defaulVariant->setName($name);

        $valid = BaseController::validate($defaulVariant, $validator);
        if (!empty($valid))
            return $valid;
        $product->addProductVariant($defaulVariant);
        $this->em->persist($defaulVariant);
        $this->em->flush();
        if ($slug !== '') {
            $translation = new ProductVariantTranslation();
            $translation->setLocale($this->getDefaultLocale());
            $translation->setProductVariant($defaulVariant);
            $translation->setName($name);
            $translation->setSlug($slug);
            $this->em->persist($translation);
            $this->em->flush();
        }
        if ($hire_price !== null) {
            // Cena
            // Koruny
            $currency = $this->getDoctrine()->getRepository(Currency::class)->find(1);
            // 21%
            $vat = $this->getDoctrine()->getRepository(Vat::class)->find(3);
            $price = new Price();
            $price->setCurrency($currency);
            $price->setVat2($vat);
            $price->setVat(true);
            $price->setPrice($hire_price);
            $price->setProductVariant($defaulVariant);
            $this->em->persist($price);
            $this->em->flush();
        }
    }

    /**
     * @Rest\Get("/all", name="all")
     * @Rest\View(serializerGroups={"list", "allList"})
     * @param ProductRepository $repository
     * @return Product[]
     */
    public function getAllProducts(ProductRepository $repository)
    {
        $products = $repository->findAll();
        return $products;
    }

    /**
     * @param ValidatorInterface $validator
     * @param Request $request
     * @Rest\Post("/import", name="import")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function importProducts(ValidatorInterface $validator, Request $request)
    {
        set_time_limit(300);
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $category = new Category();
        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->setLocale($this->getDefaultLocale());
        $categoryTranslation->setMetadata("{}");

        if ($data[0]['type'] === 'product') {
            $category->load(['name' => 'Import produktů'], $category);
            $categoryTranslation->setName('Import produktů');
            $categoryTranslation->setSlug($this->slugGeneratetor($categoryTranslation->getName()));
        } else {
            $category->load(['name' => 'Import příslušenství'], $category);
            $categoryTranslation->setName('Import příslušenství');
            $categoryTranslation->setSlug($this->slugGeneratetor($categoryTranslation->getName()));
        }
        $this->em->persist($category);
        $categoryTranslation->setCategory($category);
        $this->em->persist($categoryTranslation);

        foreach ($data as $item) {
            if (isset($item['id'])) {
                $product = new Product();
                $product->setId((int)$item['id']);
                $product->setName($item['name']);
                $product->setSort(0);
                $product->setEnabled(true);

                // vypnuti auto_increment
                $metadata = $this->em->getClassMetaData(get_class($product));
                $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
                $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());

                $this->em->persist($product);
                $this->em->flush();

//                return $product;
                $mainCatProduct = new ProductCategory();
                $mainCatProduct->setCategory($category);
                $mainCatProduct->setIsMain(true);
                $product->addItemsCategory($mainCatProduct);

                $valid = BaseController::validate($product, $validator);
                if (!empty($valid))
                    return $valid;
                $this->em->persist($product);
                $this->em->flush();

                $defaultTranslation = new ProductTranslation();
                $defaultTranslation->setProduct($product);
                $this->generateDefaultTranslation($defaultTranslation, $product, $this->slugGeneratetor($item['name']), $validator, $item['popis']);
                $this->generateDefaultVariant($product, $validator, $item['hire_price']);
            } else {
                $variant = new ProductVariant();
                /**
                 * @var Product $product
                 */
                $product = $this->em->getRepository(Product::class)->find((int)$item['product_id']);
                if (!$product) {
                    return ($item['product_id']);
                }
//                exit(var_dump($item['product_id']));

                // tady uz existuji varianty, proto odstranime defaultni variantu

                $variant->setProduct($product);
                $variant->setName($item['name']);
                $variant->setMain(false);

                $product->addProductVariant($variant);
                $this->em->persist($variant);
                $this->em->flush();

                foreach ($product->getProductVariants() as $productVariant) {
                    if ($productVariant->getName() === '') {
                        $this->em->remove($productVariant);
                        $this->em->flush();
                    }
                }
                // Pokud jeste neexistuje hlavni varianta, tak ji nastavime
                if ($product->getMainProductVariants() === 0) {
                    $variant->setMain(true);
                    $this->em->flush();
                }


                $defaultTranslation = new ProductVariantTranslation();
                $defaultTranslation->setProductVariant($variant);
                $this->generateDefaultTranslation($defaultTranslation, $variant, $this->slugGeneratetor($item['name']), $validator);
                $this->em->persist($defaultTranslation);
                $this->em->flush();

                // Cena
                // Koruny
                $currency = $this->getDoctrine()->getRepository(Currency::class)->find(1);
                // 21%
                $vat = $this->getDoctrine()->getRepository(Vat::class)->find(3);
                $price = new Price();
                $price->setCurrency($currency);
                $price->setVat2($vat);
                $price->setVat(true);
                $price->setPrice($item['hire_price']);
                $price->setProductVariant($variant);
                $this->em->persist($price);
                $this->em->flush();
            }
        }
    }

    /**
     * @Rest\Patch("/slugupdate", name="update_slug")
     * @Rest\View(serializerGroups={"list"})
     */
    public function updateSlug(ProductRepository $productRepository)
    {
        $products = $productRepository->findAll();
        $data = [];
        foreach ($products as $product) {
            $variants = $product->getProductVariants();
            $productTranslations = $product->getTranslationsObj();
            foreach ($variants as $variant) {
                $variantTranslations = $variant->getTranslationsObj();
                foreach ($productTranslations as $productTranslation) {
                    foreach ($variantTranslations as $variantTranslation) {
                        /**
                         * @var ProductVariantTranslation $variantTranslation
                         */
                        $delimiter = $variantTranslation->getSlug() !== '' ? '-' : '';

                        if ($productTranslations !== null && $variantTranslation !== null) {

//                            $productSlug = is_array($productTranslations) ? '$productTranslations->getSlug()' : $productTranslations[0]->getSlug();
                            if ($productTranslation->getLocale() === $variantTranslation->getLocale()) {
                                $variantTranslation->setSlug($productTranslation->getSlug() . $delimiter . $variantTranslation->getSlug());
                                $data[] = $variantTranslation;
                            }
                        }
                    }
                }
            }
        }
        $this->em->flush();
        return $data;
//        return $productTranslations[0];
        return $variantTranslations;
    }

    /**
     * @Rest\Get("/{id}", name="deteil")
     * @Rest\View(serializerGroups={"detail"})
     *
     * @param Product|null $product
     * @return Product
     */
    public function getProduct(Product $product = null)
    {
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);

        return $product;
    }

    /**
     * @Rest\Put("/{id}", name="update")
     * @Rest\View(serializerGroups={"detail"})
     *
     * @param Request $request
     * @param Product|null $product
     * @return Product
     */
    public function update(Request $request, Product $product = null)
    {
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);

        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data)
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);

        $product->setName($data['name']);
        $product->setEnabled($data['enabled']);
        $product->setDisplayOnHomepage($data['displayOnHomepage']);
        $product->setWithVariant($data['withVariant']);
        if (isset($data['brand'])) {
            if (isset($data['brand']['id'])) {
                /** @var Brand $brand */
                $brand = $this->em->getRepository(Brand::class)->find($data['brand']['id']);
                if (!$brand) {
                    ErrorMessages::message(ErrorMessages::BRAND_NOT_FOUND);
                }
                $product->setBrand($brand);
            }
        } else {
            $product->setBrand(null);
        }
        if (isset($data['page'])) {
            if (isset($data['page']['id'])) {
                /** @var Page $page */
                $page = $this->em->getRepository(Page::class)->find($data['page']['id']);
                if (!$page) {
                    ErrorMessages::message(ErrorMessages::PAGE_NOT_FOUND);
                }
                $product->setPage($page);
            }
        } else {
            $product->setPage(null);
        }
        if (isset($data['sort']))
            $product->setSort($data['sort']);
        if (isset($data['homePageSort']))
            $product->setHomePageSort($data['homePageSort']);
        if (isset($data['productCode']))
            $product->setProductCode($data['productCode']);
        if ($product->getWithVariant()) {
            $product->setVariantsDefinedByColor($data['variantsDefinedByColor']);
            $product->setVariantsDefinedBySize($data['variantsDefinedBySize']);
        } else {
            $product->setVariantsDefinedByColor(false);
            $product->setVariantsDefinedBySize(false);
        }
        $this->em->flush();
        return $product;
    }

    /**
     * @Rest\Post("/{id}/copy", name="copy")
     * @Rest\View(serializerGroups={"detail"})
     *
     * @param Product $product
     * @return Product
     */
    public function copy(Product $product)
    {
        $newProduct = clone $product;
        $this->em->detach($newProduct);
        $this->em->persist($newProduct);
        $this->em->flush();
        return $newProduct;
    }

    /**
     * @param Request $request
     * @param Product $product
     * @return array
     * @Rest\Post("/{id}/flag", name="add_flag")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function addFlags(Request $request, Product $product = null)
    {
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);

        $data = json_decode($request->getContent(), true);


        if ($product->getFlagsObj()) {
            foreach ($product->getFlagsObj() as $flag) {
                $product->removeFlag($flag);
                $this->em->flush();
            }
        }
        foreach ($data as $item) {
            /**
             * @var $flag Flag
             */
            $flag = $this->getDoctrine()->getRepository(Flag::class)->find($item);
            if (!$flag)
                ErrorMessages::message(ErrorMessages::EMPTY_BODY);
            $product->addFlag($flag);
            $this->em->flush();
        }
        return $product->getFlags();
    }

    /**
     * @Rest\Get("/{id}/flag", name="list_flag")
     * @Rest\View(serializerGroups={"list"})
     *
     * @param Product $product
     */
    public function flagList(Product $product)
    {
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);
        return $product->getFlags();
    }

    /**
     * @param Request $request
     * @param Product $product
     * @return array
     * @Rest\Post("/{id}/sign", name="add_sign")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function addSigns(Request $request, Product $product = null)
    {
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);

        $data = json_decode($request->getContent(), true);


        if ($product->getSignsObj()) {
            foreach ($product->getSignsObj() as $sign) {
                $product->removeSign($sign);
            }
            $this->em->flush();
        }
        foreach ($data as $item) {
            /**
             * @var $sign Sign
             */
            $sign = $this->getDoctrine()->getRepository(Sign::class)->find($item);
            if (!$sign)
                ErrorMessages::message(ErrorMessages::EMPTY_BODY);
            $product->addSign($sign);
            $this->em->flush();
        }
        return $product->getSigns();
    }

    /**
     * @Rest\Get("/{id}/sign", name="list_sign")
     * @Rest\View(serializerGroups={"list"})
     *
     * @param Product $product
     */
    public function signList(Product $product)
    {
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);
        return $product->getSigns();
    }

    /**
     * @Rest\Post("/{id}/related", name="add_related_products")
     * @Rest\View(serializerGroups={"list"})
     *
     * @param Request $request
     * @param Product|null $product
     */
    public function addRelated(Request $request, Product $product = null)
    {
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);

        $data = json_decode($request->getContent(), true);

        $related = $this->em->getRepository(ProductRelated::class)->findBy(['product' => $product]);
        foreach ($related as $item) {
            $this->em->remove($item);
        }
//        foreach ($product->getRelatedProduct() as $related) {
//            $product->removeRelatedProduct($related);
//        }
        $this->em->flush();

        foreach ($data as $item) {
            /** @var Product $p */
            $p = $this->getDoctrine()->getRepository(Product::class)->find($item['id']);
            if (!$p)
                ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);
            if ($product->getId() === $p->getId()) {
                continue;
            }
            $r = new ProductRelated();
            $r->setProduct($product);
            $r->setRelated($p);
            $r->setSort($item['sort']);
            $this->em->persist($r);
        }
        $this->em->flush();
        return $this->getRelatedList($this->em->getRepository(ProductRelated::class), $product);
    }

    /**
     * @Rest\Get("/{id}/related", name="list_related_product")
     * @Rest\View(serializerGroups={"detail"})
     *
     * @param Product|null $product
     * @return Product[]|\Doctrine\Common\Collections\Collection
     */
    public function getRelatedList(ProductRelatedRepository $repository, Product $product = null)
    {
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);
        return $repository->build($product);
    }

    /**
     * @Rest\Post("/{id}/accessory", name="add_accessory_products")
     * @Rest\View(serializerGroups={"list"})
     *
     * @param Request $request
     * @param Product|null $product
     * @return array
     */
    public function addAccessory(Request $request, Product $product = null)
    {
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);

        $data = json_decode($request->getContent(), true);
        $accessories = $this->em->getRepository(ProductAccessory::class)->findBy(['product' => $product]);
        foreach ($accessories as $accessory) {
            $this->em->remove($accessory);
        }
        $this->em->flush();
        foreach ($data as $item) {
            /**
             * @var Product $p
             */
            $p = $this->getDoctrine()->getRepository(Product::class)->find($item['id']);
            if (!$p)
                ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);
            if ($product->getId() === $p->getId()) {
                continue;
            }
            $_accessory = new ProductAccessory();
            $_accessory->setProduct($product);
            $_accessory->setAccessory($p);
            $_accessory->setSort($item['sort']);
            $this->em->persist($_accessory);
        }
        $this->em->flush();
        return $this->getAccessoryList($this->em->getRepository(ProductAccessory::class), $product);
    }

    /**
     * @Rest\Get("/{id}/productjoin", name="list_product_join")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function getProductJoinList(Product $product = null)
    {
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);
        return $this->em->getRepository(ProductJoin::class)->findBy(['product' => $product]);
    }

    /**
     * @Rest\Post("/{id}/productjoin", name="add_product_join")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function addProductJoin(Request $request, Product $product = null)
    {
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);

        $data = json_decode($request->getContent(), true);

        foreach ($this->em->getRepository(ProductJoin::class)->findBy(['product' => $product]) as $item) {
            $this->em->remove($item);
        }
        $this->em->flush();
        foreach ($data as $item) {
            /** @var Product $p */

            $join = new ProductJoin();
            $join->setProduct($product);
            $join->setJoinId($item['joinId']);
            $join->setQuantity($item['quantity']);
            $join->setJoinName($item['joinName']);
            $this->em->persist($join);
        }
        $this->em->flush();
        return $this->em->getRepository(ProductJoin::class)->findBy(['product' => $product]);
    }

    /**
     * @Rest\Get("/all/pn", name="list_all_foreign_products")
     * @Rest\QueryParam(name="search")
     */
    public function getProductsFromPNSystem(HttpClientInterface $client, ParamFetcherInterface $pf)
    {
        $search = $pf->get('search');

        $response = $client->request('GET', $_ENV['PN_URL'] . '/eshop/products',
            [
                'headers' => [
                    'Access-Token' => $_ENV['PN_SECRET_KEY'],
                ]
            ]
        );
        $content = $response->getContent();
        return json_decode($content);
    }

    /**
     * @Rest\Get("/{id}/accessory", name="list_accessory_product")
     * @Rest\View(serializerGroups={"detail"})
     *
     * @param Product|null $product
     * @return Product[]|\Doctrine\Common\Collections\Collection
     */
    public function getAccessoryList(ProductAccessoryRepository $repository, Product $product = null)
    {
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);
        return $repository->build($product);
    }

    /**
     * @Rest\Delete("/{id}", name="delete")
     * @Rest\View(statusCode=204)
     *
     * @param Product|null $product
     */
    public function delete(Product $product = null)
    {
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);
        $accessories = $this->em->getRepository(ProductAccessory::class)->findBy(['accessory' => $product->getId()]);
        foreach ($accessories as $accessory) {
            $this->em->remove($accessory);
        }
        $relateds = $this->em->getRepository(ProductRelated::class)->findBy(['related' => $product->getId()]);
        foreach ($relateds as $related) {
            $this->em->remove($related);
        }
        $this->em->remove($product);
        $this->em->flush();
    }
}
