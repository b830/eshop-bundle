<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\ProductInspiration;
use App\Akip\EshopBundle\Entity\ProductInspirationTranslation;
use App\Akip\EshopBundle\Repository\ProductInspirationRepository;
use App\Akip\FileManagerBundle\Entity\File;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductInspirationController
 * @package App\Akip\EshopBundle\Controller
 * @Route("/api/product/", name="produt_inspiration")
 */
class ProductInspirationController extends BaseController
{
    private $em;

    /**
     * ProductInspirationController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Rest\Get("{id}/inspiration", name="list")
     * @Rest\View(serializerGroups={"list"})
     * @param ProductInspirationRepository $repository
     */
    public function list(ProductInspirationRepository $repository, Product $product = null)
    {
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);
        return $repository->build($product);
    }

    /**
     * @Rest\Post("{id}/inspiration", name="save")
     * @Rest\View(serializerGroups={"list"})
     */
    public function save(Request $request, Product $product = null)
    {
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);

        $data = json_decode($request->getContent(), true);

        $inspirations = $this->em->getRepository(ProductInspiration::class)->findBy(['product' => $product]);
        if (!empty($inspirations)) {
            foreach ($inspirations as $inspiration) {
                foreach ($inspiration->getTranslationsObj() as $item) {
                    $this->em->remove($item);
                }
                $this->em->remove($inspiration);
            }
            $this->em->flush();
        }
        $translations = array();
        foreach ($data as $item) {
            $f = $this->getDoctrine()->getRepository(File::class)->find($item['uuid']);

            if (!$f)
                ErrorMessages::message(ErrorMessages::PHOTO_NOT_FOUND);
            $inspiration = new ProductInspiration();
            $inspiration->setSort($item['sort']);
            $inspiration->setProduct($product);
            $inspiration->setFile($f);
            $this->em->persist($inspiration);

            $this->em->flush();
//            return $inspiration;
            if (isset($item['translations'])) {
                foreach ($item['translations'] as $locale => $translation) {
                    $productInspirationTranslation = new ProductInspirationTranslation();
                    $productInspirationTranslation->load($translation, $locale, $inspiration);
                    $inspiration->addTranslation($productInspirationTranslation);
                    $this->em->persist($productInspirationTranslation);
                    array_push($translations, $productInspirationTranslation);
                }
            }
        }

        $this->em->flush();
        /** @var ProductInspirationTranslation $translation */
//        foreach ($translations as $translation) {
//            $this->em->persist($translation);
//        }
//        $this->em->flush();
//        return $translations;
        return $this->list($this->em->getRepository(ProductInspiration::class), $product);
    }
}
