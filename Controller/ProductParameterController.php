<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Parameter;
use App\Akip\EshopBundle\Entity\ParameterValue;
use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\ProductParameter;
use App\Akip\EshopBundle\Repository\ProductParameterRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use phpDocumentor\Reflection\DocBlock\Tags\Param;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;


/**
 * Class ProductParameterController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/product", name="product_parameter_")
 */
class ProductParameterController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ProductParameterController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param ProductParameterRepository $repository
     * @param Product|null $product
     *
     * @Rest\Get("/{id}/parameter", name="list")
     * @Rest\View(serializerGroups={"list"})
     */
    public function list(ProductParameterRepository $repository, Product $product = null)
    {

        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);
        $parameters = $product->uniqueParameters();
        $validParams = [];

        if (!$parameters)
            return [];
        foreach ($parameters as $parameter){
            $p = $this->em->getRepository(Parameter::class)->findOneBy(['id' => $parameter->getId()]);
            if ($p){
                $validParams[] = $p;
            }
        }

        if (!$validParams)
            return [];
        foreach ($validParams as $parameter) {
            if ($this->em->getRepository(Parameter::class)->find($parameter))
                $data[] = $repository->findByProductAndParameter($product, $parameter);
        }

        usort($data, function ($item1, $item2) {
            return $item1['sort'] <=> $item2['sort'];
        });

        return $data;
    }

    /**
     * @Rest\Put("/{idProduct}/parameter/{idParameter}", name="update")
     * @Rest\View(serializerGroups={"list"})
     *
     * @param ProductParameterRepository $repository
     * @param Request $request
     * @param int $idProduct
     * @param int $idParameter
     * @return mixed
     */
    public function update(ProductParameterRepository $repository, Request $request, int $idProduct, int $idParameter)
    {
        $param = $this->getDoctrine()->getRepository(Parameter::class)->findOneBy(['id' => $idParameter]);
        if (!$param)
            ErrorMessages::message(ErrorMessages::PARAMETER_NOT_FOUND);

        $product = $this->getDoctrine()->getRepository(Product::class)->findOneBy(['id' => $idProduct]);
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);

        // $parameter = class ProductParameter
        $parameter = $repository->findBy(['product' => $idProduct, 'parameter' => $idParameter]);
        if ($parameter) {
            if (is_array($parameter)) {
                foreach ($parameter as $item)
                    $this->em->remove($item);
            } else {
                $this->em->remove($parameter);
            }
            $this->em->flush();
        }
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data)
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);

        $this->loadEntity($param, $product, $data);

        $this->em->flush();
        return $repository->findByProductAndParameter($product, $param);
    }


    /**
     * @Rest\Post("/{idProduct}/parameter/{idParameter}", name="add")
     * @Rest\View(serializerGroups={"list"})
     *
     * @param ProductParameterRepository $repository
     * @param Request $request
     * @param int $idProduct
     * @param int $idParameter
     * @return mixed
     */
    public function save(ProductParameterRepository $repository, Request $request, int $idProduct, int $idParameter)
    {
        $param = $this->getDoctrine()->getRepository(Parameter::class)->findOneBy(['id' => $idParameter]);
        if (!$param)
            ErrorMessages::message(ErrorMessages::PARAMETER_NOT_FOUND);

        $product = $this->getDoctrine()->getRepository(Product::class)->findOneBy(['id' => $idProduct]);
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);

        $parameter = $repository->findBy(['product' => $idProduct, 'parameter' => $idParameter]);
        if ($parameter) {
            if (is_array($parameter)) {
                foreach ($parameter as $item)
                    $this->em->remove($item);
            } else {
                $this->em->remove($parameter);
            }
            $this->em->flush();
        }
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data)
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        $this->loadEntity($param, $product, $data);

        $this->em->flush();
        return $repository->findByProductAndParameter($product, $param);
    }

    private function loadEntity(Parameter $param, Product $product, $data)
    {
        if (in_array($param->getType(), [Parameter::TYPE_CHOICE, Parameter::TYPE_MULTI_CHOICE, Parameter::TYPE_COLOR])) {
            if (($param->getType() === Parameter::TYPE_CHOICE || $param->getType() === Parameter::TYPE_COLOR) && is_array($data['values'])){
                if (count($data['values']) > 1){
                    throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, "For type [{$param->getType()}] only 1 value is acceptable");
                }
            }
//            if ($param->getType() === Parameter::TYPE_CHOICE && count($data['values']) > 1)
//                throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, "For type [{$param->getType()}] only 1 value is acceptable");
            if (is_array($data['values'])) {
                foreach ($data['values'] as $valueId) {
                    $paramValue = $this->getDoctrine()->getRepository(ParameterValue::class)->find($valueId);

                    $this->check($paramValue, $param);

                    $parameter = new ProductParameter();
                    $parameter->load($param, $product, $data['sort'], $paramValue);
                    $this->em->persist($parameter);
                    $this->em->flush();
                }
            } else {
                /** @var ParameterValue $paramValue */
                $paramValue = $this->getDoctrine()->getRepository(ParameterValue::class)->find($data['values']);

                $this->check($paramValue, $param);

                $parameter = new ProductParameter();

                $parameter->load($param, $product,$data['sort'], $paramValue);

                $this->em->persist($parameter);
                $this->em->flush();
            }
        } else {
            $parameter = new ProductParameter();
            if (is_array($data['values'])) {
                $value = $data['values'][0];
            } else{
                $value = $data['values'];
            }
            $parameter->load($param, $product,$data['sort'], null, $value);
            $this->em->persist($parameter);
            $this->em->flush();
        }
    }

    // kontrola zda Parameter Value existuje a je v listu hodnot
    private function check($paramValue, $parameter)
    {
        if (!$paramValue)
            ErrorMessages::message(ErrorMessages::PARAMETER_VALUE_NOT_FOUND);
        if (!$parameter->getValues()->contains($paramValue))
            ErrorMessages::message(ErrorMessages::PARAMETER_VALUE_NOT_FOUND, '', ' in specified parameter');
    }

    /**
     * @Rest\Delete("/{idProduct}/parameter/{idParameter}", name="delete")
     * @Rest\View(StatusCode = 204)
     *
     * @param ProductParameterRepository $repository
     * @param $idProduct
     * @param $idParameter
     */
    public function delete(ProductParameterRepository $repository ,$idProduct, $idParameter)
    {
        $param = $this->getDoctrine()->getRepository(Parameter::class)->findOneBy(['id' => $idParameter]);
        if (!$param)
            ErrorMessages::message(ErrorMessages::PARAMETER_NOT_FOUND);

        $product = $this->getDoctrine()->getRepository(Product::class)->findOneBy(['id' => $idProduct]);
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);

        // $parameter = class ProductParameter
        $parameter = $repository->findBy(['product' => $idProduct, 'parameter' => $idParameter]);
        if ($parameter) {
            foreach ($parameter as $item) {
                $this->em->remove($item);
            }
            $this->em->flush();
        }
    }

}
