<?php

namespace App\Akip\EshopBundle\Controller;


use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\ProductPhoto;
use App\Akip\EshopBundle\Entity\ProductPhotoTranslation;
use App\Akip\EshopBundle\Repository\ProductParameterRepository;
use App\Akip\EshopBundle\Repository\ProductPhotoRepository;
use App\Akip\FileManagerBundle\Entity\File;
use App\Akip\FileManagerBundle\Repository\FileRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class ProductPhotoController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/product/", name="product_photo_")
 */
class ProductPhotoController extends BaseController
{
    private $em;

    /**
     * ProductPhotoController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Rest\Get("{id}/photo", name="list")
     * @Rest\View(serializerGroups={"list"})
     * @param ProductPhotoRepository $repository
     * @param Product|null $product
     */
    public function list(ProductPhotoRepository $repository, Product $product = null)
    {
        if (!$product)
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Product with specified id not found');
        return $repository->build($this->em->getRepository(File::class), $product);
//        return $repository->findBy(['product' => $product], ['sort' => 'ASC']);
    }

    /**
     * @Rest\Post("{id}/photo", name="save")
     * @Rest\View(serializerGroups={"detail"})
     *
     * @param Request $request
     * @param Product|null $product
     */
    public function save(Request $request, Product $product = null)
    {
        if (!$product)
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Product with specified id not found');
        $data = json_decode($request->getContent(), true);

        $mainCount = 0;
        $photos = array();
        $translations = array();
        $oldPhotos = $product->getPhotos();
        foreach ($data as $item) {
            $photo = $this->em->getRepository(File::class)->find($item['uuid']);
            if (!$photo)
                throw new HttpException(Response::HTTP_NOT_FOUND, 'Photo with specified uuid not found');
            $productPhoto = new ProductPhoto();

            $productPhoto->setName($photo->getOriginalName());
            $productPhoto->setMain($item['main']);
            $productPhoto->setPhoto($photo);
            $productPhoto->setProduct($product);
            $productPhoto->setSort($item['sort']);
            if (isset($item['translations'])){
                foreach ($item['translations'] as $locale => $translation) {
                    $productPhotoTranslation = new ProductPhotoTranslation();
                    $productPhotoTranslation->load($translation, $locale, $productPhoto);
                    $productPhoto->addTranslation($productPhotoTranslation);
                }
//                array_push($translations, $productPhotoTranslation);
            }
            if ($productPhoto->getMain())
                $mainCount++;
            array_push($photos, $productPhoto);
        }
        if ($mainCount > 1) {
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Only 1 photo can be main');
        } elseif ($mainCount == 0 && count($photos) > 0) {
            $photos[0]->setMain(true);
        }

        /** @var ProductPhoto $item */
        foreach ($oldPhotos as $item){
            foreach ($item->getTranslationsObj() as $translation) {
                $this->em->remove($translation);
            }
            $this->em->remove($item);
        }
        $this->em->flush();

        foreach ($photos as $photo) {
            $product->addPhoto($photo);
            $this->em->persist($photo);
        }
        $this->em->flush();


        return $this->list($this->em->getRepository(ProductPhoto::class), $product);
    }

    /**
     * @Rest\Put("{id}/photo", name="update")
     * @Rest\View(serializerGroups={"list"})
     *
     * @param Request $request
     * @param Product|null $product
     */
    public function update(Request $request, Product $product = null)
    {
        if (!$product)
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Product with specified id not found');
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, 'Empty body');
        }
        foreach ($data as $item) {
            $photo = $this->em->getRepository(File::class)->find($item['uuid']);
            if (!$photo)
                throw new HttpException(Response::HTTP_NOT_FOUND, 'Photo with specified uuid not found');
            $productPhoto = new ProductPhoto();

            $productPhoto->setName($photo->getOriginalName());
            $productPhoto->setMain($item['main']);
            $productPhoto->setPhoto($photo);
            $productPhoto->setProduct($product);

            if ($product->checkPhotoExist($productPhoto)) {
                continue;
            }
        }
    }

    /**
     * @Rest\Get("photo/{id}", name="detail")
     * @Rest\View(serializerGroups={"detail"})
     * @param ProductPhoto $productPhoto
     */
    public function getPhoto(ProductPhoto $productPhoto = null)
    {
        if (!$productPhoto)
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Product photo with specified id not found');
        return $productPhoto;
    }

    /**
     * @Rest\Delete("photo/{id}", name="delete")
     * @Rest\View(statusCode=204)
     * @param ProductPhoto $productPhoto
     */
    public function delete(ProductPhoto $productPhoto = null)
    {
        if (!$productPhoto)
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Product photo with specified id not found');
        $this->em->remove($productPhoto);
        $this->em->flush();
    }
}
