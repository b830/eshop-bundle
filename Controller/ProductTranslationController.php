<?php

namespace App\Akip\EshopBundle\Controller;


use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\ProductTranslation;
use App\Akip\EshopBundle\Entity\ProductVariantTranslation;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ProductTranslationController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/product", name="product_translation_")
 */
class ProductTranslationController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ProductTranslationController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Rest\Get("/{id}/translation", name="list")
     * @Rest\View(serializerGroups={"detail"})
     * @param Product $product
     */
    public function list(Product $product)
    {
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);
        return $product->getTranslations();
    }

    /**
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param Product $product
     * @Rest\Post("/{id}/translation", name="add")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function save(Request $request, ValidatorInterface $validator, Product $product = null)
    {
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);

        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data)
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);


        foreach ($data as $item){
            $testSlug = $this->em->getRepository(ProductTranslation::class)->findOneBy(['slug' => $item['slug']]);
            $translations = $product->getTranslationsObj();
            if ($testSlug && !$translations->contains($testSlug)) {
                ErrorMessages::message(ErrorMessages::SLUG_EXISTS);
            }
        }

        $this->checkLocale(array_keys($data));

        $productTranslations = array();
        foreach ($data as $key => $item) {
            $productTranslation = new ProductTranslation();
            $productTranslation->load($key, $item);
            $productTranslations[] = $productTranslation;
        }

        if($product->getTranslationsObj()){
            foreach ($product->getTranslationsObj() as $translation){
                $product->removeTranslation($translation);
            }
            $this->em->flush();
        }



        foreach ($productTranslations as $productTranslation) {
//            if ($this->getDoctrine()->getRepository(ProductTranslation::class)->findOneBy(['product' => $product, 'locale' => $key]))
//                throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, "Translation with locale [{$key}] for this product already exists");
            $product->addTranslation($productTranslation);
            $this->em->persist($productTranslation);
            $this->em->flush();
        }
        return $product->getTranslationsObj()->getValues();
    }

    /**
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param ProductTranslation|null $productTranslation
     * @Rest\Put("/translation/{id}", name="update")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function update(Request $request, ValidatorInterface $validator, ProductTranslation $productTranslation = null)
    {
        if (!$productTranslation)
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Product translation with specified id not found');

        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data)
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);

        $this->checkLocale(array_keys($data));

        foreach ($data as $key => $item){
            $productTranslation->load($key, $item);
//            $testSlug = $this->em->getRepository(ProductTranslation::class)->findOneBy(['slug' => $item['slug']]);
//            if ($testSlug) {
//                ErrorMessages::message(ErrorMessages::SLUG_EXISTS);
//            }
        }


//        die("xxx");
        $this->em->flush();

        return $productTranslation;
    }
}
