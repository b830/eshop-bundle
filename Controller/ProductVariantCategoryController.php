<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Category;
use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\ProductCategory;
use App\Akip\EshopBundle\Entity\ProductVariant;
use App\Akip\EshopBundle\Entity\ProductVariantCategory;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class ProductCategoryController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/product/variant", name="product_variant_category_")
 */
class ProductVariantCategoryController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ProductCategoryController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Rest\Get("/{id}/category", name="list")
     * @Rest\View(serializerGroups={"list"})
     *
     * @param ProductVariant|null $product
     * @return ProductVariantCategory[]|object[]
     *
     */
    public function list(ProductVariant $productVariant = null)
    {
        if (!$productVariant)
            ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);

        return $productVariant->getCategoryOverWrite() === true ? $productVariant->getItemsCategory() : [];
//        if ($productVariant->getCategoryOverWrite() ==)
//        return $productVariant->getItemsCategory();
    }

    /**
     * @Rest\Post("/{id}/category", name="add")
     * @Rest\View(serializerGroups={"list"})
     *
     * @param Request $request
     * @param ProductVariant|null $product
     */
    public function save(Request $request, ProductVariant $productVariant = null)
    {
        if (!$productVariant)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);

        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data)
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);

        if ($data['categoryOverWrite'] === false){
            foreach ($productVariant->getItemsCategory() as $productVariantCategory) {
                $this->em->remove($productVariantCategory);
            }
            $productVariant->setCategoryOverWrite(false);
            $this->em->flush();
            return [];
        } else {
            $productVariant->setCategoryOverWrite(true);
            if (in_array($data['mainCat'], $data['cat']))
                throw new HttpException(Response::HTTP_NOT_FOUND, "Duplicity category id - [{$data['mainCat']}]");

            // Remove all rows from product_category table
            foreach ($productVariant->getItemsCategory() as $productVariantCategory) {
                $this->em->remove($productVariantCategory);
            }

            // duplicate primary key fix
            $this->em->flush();

            $mainCat = $this->getDoctrine()->getRepository(Category::class)->find($data['mainCat']);
            if (!$mainCat)
                ErrorMessages::message(ErrorMessages::CATEGORY_NOT_FOUND);

            $productMainCat = new ProductVariantCategory();
            $productMainCat->setIsMain(true);
            $productMainCat->setCategory($mainCat);
            $productMainCat->setProductVariant($productVariant);

            $productVariant->addItemsCategory($productMainCat);

            foreach ($data['cat'] as $item) {
                $category = $this->getDoctrine()->getRepository(Category::class)->find($item);

                if (!$category)
                    ErrorMessages::message(ErrorMessages::CATEGORY_NOT_FOUND);

                $productCat = new ProductVariantCategory();
                $productCat->setCategory($category);
                $productVariant->addItemsCategory($productCat);
            }

            $this->em->flush();
            return $productVariant->getItemsCategory()->getValues();
        }
    }
}
