<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Category;
use App\Akip\EshopBundle\Entity\Currency;
use App\Akip\EshopBundle\Entity\Flag;
use App\Akip\EshopBundle\Entity\Parameter;
use App\Akip\EshopBundle\Entity\ParameterTranslation;
use App\Akip\EshopBundle\Entity\Price;
use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\ProductVariant;
use App\Akip\EshopBundle\Entity\ProductVariantParameter;
use App\Akip\EshopBundle\Entity\ProductVariantRelated;
use App\Akip\EshopBundle\Entity\ProductVariantTranslation;
use App\Akip\EshopBundle\Entity\Sign;
use App\Akip\EshopBundle\Entity\StockStatus;
use App\Akip\EshopBundle\Entity\Vat;
use App\Akip\EshopBundle\Repository\ParameterRepository;
use App\Akip\EshopBundle\Repository\ProductVariantRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use phpDocumentor\Reflection\Element;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ProductVariantController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/product", name="product_variant_")
 */
class ProductVariantController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ProductVariantController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * @Rest\Get("/variant", name="list")
     * @Rest\View(serializerGroups={"list"})
     *
     * @Rest\QueryParam(name="limit", default="10")
     * @Rest\QueryParam(name="offset", default="0")
     *
     * @Rest\QueryParam(name="search")
     * @Rest\QueryParam(name="filter", map=true)
     * @Rest\QueryParam(name="order", map=true)
     * @param ProductVariantRepository $repository
     * @param ParamFetcherInterface $pf
     * @return array
     */
    public function list(ProductVariantRepository $repository, ParamFetcherInterface $pf)
    {
        $total = count($repository->findAll());
        $this->filter = $pf->get('filter');
        $this->order = $this->getOrderBy($pf->get('order'));

        $criteria = Criteria::create();

        if ($pf->get('search')) {
            $criteria->andWhere($repository->getSearchCriteria($pf->get('search')));
        }
        // FILTERS
//        if ($this->getFilter('is_company')) {
//            $criteria->andWhere(Criteria::expr()->eq('is_company', $this->getFilter('is_company')));
//        }
        // ORDER BY
        $criteria->orderBy($this->order);

        $filteredCount = $repository->matching($criteria)->count();

        // LIMIT, OFFSET
        $criteria->setFirstResult($pf->get('offset'));
        $criteria->setMaxResults($pf->get('limit'));

        $data = $repository->matching($criteria);

        $filteredCount = count($data);
        return $this->listResponse($data, $total, $filteredCount, $pf->get('offset'), $pf->get('limit'));
    }


    /**
     * @Rest\Get("/variant/all", name="all_product_variants")
     * @Rest\View(serializerGroups={"list", "allList"})
     * @param ProductVariantRepository $repository
     */
    public function getAllProductVariants(ProductVariantRepository $repository)
    {
        $variants = $repository->findAll();
        return $variants;
    }

    //list variant v urcitym prodoktu

    /**
     * @Rest\Get("/{id}/variant", name="list_in_product")
     * @Rest\View(serializerGroups={"list"})
     *
     * @Rest\QueryParam(name="limit", default="10")
     * @Rest\QueryParam(name="offset", default="0")
     *
     * @Rest\QueryParam(name="search")
     * @Rest\QueryParam(name="filter", map=true)
     * @Rest\QueryParam(name="order", map=true)
     * @param ProductVariantRepository $repository
     * @param ParamFetcherInterface $pf
     * @param Product|null $product
     * @return array
     */
    public function listInProduct(ProductVariantRepository $repository, ParamFetcherInterface $pf, Product $product = null)
    {
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);
        return $this->em->getRepository(ProductVariant::class)->findBy(['product' => $product], ['sort' => 'ASC']);
//        return $product->getProductVariants();
    }


    /**
     * @Rest\Get("/variant/{id}", name="get")
     * @Rest\View(serializerGroups={"detail"})
     * @param ProductVariant|null $productVariant
     */
    public function getProductVariant(ProductVariant $productVariant = null)
    {
        if (!$productVariant)
            ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);
        return $productVariant;
    }

    /**
     * @Rest\Put("/variant/{id}", name="update")
     * @Rest\View(serializerGroups={"detail"})
     *
     * @param ProductVariant $productVariant
     * @param Request $request
     * @param ValidatorInterface $validator
     */
    public function update(Request $request, ValidatorInterface $validator, ProductVariant $productVariant = null)
    {
        if (!$productVariant)
            ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);

        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data)
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);

        $productVariant->setName($data['name']);
        $productVariant->setMain($data['main']);
        $productVariant->setEnabled($data['enabled']);
        $productVariant->setEan($data['ean']);
        $productVariant->setAddProductSlug($data['addProductSlug']);
        if (isset($data['sort']))
            $productVariant->setSort($data['sort']);
        if (isset($data['inStock']))
            $productVariant->setInStock($data['inStock']);


        if ($data['main'] === true) {
            $mainVariants = $this->getDoctrine()->getRepository(ProductVariant::class)->findBy(['product' => $productVariant->getProduct(), 'main' => true]);
            foreach ($mainVariants as $mainVariant) {
                $mainVariant->setMain(false);
                $this->em->persist($mainVariant);
            }
            $productVariant->setMain(true);
        } else {
            $productVariant->setMain(false);
        }

        if ($data['actualStockStatus']) {
            /** @var StockStatus $stockStatus */
            $stockStatus = $this->em->getRepository(StockStatus::class)->find($data['actualStockStatus']['id']);
            if (!$stockStatus) {
                ErrorMessages::message(ErrorMessages::STOCK_STATUS_NOT_FOUND);
            }
            $productVariant->setActualStockStatus($stockStatus);
        } else {
            $productVariant->setActualStockStatus(null);
        }
        if ($data['outOfStockStatus']) {
            /** @var StockStatus $stockStatus */
            $stockStatus = $this->em->getRepository(StockStatus::class)->find($data['outOfStockStatus']['id']);
            if (!$stockStatus) {
                ErrorMessages::message(ErrorMessages::STOCK_STATUS_NOT_FOUND);
            }
            $productVariant->setOutOfStockStatus($stockStatus);
        } else {
            $productVariant->setOutOfStockStatus(null);
        }

        $valid = BaseController::validate($productVariant, $validator);
        if (!empty($valid))
            return $valid;

        $this->em->flush();
        return $productVariant;
    }

    /**
     * @Rest\Post("/variant/{id}/related", name="add_related_products")
     * @Rest\View(serializerGroups={"list"})
     *
     * @param Request $request
     * @param ProductVariant|null $productVariant
     * @return
     */
    public function addRelated(Request $request, ProductVariant $productVariant = null)
    {
        if (!$productVariant)
            ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);

        $data = json_decode($request->getContent(), true);

        $related = $this->em->getRepository(ProductVariantRelated::class)->findBy(['productVariant' => $productVariant]);
        foreach ($related as $item) {
            $this->em->remove($item);
        }
        $this->em->flush();

        foreach ($data as $item) {
            /** @var ProductVariant $p */
            $p = $this->getDoctrine()->getRepository(ProductVariant::class)->find($item['id']);
            if (!$p)
                ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);
            if ($productVariant->getId() === $p->getId()) {
                continue;
            }
            $r = new ProductVariantRelated();
            $r->setProductVariant($productVariant);
            $r->setRelated($p);
            $r->setSort($item['sort']);
            $this->em->persist($r);
        }
        $this->em->flush();
        return $this->getRelatedList($productVariant);
    }

    /**
     * @Rest\Get("/variant/{id}/related", name="list_related_product")
     * @Rest\View(serializerGroups={"allList"})
     *
     * @param ProductVariant|null $variant
     * @return ProductVariant[]|\Doctrine\Common\Collections\Collection
     */
    public function getRelatedList(ProductVariant $variant = null)
    {
        if (!$variant)
            ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);
        return $this->em->getRepository(ProductVariantRelated::class)->build($variant);
    }

    /**
     * @Rest\Post("/{id}/variant", name="save")
     * @Rest\View(serializerGroups={"detail"})
     *
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param Product|null $product
     */
    public function save(Request $request, ValidatorInterface $validator, Product $product = null)
    {
        if (!$product)
            ErrorMessages::message(ErrorMessages::PRODUCT_NOT_FOUND);

        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data)
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);

        $productVariant = new ProductVariant();
        $productVariant->setName($data['name']);
        $productVariant->setMain(false);
        $productVariant->setEnabled(true);
        $productVariant->setSort(0);

        /** @var StockStatus $actualStockStatus */
        $actualStockStatus = $this->em->getRepository(StockStatus::class)->findOneBy(['slug' => 'in-stock']);
        if ($actualStockStatus) {
            $productVariant->setActualStockStatus($actualStockStatus);
        }

        /** @var StockStatus $outOfStockStatus */
        $outOfStockStatus = $this->em->getRepository(StockStatus::class)->findOneBy(['slug' => 'sold-out']);
        if ($outOfStockStatus) {
            $productVariant->setOutOfStockStatus($outOfStockStatus);
        }

        if ($product->getProductVariants()->count() < 1)
            $productVariant->setMain(true);
        else
            $productVariant->setMain(false);

        if (isset($data['sort']))
            $productVariant->setSort($data['sort']);

        $valid = BaseController::validate($productVariant, $validator);
        if (!empty($valid))
            return $valid;

        $product->addProductVariant($productVariant);
        $this->em->persist($productVariant);
        $this->em->flush();

        $translation = new ProductVariantTranslation();
        $translation->setName($data['name']);
        $translation->setSlug($product->getTranslations()[$this->getDefaultLocale()]->getSlug() . '-' . $data['slug']);
        $translation->setLocale($this->getDefaultLocale());
        $translation->setProductVariant($productVariant);
        $this->em->persist($translation);
        $this->em->flush();

        return $productVariant;
    }

    /**
     * @Rest\Delete("/variant/{id}", name="delete")
     * @Rest\View(statusCode=204)
     * @param ProductVariant|null $productVariant
     */
    public function delete(ProductVariant $productVariant = null)
    {
        if (!$productVariant)
            ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);
        if ($productVariant->getMain())
            ErrorMessages::message(ErrorMessages::MAIN_VARIANT_DELETE);
        $this->em->remove($productVariant);
        $this->em->flush();
    }

    /**
     * @Rest\Post("/variant/{id}/flag", name="add_flag")
     * @Rest\View(serializerGroups={"list"})
     * @param Request $request
     * @param ProductVariant|null $productVariant
     */
    public function addFlags(Request $request, ProductVariant $productVariant = null)
    {
        if (!$productVariant)
            ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);

        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data)
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        if ($productVariant->getFlagsObj()) {
            foreach ($productVariant->getFlagsObj() as $flag) {
                $productVariant->removeFlag($flag);
            }
            $this->em->flush();
        }
        if ($data['flagOverWrite'] == false) {
            $productVariant->setFlagOverWrite(false);
            $this->em->flush();
            return $productVariant->getProduct()->getFlags();
        } elseif ($data['flagOverWrite'] == true) {
            $productVariant->setFlagOverWrite(true);
            foreach ($data['flags'] as $item) {
                $flag = $this->getDoctrine()->getRepository(Flag::class)->find($item);
                $flag->addProductVariant($productVariant);
            }
            $this->em->flush();
            return $productVariant->getFlagsObj()->getValues();
        }
    }

    /**
     * @Rest\Get("/variant/{id}/flag", name="list_flag")
     * @Rest\View(serializerGroups={"list"})
     *
     * @param ProductVariant $productVariant
     */
    public function flagList(ProductVariant $productVariant)
    {
        if (!$productVariant)
            ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);
        return $productVariant->getFlags();
    }

    /**
     * @Rest\Post("/variant/{id}/sign", name="add_sign")
     * @Rest\View(serializerGroups={"list"})
     * @param Request $request
     * @param ProductVariant|null $productVariant
     */
    public function addSigns(Request $request, ProductVariant $productVariant = null)
    {
        if (!$productVariant)
            ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);

        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data)
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        if ($productVariant->getSignsObj()) {
            foreach ($productVariant->getSignsObj() as $sign) {
                $productVariant->removeSign($sign);
            }
            $this->em->flush();
        }
        if ($data['signOverWrite'] == false) {
            $productVariant->setSignOverWrite(false);
            $this->em->flush();
            return $productVariant->getProduct()->getSigns();
        } elseif ($data['signOverWrite'] == true) {
            $productVariant->setSignOverWrite(true);
            foreach ($data['signs'] as $item) {
                $sign = $this->getDoctrine()->getRepository(Sign::class)->find($item);
                $sign->addProductVariant($productVariant);
            }
            $this->em->flush();
            return $productVariant->getSignsObj()->getValues();
        }
    }

    /**
     * @Rest\Get("/variant/{id}/sign", name="list_sign")
     * @Rest\View(serializerGroups={"list"})
     *
     * @param ProductVariant $productVariant
     */
    public function signList(ProductVariant $productVariant)
    {
        if (!$productVariant)
            ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);
        return $productVariant->getSigns();
    }

    /**
     * @Rest\Post("/variant/{id}/copy", name="copy")
     * @Rest\View(serializerGroups={"detail"})
     *
     * @param ProductVariant $productVariant
     * @return ProductVariant
     */
    public function copy(ProductVariant $productVariant)
    {
        $newProductVariant = clone $productVariant;
        $this->em->detach($newProductVariant);
        $this->em->persist($newProductVariant);
        $this->em->flush();
        return $newProductVariant;
    }


    /**
     * @Rest\Get("/variant/{id}/required-parameters", name="check_required_parameters")
     * @Rest\View(serializerGroups={"detail"})
     *
     * @param ProductVariant $productVariant
     * @param ParameterRepository $parameterRepository
     * @return array
     */
    public function checkReuqiredParameters(ProductVariant $productVariant, ParameterRepository $parameterRepository)
    {
        if (!$productVariant)
            ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);
        $parameters = [];
        $product = $productVariant->getProduct();
        if ($product->getVariantsDefinedByColor()) {
            $colorParameter = $parameterRepository->findByTranlationSlug(ParameterRepository::COLOR_SLUG);
            if (!$colorParameter) {
                $colorParameter = $this->createParameter('Barva', Parameter::TYPE_COLOR, ParameterRepository::COLOR_SLUG);
            }
            $variantColorParameter = $this->em->getRepository(ProductVariantParameter::class)->findOneBy(['parameter' => $colorParameter, 'productVariant' => $productVariant]);
            if (!$variantColorParameter) {
                $parameters[] = $colorParameter;
            }
        }
        if ($product->getVariantsDefinedBySize()) {
            $sizeParameter = $parameterRepository->findByTranlationSlug(ParameterRepository::SIZE_SLUG);
            if (!$sizeParameter) {
                $sizeParameter = $this->createParameter('Velikost', Parameter::TYPE_CHOICE, ParameterRepository::SIZE_SLUG);
            }
            $variantSizeParameter = $this->em->getRepository(ProductVariantParameter::class)->findOneBy(['parameter' => $sizeParameter, 'productVariant' => $productVariant]);
            if (!$variantSizeParameter) {
                $parameters[] = $sizeParameter;
            }
        }
        return $parameters;
    }

    private function createParameter($name, $type, $slug) {
        $parameter = new Parameter();
        $parameter->load(['name' => $name, 'type' => $type, 'sort' => 0]);
        $parameterTranslation = new ParameterTranslation();
        $parameterTranslation->load('cs', ['name' => $name, 'slug' => $slug, 'prefix' => '', 'suffix' => '']);
        $parameter->addTranslation($parameterTranslation);
        $this->em->persist($parameterTranslation);
        $this->em->persist($parameter);
        $this->em->flush();
        return $parameter;
    }

    /**
     * @Rest\Post("/variant/{id}/price", name="add_price")
     * @Rest\View(serializerGroups={"detail"})
     *
     * @param Request $request
     * @param ProductVariant|null $productVariant
     */
    public function addPrice(Request $request, ProductVariant $productVariant = null)
    {
        if (!$productVariant)
            ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);

        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data)
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        $vat = $this->getDoctrine()->getRepository(Vat::class)->find($data['vatId']);
        if (!$vat)
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Vat with specified id not found');

        // PERSPEKTIVA: jine meny nez koruny
        if (isset($data['currencyId'])) {
            $currency = $this->getDoctrine()->getRepository(Currency::class)->find($data['currencyId']);
            if (!$currency)
                ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);
        } else {
            // koruna
            $currency = $this->getDoctrine()->getRepository(Currency::class)->find(1);
        }
        if (!$currency){
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Currency not found');
        }
        //nacteni dat - problem symfony
        $currency->getId();

        $price = new Price();

        $price->load($data, $currency, $vat);

        foreach ($productVariant->getPriceObj() as $item){
            $productVariant->removePrice($item);
            $this->em->remove($item);
        }
        $this->em->flush();

        $price->setProductVariant($productVariant);

        $this->em->persist($price);
        $this->em->flush();
        return $price;
    }
    /**
     * @Rest\Put("/variant/update/sort", name="update_variants_sort")
     * @Rest\View(serializerGroups={"detail"})
     *
     * @param Request $request
     */
    public function updateVariantsSort(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data)
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        foreach ($data as $item) {
            /** @var ProductVariant $variant */
            $variant = $this->em->getRepository(ProductVariant::class)->find($item['id']);
            if (!$variant)
                ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);
            $variant->setSort($item['sort']);
        }
        $this->em->flush();
        return $data;
    }

}
