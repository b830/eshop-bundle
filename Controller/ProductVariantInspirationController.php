<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\ProductInspiration;
use App\Akip\EshopBundle\Entity\ProductVariant;
use App\Akip\EshopBundle\Entity\ProductVariantInspiration;
use App\Akip\EshopBundle\Repository\ProductInspirationRepository;
use App\Akip\EshopBundle\Repository\ProductVariantInspirationRepository;
use App\Akip\FileManagerBundle\Entity\File;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductInspirationController
 * @package App\Akip\EshopBundle\Controller
 * @Route("/api/product/variant/", name="product_variant_inspiration")
 */
class ProductVariantInspirationController extends BaseController
{
    private $em;

    /**
     * ProductInspirationController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Rest\Get("{id}/inspiration", name="list")
     * @Rest\View(serializerGroups={"list"})
     * @param ProductInspirationRepository $repository
     */
    public function list(ProductVariantInspirationRepository $repository, ProductVariant $productVariant = null)
    {
        if (!$productVariant)
            ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);
        $data['inspirationOverWrite'] = $productVariant->getInspirationOverWrite();
        $data['files'] = $productVariant->getInspirationOverWrite() === false ? $this->em->getRepository(ProductInspiration::class)->build($productVariant->getProduct()) : $repository->build($productVariant);
        return $data;
    }

    /**
     * @Rest\Post("{id}/inspiration", name="save")
     * @Rest\View(serializerGroups={"list"})
     */
    public function save(Request $request, ProductVariant $productVariant = null)
    {
        if (!$productVariant)
            ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);

        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data)
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);

        if (isset($data['inspirationOverWrite'])){
            $productVariant->setInspirationOverWrite($data['inspirationOverWrite']);
            $this->em->flush();
            if ($productVariant->getInspirationOverWrite() === false){
                return $this->list($this->em->getRepository(ProductVariantInspiration::class), $productVariant);
            }
        }

        $inspirations = $this->em->getRepository(ProductVariantInspiration::class)->findBy(['productVariant' => $productVariant]);
        foreach ($inspirations as $inspiration){
            $this->em->remove($inspiration);
        }
        $this->em->flush();
        foreach ($data['files'] as $item) {
            $file = $this->getDoctrine()->getRepository(File::class)->find($item['uuid']);

            if (!$file) {
                ErrorMessages::message(ErrorMessages::PHOTO_NOT_FOUND);
            }
            $productVariantInspiration = new ProductVariantInspiration();
            $productVariantInspiration->setProductVariant($productVariant);
            $productVariantInspiration->setFile($file);
            $productVariantInspiration->setSort($item['sort']);
            $this->em->persist($productVariantInspiration);
            $this->em->flush();
        }
        return $this->list($this->em->getRepository(ProductVariantInspiration::class), $productVariant);
    }
}
