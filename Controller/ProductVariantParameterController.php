<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Parameter;
use App\Akip\EshopBundle\Entity\ParameterValue;
use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\ProductParameter;
use App\Akip\EshopBundle\Entity\ProductVariant;
use App\Akip\EshopBundle\Entity\ProductVariantParameter;
use App\Akip\EshopBundle\Repository\ParameterRepository;
use App\Akip\EshopBundle\Repository\ProductVariantParameterRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @Rest\Route("/api/product/variant", name="product_variant_parameter_")
 *
 * Class ProductVariantParameterController
 * @package App\Akip\EshopBundle\Controller
 */
class ProductVariantParameterController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ProductVariantParameterController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Rest\Get("/{id}/parameter", name="list")
     * @Rest\View(serializerGroups={"list"})
     *
     * @param ProductVariantParameterRepository $repository
     * @param ProductVariant $variant
     */
    public function list(ProductVariantParameterRepository $repository, ProductVariant $variant = null)
    {
        if (!$variant)
            ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);

        $parameters = $variant->uniqueParameters();
        $validParams = [];

        if (!$parameters)
            return [];
            
        foreach ($parameters as $parameter){
            $p = $this->em->getRepository(Parameter::class)->findOneBy(['id' => $parameter->getId()]);
            if ($p){
                $validParams[] = $p;
            }
        }
        if (!$validParams)
            return [];
        foreach ($parameters as $parameter) {
            $data[] = $repository->findByProductVariantAndParameter($variant, $parameter);
        }
        usort($data, function ($item1, $item2) {
            return $item1['sort'] <=> $item2['sort'];
        });
        return $data;
    }

    /**
     * @Rest\Put("/{idVariant}/parameter/{idParameter}", name="update")
     * @Rest\View(serializerGroups={"list"})
     *
     * @param ProductVariantParameterRepository $repository
     * @param Request $request
     * @param int $idVariant
     * @param int $idParameter
     * @return mixed
     */
    public function update(ProductVariantParameterRepository $repository, Request $request, int $idVariant, int $idParameter)
    {
        $param = $this->getDoctrine()->getRepository(Parameter::class)->findOneBy(['id' => $idParameter]);
        if (!$param)
            ErrorMessages::message(ErrorMessages::PARAMETER_NOT_FOUND);

        $variant = $this->getDoctrine()->getRepository(ProductVariant::class)->findOneBy(['id' => $idVariant]);
        if (!$variant)
            ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);

        // $parameter = class ProductVariantParameter
        $parameter = $repository->findBy(['productVariant' => $idVariant, 'parameter' => $idParameter]);

        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data)
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);


        if ($parameter) {
            if (is_array($parameter)) {
                foreach ($parameter as $item)
                    $this->em->remove($item);
            } else {
                $this->em->remove($parameter);
            }
            $this->em->flush();
        }

        $this->loadEntity($param, $data, $variant);
        $this->checkUniqueDefineParameterValues($variant->getProduct(), $variant, $param, $data);
        $this->em->flush();
        return $repository->findByProductVariantAndParameter($variant, $param);
    }

    /**
     * @Rest\Post("/{idVariant}/parameter/{idParameter}", name="add")
     * @Rest\View(serializerGroups={"list"})
     *
     * @param ProductVariantParameterRepository $repository
     * @param Request $request
     * @param int $idVariant
     * @param int $idParameter
     */
    public function save(ProductVariantParameterRepository $repository, Request $request, int $idVariant, int $idParameter)
    {
        /** @var Parameter $param */
        $param = $this->getDoctrine()->getRepository(Parameter::class)->findOneBy(['id' => $idParameter]);
        if (!$param)
            ErrorMessages::message(ErrorMessages::PARAMETER_NOT_FOUND);

        /** @var ProductVariant $variant */
        $variant = $this->getDoctrine()->getRepository(ProductVariant::class)->findOneBy(['id' => $idVariant]);
        if (!$variant)
            ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);

        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data)
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);



        $parameter = $repository->findBy(['productVariant' => $idVariant, 'parameter' => $idParameter]);
        if ($parameter) {
            if (is_array($parameter)) {
                foreach ($parameter as $item)
                    $this->em->remove($item);
            } else {
                $this->em->remove($parameter);
            }
            $this->em->flush();
        }


//        $this->checkUniqueDefineParameterValues($variant->getProduct(), $param, $data);

        $this->loadEntity($param,$data, $variant);
        $this->checkUniqueDefineParameterValues($variant->getProduct(), $variant, $param, $data);
        $this->em->flush();
        return $repository->findByProductVariantAndParameter($variant, $param);

    }

    private function checkUniqueDefineParameterValues(Product $product,ProductVariant $productVariant, Parameter $parameter, $data)
    {
        if (!in_array($parameter->getType(), [Parameter::TYPE_CHOICE, Parameter::TYPE_COLOR])) {
            return;
        }
        $params = [];

        /** @var ParameterValue $paramValue */
        $parameterValue = $this->getDoctrine()->getRepository(ParameterValue::class)->find($data['values']);

        /** @var ParameterRepository $parameterRepository */
        $parameterRepository = $this->em->getRepository(Parameter::class);

        /** @var ProductVariantParameterRepository $productVariantParameterRepository */
        $productVariantParameterRepository = $this->em->getRepository(ProductVariantParameter::class);

        $colorParameter = $parameterRepository->findByTranlationSlug(ParameterRepository::COLOR_SLUG);
        $sizeParameter = $parameterRepository->findByTranlationSlug(ParameterRepository::SIZE_SLUG);

        if ($parameter !== $colorParameter && $parameter !== $sizeParameter) {
            return;
        }
        // only color parameter
        if ($product->getVariantsDefinedByColor() && !$product->getVariantsDefinedBySize()) {
            if ($parameter === $colorParameter) {
                $params = $productVariantParameterRepository->findAllVariantParameters($product, $colorParameter, $productVariant, $parameterValue);
            }
            if (count($params) > 0 ) {
                ErrorMessages::message(ErrorMessages::COMBINATION_ALREADY_EXISTS, '' ," {$parameterValue->getName()}");
            }
        }
        // only size parameter
        elseif (!$product->getVariantsDefinedByColor() && $product->getVariantsDefinedBySize()) {
            if ($parameter === $sizeParameter) {
                $params = $productVariantParameterRepository->findAllVariantParameters($product, $sizeParameter, $productVariant, $parameterValue);
            }
            if (count($params) > 0 ) {
                ErrorMessages::message(ErrorMessages::COMBINATION_ALREADY_EXISTS, '' , " {$parameterValue->getName()}");
            }
        }
        // color and size parameter
        elseif ($product->getVariantsDefinedByColor() && $product->getVariantsDefinedBySize()) {
            if ($parameter === $colorParameter) {
                $colorVariantParameter = $productVariantParameterRepository->findOneBy(['productVariant' => $productVariant, 'parameter' => $sizeParameter]);
                $sizeVariantParams = $productVariantParameterRepository->findAllVariantParametersByProductAndVariant($product,$sizeParameter, $productVariant);
                if ($sizeVariantParams !== null && count($sizeVariantParams) > 0) {
                    /** @var ProductVariantParameter $sizeItem */
                    $values = [];
                    foreach ($sizeVariantParams as $sizeItem) {
                        if ($sizeItem !== null && $sizeItem->getParameterValue() !== null && $colorVariantParameter !== null && $sizeItem->getParameterValue()->getId() === $colorVariantParameter->getParameterValue()->getId()) {
                            $colorItem = $productVariantParameterRepository->findOneBy(['parameter' => $sizeParameter, 'productVariant' => $sizeItem->getProductVariant()]);
                            if ($colorItem !== null && $colorItem->getParameterValue() !== null && $colorItem->getParameterValue()->getId() === $parameterValue->getId() && $sizeItem->getProductVariant() !== $productVariant) {
                                ErrorMessages::message(ErrorMessages::COMBINATION_ALREADY_EXISTS, '', " {$colorItem->getParameterValue()->getName()} - {$sizeItem->getParameterValue()->getName()}");
                            }
                        }
//                        $values[] = $sizeItem->getParameterValue()->getId();
                    }
//                    if ($this->arrayContainsDuplicity($values)) {
//                        ErrorMessages::message(ErrorMessages::COMBINATION_ALREADY_EXISTS, 'xxxxx');
//                    }
                }
            } elseif ($parameter === $sizeParameter) {
                $colorVariantParameter = $productVariantParameterRepository->findOneBy(['productVariant' => $productVariant, 'parameter' => $colorParameter]);
                $colorVariantParams = $productVariantParameterRepository->findAllVariantParametersByProductAndVariant($product,$colorParameter , $productVariant);
                if ($colorVariantParams !== null && count($colorVariantParams) > 0) {
                    /** @var ProductVariantParameter $colorItem */
                    $values = [];
                    foreach ($colorVariantParams as $colorItem) {
                        if ($colorItem === null) {
                            die($colorItem->getId());
                        }
                        if ($colorItem  !== null && $colorItem->getParameterValue() !== null && $colorVariantParameter !== null && $colorItem->getParameterValue()->getId() === $colorVariantParameter->getParameterValue()->getId()) {
                            $sizeItem = $productVariantParameterRepository->findOneBy(['parameter' => $sizeParameter, 'productVariant' => $colorItem->getProductVariant()]);
                            if ($sizeItem !== null && $sizeItem->getParameterValue() !== null && $sizeItem->getParameterValue()->getId() === $parameterValue->getId() && $colorItem->getProductVariant() !== $productVariant) {
                                ErrorMessages::message(ErrorMessages::COMBINATION_ALREADY_EXISTS, ''," {$colorItem->getParameterValue()->getName()} - {$sizeItem->getParameterValue()->getName()}");
                            }
                        }
//                        $values[] = $colorItem->getParameterValue()->getId();
                    }
                    // check if array contains duplicity values
//                    if ($this->arrayContainsDuplicity($values)) {
//                        ErrorMessages::message(ErrorMessages::COMBINATION_ALREADY_EXISTS, 'zzzzzz');
//                    }
                }
            }
        }
    }

    private function arrayContainsDuplicity($array) {
        return count($array) !== count(array_unique($array, SORT_NUMERIC));
    }

    private function loadEntity(Parameter $param, $data, ProductVariant $variant = null)
    {
        if (in_array($param->getType(), [Parameter::TYPE_CHOICE, Parameter::TYPE_MULTI_CHOICE, Parameter::TYPE_COLOR])) {
            if (($param->getType() === Parameter::TYPE_CHOICE || $param->getType() === Parameter::TYPE_COLOR) && is_array($data['values'])){
                if (count($data['values']) > 1){
                    throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, "For type [{$param->getType()}] only 1 value is acceptable");
                }
            }
            if (is_array($data['values'])) {
                foreach ($data['values'] as $valueId) {
                    /** @var ParameterValue $paramValue */
                    $paramValue = $this->getDoctrine()->getRepository(ParameterValue::class)->find($valueId);

                    $this->check($paramValue, $param);

                    $parameter = new ProductVariantParameter();
                    $parameter->load($param, $variant, $data['sort'], $paramValue);
                    $this->em->persist($parameter);
                    $this->em->flush();
                }
            } else {
                /** @var ParameterValue $paramValue */
                $paramValue = $this->getDoctrine()->getRepository(ParameterValue::class)->find($data['values']);

                $this->check($paramValue, $param);

                $parameter = new ProductVariantParameter();

                $parameter->load($param, $variant, $data['sort'], $paramValue);

                $this->em->persist($parameter);
                $this->em->flush();
            }
        } else {
            $parameter = new ProductVariantParameter();
            $parameter->load($param, $variant, $data['sort'], null, $data['values']);
            $this->em->persist($parameter);
            $this->em->flush();
        }
    }

    // kontrola zda Parameter Value existuje a je v listu hodnot
    private function check($paramValue, $parameter)
    {
        if (!$paramValue)
            ErrorMessages::message(ErrorMessages::PARAMETER_VALUE_NOT_FOUND);
        if (!$parameter->getValues()->contains($paramValue))
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Parameter value with specified id in parameter values not found');
    }

    /**
     * @Rest\Delete("/{idVariant}/parameter/{idParameter}", name="delete")
     * @Rest\View(StatusCode = 204)
     *
     * @param ProductVariantParameterRepository $repository
     * @param $idVariant
     * @param $idParameter
     */
    public function delete(ProductVariantParameterRepository $repository, $idVariant, $idParameter)
    {
        $param = $this->getDoctrine()->getRepository(Parameter::class)->findOneBy(['id' => $idParameter]);
        if (!$param)
            ErrorMessages::message(ErrorMessages::PARAMETER_NOT_FOUND);

        $variant = $this->getDoctrine()->getRepository(ProductVariant::class)->findOneBy(['id' => $idVariant]);
        if (!$variant)
            ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);

        // $parameter = class ParameterVariantValue
        $parameter = $repository->findBy(['productVariant' => $idVariant, 'parameter' => $idParameter]);
        if ($parameter) {
            foreach ($parameter as $item) {
                $this->em->remove($item);
            }
            $this->em->flush();
        }
    }
}

