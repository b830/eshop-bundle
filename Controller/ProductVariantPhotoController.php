<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\ProductPhoto;
use App\Akip\EshopBundle\Entity\ProductVariant;
use App\Akip\EshopBundle\Entity\ProductVariantInspiration;
use App\Akip\EshopBundle\Entity\ProductVariantPhoto;
use App\Akip\EshopBundle\Entity\ProductVariantPhotoTranslation;
use App\Akip\EshopBundle\Repository\ProductPhotoRepository;
use App\Akip\EshopBundle\Repository\ProductVariantPhotoRepository;
use App\Akip\FileManagerBundle\Entity\File;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;


/**
 * Class ProductVariantPhotoController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/product/variant/", name="product_variant_photo_")
 */
class ProductVariantPhotoController extends BaseController
{
    private $em;

    /**
     * ProductPhotoController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Rest\Get("{id}/photo", name="list")
     * @Rest\View(serializerGroups={"list"})
     * @param ProductVariantPhoto $repository
     * @param ProductVariant $variant
     */
    public function list(ProductVariantPhotoRepository $repository, ProductVariant $variant = null)
    {
        if (!$variant)
            ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);
//        if ($variant->getPhotos()->isEmpty() and $variant->getPhotoOverWrite()){
//            return [];
//        }
//        $fileRepository = $this->em->getRepository(File::class);
//        return $variant->getPhotoOverWrite();
        $data['photoOverWrite'] = $variant->getPhotoOverWrite();
//        $data['files'] = $variant->getPhotoOverWrite() === false ? $this->em->getRepository(ProductPhoto::class)->build($fileRepository ,$variant->getProduct()) : $repository->build($fileRepository, $variant);
        $data['files'] = $repository->build($this->em->getRepository(File::class), $variant);
        return $data;
//        return $repository->findBy(['product' => $variant], ['sort' => 'ASC']);
    }

    /**
     * @Rest\Post("{id}/photo", name="save")
     * @Rest\View(serializerGroups={"detail"})
     *
     * @param Request $request
     * @param ProductVariant $variant
     * @return array
     */
    public function save(Request $request, ProductVariant $variant = null)
    {
        if (!$variant)
            ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }

        if (isset($data['photoOverWrite'])){
            $variant->setPhotoOverWrite($data['photoOverWrite']);
            $this->em->flush();
            if ($variant->getPhotoOverWrite() === false){
                return $this->list($this->em->getRepository(ProductVariantPhoto::class), $variant);
            }
        }

        $mainCount = 0;
        $photos = array();
        $translations = array();
        $oldPhotos = $variant->getPhotos();
        foreach ($data['files'] as $item) {
            /** @var File $photo */
            $photo = $this->em->getRepository(File::class)->find($item['uuid']);
            if (!$photo)
                throw new HttpException(Response::HTTP_NOT_FOUND, 'Photo with specified uuid not found');
            $productPhoto = new ProductVariantPhoto();

            $productPhoto->setName($photo->getOriginalName());
            $productPhoto->setMain($item['main']);
            $productPhoto->setPhoto($photo);
            $productPhoto->setProductVariant($variant);
            $productPhoto->setSort($item['sort']);
            if (isset($item['translations'])){
                foreach ($item['translations'] as $locale => $translation) {
                    $productPhotoTranslation = new ProductVariantPhotoTranslation();
                    $productPhotoTranslation->load($translation, $locale, $productPhoto);
                    $productPhoto->addTranslation($productPhotoTranslation);
                }
//                array_push($translations, $productPhotoTranslation);
            }
            if ($productPhoto->getMain())
                $mainCount++;
            array_push($photos, $productPhoto);
        }
        if ($mainCount > 1) {
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Only 1 photo can be main');
        } elseif ($mainCount == 0 && count($photos) > 0) {
            $photos[0]->setMain(true);
        }

        /** @var ProductVariantPhoto $item */
        foreach ($oldPhotos as $item){
            foreach ($item->getTranslationsObj() as $translation) {
                $this->em->remove($translation);
            }
            $this->em->remove($item);
        }
        $this->em->flush();

        foreach ($photos as $photo) {
            $variant->addPhoto($photo);
            $this->em->persist($photo);
        }
        $this->em->flush();
        return $this->list($this->em->getRepository(ProductVariantPhoto::class), $variant);
    }

    /**
     * @Rest\Put("{id}/photo", name="update")
     * @Rest\View(serializerGroups={"list"})
     *
     * @param Request $request
     * @param ProductVariant $variant
     */
    public function update(Request $request, ProductVariant $variant = null)
    {
        if (!$variant)
            ErrorMessages::message(ErrorMessages::PRODUCT_VARIANT_NOT_FOUND);
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        foreach ($data as $item) {
            $photo = $this->em->getRepository(File::class)->find($item['uuid']);
            if (!$photo)
                ErrorMessages::message(ErrorMessages::PHOTO_NOT_FOUND);
            $variantPhoto = new ProductVariantPhoto();

            $variantPhoto->setName($photo->getOriginalName());
            $variantPhoto->setMain($item['main']);
            $variantPhoto->setPhoto($photo);
            $variantPhoto->setProductVariant($variant);

            if ($variant->checkPhotoExist($variantPhoto)) {
                continue;
            }
        }
    }

    /**
     * @Rest\Get("photo/{id}", name="detail")
     * @Rest\View(serializerGroups={"detail"})
     * @param ProductPhoto $variantPhoto
     */
    public function getPhoto(ProductVariantPhoto $variantPhoto = null)
    {
        if (!$variantPhoto)
            ErrorMessages::message(ErrorMessages::NOT_FOUND);
        return $variantPhoto;
    }

    /**
     * @Rest\Delete("photo/{id}", name="delete")
     * @Rest\View(statusCode=204)
     * @param ProductPhoto $variantPhoto
     */
    public function delete(ProductVariantPhoto $variantPhoto = null)
    {
        if (!$variantPhoto)
            ErrorMessages::message(ErrorMessages::NOT_FOUND);
        $this->em->remove($variantPhoto);
        $this->em->flush();
    }
}
