<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\EshopBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\ProductTranslation;
use App\Akip\EshopBundle\Entity\ProductVariant;
use App\Akip\EshopBundle\Entity\ProductVariantTranslation;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ProductVariantTranslationController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/product/variant", name="product_variant_translation_")
 */
class ProductVariantTranslationController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ProductVariantTranslationController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Rest\Get("/{id}/translation", name="list")
     * @Rest\View(serializerGroups={"list"})
     * @param ProductVariant|null $productVariant
     * @return array
     */
    public function list(ProductVariant $productVariant = null)
    {
        if (!$productVariant)
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Product variant with specified id not found');
        return $productVariant->getTranslations();
    }

    /**
     * @Rest\Post("/{id}/translation", name="add")
     * @Rest\View(serializerGroups={"detail"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param ProductVariant|null $productVariant
     */
    public function save(Request $request, ValidatorInterface $validator, ProductVariant $productVariant = null)
    {
        if (!$productVariant)
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Product with specified id not found');

        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data)
            throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, 'Empty body');

        $this->checkLocale(array_keys($data));

        $translations = array();
        $productTranslation = $productVariant->getProduct()->getTranslations();
        foreach ($data as $key => $item) {
            $translation = new ProductVariantTranslation();
            $translation->load($key, $item);
//            $delimiter = $translation->getSlug() !== '' ? '-' : '';
//            $productSlug = array_key_exists($key, $productTranslation) ? $productTranslation[$key]->getSlug() : $productTranslation['cs']->getSlug();
//            $translation->setSlug($productSlug . $delimiter . $translation->getSlug());
            $translations[] = $translation;
        }

        if($productVariant->getTranslationsObj()){
            foreach ($productVariant->getTranslationsObj() as $translation){
                $productVariant->removeTranslation($translation);
            }
            $this->em->flush();
        }



        foreach ($translations as $translation) {
//            if ($this->getDoctrine()->getRepository(ProductTranslation::class)->findOneBy(['product' => $product, 'locale' => $key]))
//                throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, "Translation with locale [{$key}] for this product already exists");
            $testSlug = $this->em->getRepository(ProductVariantTranslation::class)->findOneBy(['slug' => $translation->getSlug()]);
            if ($testSlug && $testSlug->getSlug() !== '') {
                ErrorMessages::message(ErrorMessages::SLUG_EXISTS);
            }
            $productVariant->addTranslation($translation);
            $this->em->persist($translation);
            $this->em->flush();
        }
        return $productVariant->getTranslations();
    }

    /**
     * @Rest\Put("/translation/{id}", name="update")
     * @Rest\View(serializerGroups={"detail"})
     *
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param ProductVariantTranslation $translation
     */
    public function update(Request $request, ValidatorInterface $validator, ProductVariantTranslation $translation)
    {
        if (!$translation)
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Product variant translation with specified id not found');

        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data)
            throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, 'Empty body');
        $this->checkLocale(array_keys($data));
        foreach ($data as $key => $item) {
            $translation->load($key, $item);
            if($key !== $translation->getLocale()) {
                if ($this->getDoctrine()->getRepository(ProductVariantTranslation::class)->findOneBy(['productVariant' => $translation->getProductVariant(), 'locale' => $key]))
                    throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, "Translation with locale [{$key}] for this product already exists");
            }
            $valid = BaseController::validate($translation, $validator);
            if (!empty($valid))
                return $valid;
            $this->em->persist($translation);
            $this->em->flush();
        }
        return $translation->build();
    }
}
