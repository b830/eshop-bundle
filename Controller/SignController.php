<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\EshopBundle\Entity\Flag;
use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\Sign;
use App\Akip\EshopBundle\Repository\FlagRepository;
use App\Akip\EshopBundle\Repository\SignRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class SignController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/sign", name="sign_")
 */
class SignController extends AbstractEntityController
{
    /**
     * @var EntityManagerInterface
     */
    public $em;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * CountryController constructor.
     * @param SignRepository $repository
     * @param EntityManagerInterface $em
     * @param ValidatorInterface $validator
     */
    public function __construct(SignRepository $repository, EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $this->repository = $repository;
        $this->entity = new Sign();
        $this->em = $em;
        $this->validator = $validator;
    }

}
