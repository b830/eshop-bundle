<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\EshopBundle\Entity\Flag;
use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\StockStatus;
use App\Akip\EshopBundle\Repository\FlagRepository;
use App\Akip\EshopBundle\Repository\StockStatusRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class StockStatusController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/stock-status", name="stock-status_")
 */
class StockStatusController extends AbstractEntityController
{
    /**
     * @var EntityManagerInterface
     */
    public $em;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * CountryController constructor.
     * @param StockStatusRepository $stockStatusRepository
     * @param EntityManagerInterface $em
     * @param ValidatorInterface $validator
     */
    public function __construct(StockStatusRepository $stockStatusRepository, EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $this->repository = $stockStatusRepository;
        $this->entity = new StockStatus();
        $this->em = $em;
        $this->validator = $validator;
    }

}
