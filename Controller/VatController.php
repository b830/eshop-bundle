<?php

namespace App\Akip\EshopBundle\Controller;

use App\Akip\EshopBundle\Entity\Vat;
use App\Akip\EshopBundle\Repository\VatRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class VatController
 * @package App\Akip\EshopBundle\Controller
 * @Rest\Route("/api/vat", name="vat_")
 */
class VatController extends AbstractEntityController
{
    /**
     * @var EntityManagerInterface
     */
    public $em;

    /**
     * VatController constructor.
     * @param VatRepository $vatRepository
     * @param EntityManagerInterface $em
     */
    public function __construct(VatRepository $vatRepository, EntityManagerInterface $em)
    {
        $this->repository = $vatRepository;
        $this->em = $em;
        $this->entity = new Vat();
    }

}
