<?php


namespace App\Akip\EshopBundle\Entity;


class BankAccount extends BasePayment
{
    public function __construct()
    {
        $this->options = [
            'bank-account' => [
                'value' => '',
                'label' => 'Číslo účtu'
            ],
            'bank-code' => [
                'value' => '',
                'label' => 'Kód bánky'
            ],
            'bank-name' => [
                'value' => '',
                'label' => 'Název banky'
            ]
        ];
    }
}
