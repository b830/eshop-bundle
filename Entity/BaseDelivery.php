<?php


namespace App\Akip\EshopBundle\Entity;


class BaseDelivery
{
    public $options = [];
    public $heurekaSlug = '';
    public $zboziSlug = '';
    public $isAddressRequired = true;

    public function buildForm(&$form)
    {

    }

    /**
     * @return string
     */
    public function getButton($options,$data): string
    {
        return '';
    }

    public function checkFields(&$data)
    {
        foreach ($data as $key => $item) {
            if (!isset($this->options[$key])) {
                unset($data[$key]);
            }
        }
        foreach ($this->options as $key => $item) {
            if (!isset($data[$key])) {
                $data[$key] = $item;
            }
        }
    }
}
