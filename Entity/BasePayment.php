<?php


namespace App\Akip\EshopBundle\Entity;


class BasePayment
{
    public $options = [];

    public function buildForm(&$form)
    {

    }

    public function checkFields(&$data)
    {
        foreach ($data as $key => $item) {
            if (!isset($this->options[$key])) {
                unset($data[$key]);
            }
        }
        foreach ($this->options as $key => $item) {
            if (!isset($data[$key])) {
                $data[$key] = $item;
            }
        }
    }
}
