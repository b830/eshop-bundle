<?php

namespace App\Akip\EshopBundle\Entity;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\BrandTranslation;
use App\Akip\FileManagerBundle\Entity\File;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Gedmo\Loggable\Loggable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="brand", uniqueConstraints={@ORM\UniqueConstraint(name="slug", columns={"slug", "deleted_at"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\BrandRepository")
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 * @UniqueEntity("slug")
 */
class Brand
{
    use SoftDeleteableEntity;
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $name;

    /**
     * @var File
     *
     * @ORM\ManyToOne(targetEntity="App\Akip\FileManagerBundle\Entity\File")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="icon", referencedColumnName="uuid", nullable=true)
     * })
     *
     */
    private $icon;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     * @Groups({"detail" , "list"})
     * @Gedmo\Versioned()
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\BrandTranslation", mappedBy="brand", orphanRemoval=true)
     */
    private $translations;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     * @Groups({"detail"})
     */
    public function getIcon()
    {
        return $this->icon ? $this->icon->getUrl() : '';
    }

    public function getIconFile(): ?File
    {
        return $this->icon;
    }

    public function setIcon(?File $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return array
     * @Groups({"detail"})
     */
    public function getTranslations()
    {
        $items = array();
        foreach ($this->translations as $translation) {
            /**
             * @var $translation BrandTranslation
             */
            $items[$translation->getLocale()] = $translation;
        }
        return $items;
    }

    /**
     * @return Collection|BrandTranslation[]
     */
    public function getTranslationsObj(): Collection
    {
        return $this->translations;
    }

    public function addTranslation(BrandTranslation $translation): self
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setBrand($this);
        }

        return $this;
    }

    public function removeTranslation(BrandTranslation $translation): self
    {
        if ($this->translations->contains($translation)) {
            $this->translations->removeElement($translation);
            // set the owning side to null (unless already changed)
            if ($translation->getBrand() === $this) {
                $translation->setBrand(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        if ($slug === '' || !$slug) {
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Slug ');
        }

        $this->slug = $slug;

        return $this;
    }

    public function load($data, $icon = null)
    {
        $this->setName($data['name']);
        $this->setSlug($data['slug']);
        $this->setIcon($icon);
    }
}
