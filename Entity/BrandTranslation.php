<?php

namespace App\Akip\EshopBundle\Entity;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BrandTranslation
 *
 * @ORM\Table(name="brand_translation", indexes={@ORM\Index(name="brand_id", columns={"brand_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\BrandTranslationRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 * @Gedmo\Loggable()
 */
class BrandTranslation
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private $id;


    /**
     * @var string
     * @Groups({"detail", "list"})
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     * @Gedmo\Versioned()
     */
    private $description;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="locale", type="string", length=10, nullable=false)
     * @Gedmo\Versioned()
     */
    private $locale;

    /**
     *
     * @ORM\Column(name="metadata", type="text", nullable=false)
     * @Groups({"detail" , "list"})
     * @Gedmo\Versioned()
     */
    private $metadata;

    /**
     * @var Brand
     *
     * @ORM\ManyToOne(targetEntity="App\Akip\EshopBundle\Entity\Brand")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brand_id", referencedColumnName="id")
     * })
     */
    private $brand;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    public function setBrand(?Brand $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getMetadata(): ?string
    {
        return $this->metadata;
    }

    public function setMetadata(string $metadata): self
    {
        $this->metadata = $metadata;

        return $this;
    }

    public function load($locale, $data, Brand $brand)
    {
        if (empty($data['description']))
            $this->setDescription('');
        else
            $this->setDescription($data['description']);
        $this->setLocale($locale);
        $this->setBrand($brand);
        if(isset($data['metadata'])) {
            $this->setMetadata($data['metadata']);
        }
        else {
            $this->setMetadata('{}');
        }
        return $this;
    }

}
