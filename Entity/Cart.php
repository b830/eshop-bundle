<?php


namespace App\Akip\EshopBundle\Entity;


class Cart
{
    private $id;
    private $name;
    private $photoDescription;
    private $priceWithoutVat;
    private $priceWithVat;
    private $quantity;

    public function getId(){
        return $this->id;
    }
    public function setId(int $id){
        $this->id = $id;
        return $id;
    }
    public function getName(){
        return $this->id;
    }
    public function setName(int $id){
        $this->id = $id;
        return $id;
    }
}
