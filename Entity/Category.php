<?php

namespace App\Akip\EshopBundle\Entity;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\FileManagerBundle\Entity\File;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Category
 *
 * @ORM\Table(name="category", indexes={@ORM\Index(name="parent_id", columns={"parent_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\CategoryRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 * @Gedmo\Loggable()
 */
class Category
{
    use SoftDeleteableEntity;
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="lft", type="integer", nullable=false)
     * @Gedmo\Versioned()
     */
    private $lft;

    /**
     * @var int
     *
     * @ORM\Column(name="rgt", type="integer", nullable=false)
     * @Gedmo\Versioned()
     */
    private $rgt;

    /**
     * @var int
     *
     * @ORM\Column(name="depth", type="integer", nullable=false)
     * @Gedmo\Versioned()
     */
    private $depth;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     * @Gedmo\Versioned()
     */
    private $enabled;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     */
    private $parent;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="main_id", referencedColumnName="id")
     * })
     */
    private $main;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Parameter", inversedBy="category")
     * @ORM\JoinTable(name="category_parameter",
     *   joinColumns={
     *     @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="parameter_id", referencedColumnName="id")
     *   }
     * )
     */
    private $parameter;


    /**
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\CategoryTranslation", mappedBy="category", orphanRemoval=true)
     */
    private $translations;

    /**
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\Category", mappedBy="main", orphanRemoval=true)
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="main_id", referencedColumnName="main_id")
     * })
     */
    private $categoryItems;

    /**
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\ProductCategory", mappedBy="category")
     */
    private $productCategory;

    /**
     * @var File
     *
     * @ORM\ManyToOne(targetEntity="App\Akip\FileManagerBundle\Entity\File")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="icon", referencedColumnName="uuid", nullable=true)
     * })
     *
     */
    private $icon;

    /**
     * @var integer
     * @ORM\Column(name="sort", type="integer", nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $sort;


    /**
     * @var string
     * @Groups({"detail", "list"})
     * @ORM\Column(name="heureka_category", type="text", length=0, nullable=false)
     * @Gedmo\Versioned()
     */
    private $heurekaCategory = '';

    /**
     * @var string
     * @Groups({"detail", "list"})
     * @ORM\Column(name="google_category", type="text", length=0, nullable=false)
     * @Gedmo\Versioned()
     */
    private $googleCategory = '';

    /**
     * @var string
     * @Groups({"detail", "list"})
     * @ORM\Column(name="zbozi_category", type="text", length=0, nullable=false)
     * @Gedmo\Versioned()
     */
    private $zboziCategory = '';

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->parameter = new \Doctrine\Common\Collections\ArrayCollection();
//        $this->product = new \Doctrine\Common\Collections\ArrayCollection();
        $this->translations = new ArrayCollection();
        $this->categoryItems = new ArrayCollection();
//        $this->products = new ArrayCollection();
        $this->productCategory = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        if ($name === '' || !$name){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Name ');
        }
        $this->name = $name;

        return $this;
    }

    public function getLft(): ?int
    {
        return $this->lft;
    }

    public function setLft(int $lft): self
    {
        $this->lft = $lft;

        return $this;
    }

    public function getRgt(): ?int
    {
        return $this->rgt;
    }

    public function setRgt(int $rgt): self
    {
        $this->rgt = $rgt;

        return $this;
    }

    public function getDepth(): ?int
    {
        return $this->depth;
    }

    public function setDepth(int $depth): self
    {
        $this->depth = $depth;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|Parameter[]
     * //     * @Groups({"detail"})
     */
    public function getParameter(): Collection
    {
        return $this->parameter;
    }

    public function addParameter(Parameter $parameter): self
    {
        if (!$this->parameter->contains($parameter)) {
            $this->parameter[] = $parameter;
        }

        return $this;
    }

    public function removeParameter(Parameter $parameter): self
    {
        if ($this->parameter->contains($parameter)) {
            $this->parameter->removeElement($parameter);
        }

        return $this;
    }

//    /**
//     * @return Collection|Product[]
//     */
//    public function getProducts(): Collection
//    {
//        return $this->product;
//    }
//
//    public function addProduct(Product $product): self
//    {
//        if (!$this->product->contains($product)) {
//            $this->product[] = $product;
//        }
//
//        return $this;
//    }
//
//    public function removeProduct(Product $product): self
//    {
//        if ($this->product->contains($product)) {
//            $this->product->removeElement($product);
//        }
//
//        return $this;
//    }

    public function getTranslation($locale)
    {
        $criteria = Criteria::create()->where(Criteria::expr()->eq('locale', $locale));
        return $this->translations->matching($criteria)->first();
    }

    /**
     * @return Collection|CategoryTranslation[]
     */
    public function getTranslations(): Collection
    {
        return $this->translations;
    }

    public function addTranslation(CategoryTranslation $translation): self
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setCategory($this);
        }

        return $this;
    }

    public function removeTranslation(CategoryTranslation $translation): self
    {
        if ($this->translations->contains($translation)) {
            $this->translations->removeElement($translation);
            // set the owning side to null (unless already changed)
            if ($translation->getCategory() === $this) {
                $translation->setCategory(null);
            }
        }

        return $this;
    }

    public function load($data, Category $mainCat, $parent = null)
    {
        $this->setName($data['name']);
        $this->setParent($parent);
        $this->setLft(0);
        $this->setRgt(0);
        $this->setDepth(0);
        $this->setMain($mainCat);
        $this->setEnabled(true);
        if (isset($data['heurekaCategory']))
            $this->setHeurekaCategory($data['heurekaCategory']);
        if (isset($data['googleCategory']))
            $this->setGoogleCategory($data['googleCategory']);
        if (isset($data['zboziCategory']))
            $this->setZboziCategory($data['zboziCategory']);

        if (isset($data['sort']))
            $this->setSort($data['sort']);
        else
            $this->setSort(0);

        return $this;
    }

    public function getMain(): ?self
    {
        return $this->main;
    }

    public function setMain(?self $main): self
    {
        $this->main = $main;

        return $this;
    }

    public function translations()
    {
        $items = array();
        if (!$this->getTranslations()->isEmpty()) {
            foreach ($this->getTranslations() as $translation) {
                $items[$translation->getLocale()] = $translation;
            }
        }
        return $items;
    }

    public function getCategoryItems()
    {
        return $this->categoryItems;
    }

    public function setCategoryItems(?self $categoryItems): self
    {
        $this->categoryItems = $categoryItems;

        return $this;
    }

    public function removeCategoryItems($categoryItem)
    {
        if ($this->translations->contains($categoryItem)) {
            $this->translations->removeElement($categoryItem);
            // set the owning side to null (unless already changed)
            if ($categoryItem->getCategory() === $this) {
                $categoryItem->setCategory(null);
            }
        }

        return $this;
    }

    public function addCategoryItem(Category $categoryItem): self
    {
        if (!$this->categoryItems->contains($categoryItem)) {
            $this->categoryItems[] = $categoryItem;
            $categoryItem->setMain($this);
        }

        return $this;
    }

    public function removeCategoryItem(Category $categoryItem): self
    {
        if ($this->categoryItems->contains($categoryItem)) {
            $this->categoryItems->removeElement($categoryItem);
            // set the owning side to null (unless already changed)
            if ($categoryItem->getMain() === $this) {
                $categoryItem->setMain(null);
            }
        }

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return Collection|ProductCategory[]
     */
    public function getProductCategory(): Collection
    {
        return $this->productCategory;
    }

    public function addProductCategory(ProductCategory $productCategory): self
    {
        if (!$this->productCategory->contains($productCategory)) {
            $this->productCategory[] = $productCategory;
            $productCategory->addCategory($this);
        }

        return $this;
    }

    public function removeProductCategory(ProductCategory $productCategory): self
    {
        if ($this->productCategory->contains($productCategory)) {
            $this->productCategory->removeElement($productCategory);
            $productCategory->removeCategory($this);
        }

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProduct(): Collection
    {
        return $this->product;
    }

    public function getSort(): ?int
    {
        return $this->sort;
    }

    public function setSort(int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    public function getIcon(): ?File
    {
        return $this->icon;
    }

    public function setIcon(?File $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return string
     */
    public function getHeurekaCategory(): string
    {
        return $this->heurekaCategory;
    }

    /**
     * @param string $heurekaCategory
     */
    public function setHeurekaCategory(string $heurekaCategory): self
    {
        $this->heurekaCategory = $heurekaCategory;
        return $this;
    }

    /**
     * @return string
     */
    public function getGoogleCategory(): string
    {
        return $this->googleCategory;
    }

    /**
     * @param string $googleCategory
     */
    public function setGoogleCategory(string $googleCategory): self
    {
        $this->googleCategory = $googleCategory;
        return $this;
    }

    /**
     * @return string
     */
    public function getZboziCategory(): string
    {
        return $this->zboziCategory;
    }

    /**
     * @param string $zboziCategory
     */
    public function setZboziCategory(string $zboziCategory): self
    {
        $this->zboziCategory = $zboziCategory;
        return $this;
    }

}
