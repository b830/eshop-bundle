<?php

namespace App\Akip\EshopBundle\Entity;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CategoryTranslation
 *
 * @ORM\Table(name="category_translation", uniqueConstraints={@ORM\UniqueConstraint(name="slug", columns={"slug", "deleted_at"})}, indexes={@ORM\Index(name="category_id", columns={"category_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\CategoryTranslationRepository")
 * @UniqueEntity("slug")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 * @Gedmo\Loggable()
 */
class CategoryTranslation
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Groups({"detail" , "list"})
     * @Gedmo\Versioned()
     */
    private $name;

    /**
     * @var string
     * @Groups({"detail", "list"})
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     * @Gedmo\Versioned()
     */
    private $description;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="locale", type="string", length=10, nullable=false)
     * @Gedmo\Versioned()
     */
    private $locale;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     * @Groups({"detail" , "list"})
     * @Gedmo\Versioned()
     */
    private $slug;

    /**
     *
     * @ORM\Column(name="metadata", type="text", nullable=false)
     * @Groups({"detail" , "list"})
     * @Gedmo\Versioned()
     */
    private $metadata;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        if ($slug === '' || !$slug){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Slug ');
        }
        $this->slug = $slug;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function load($locale, $data, Category $cat)
    {
        $this->setName($data['name']);
        if (empty($data['description']))
            $this->setDescription('');
        else
            $this->setDescription($data['description']);
        $this->setLocale($locale);
        $this->setCategory($cat);
        $this->setSlug($data['slug']);

        if(isset($data['metadata'])) {
            $this->setMetadata($data['metadata']);
        }
        else {
            $this->setMetadata('{}');
        }
        return $this;
    }

    public function getMetadata(): ?string
    {
        return $this->metadata;
    }

    public function setMetadata(string $metadata): self
    {
        $this->metadata = $metadata;

        return $this;
    }


}
