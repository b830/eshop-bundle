<?php

namespace App\Akip\EshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use function Symfony\Component\DependencyInjection\Loader\Configurator\expr;

/**
 * CurrencyController
 *
 * @ORM\Table(name="currency", uniqueConstraints={@ORM\UniqueConstraint(name="name", columns={"name"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\CurrencyRepository")
 * @UniqueEntity("name")
 */
class Currency
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"list", "detail", "demandDetail"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     * @Groups({"list", "detail", "demandDetail"})
     */
    private $name;

    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Column(name="rate", type="integer", nullable=false)
     * @Groups({"list", "detail", "demandDetail"})
     */
    private $rate;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="symbol", type="string", length=10, nullable=false)
     * @Groups({"list", "detail", "demandDetail"})
     */
    private $symbol;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="code", type="string", length=20, nullable=false)
     * @Groups({"list", "detail", "demandDetail"})
     */
    private $code;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRate(): ?int
    {
        return $this->rate;
    }

    public function setRate(int $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    public function setSymbol(string $symbol): self
    {
        $this->symbol = $symbol;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function load($data)
    {
        $this->setName($data['name']);
        $this->setRate($data['rate']);
        $this->setSymbol($data['symbol']);
        $this->setCode($data['code']);
    }

}
