<?php

namespace App\Akip\EshopBundle\Entity;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Customer
 *
 * @ORM\Table(name="customer", uniqueConstraints={@ORM\UniqueConstraint(name="email", columns={"email"})}, indexes={@ORM\Index(name="country_id", columns={"country_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\CustomerRepository")
 * @UniqueEntity(fields={"email"}, message="Tento email je již používán")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 * @Gedmo\Loggable()
 */
class Customer implements UserInterface
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail" , "list"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Gedmo\Versioned()
     * @Assert\Email(
     *     message = "Zadaná hodnota {{ value }} není platná e-mailová adresa."
     * )
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     * @Groups({"detail" , "list"})
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(name="password_hash", type="string", length=255, nullable=false)
     */
    private $passwordHash;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Gedmo\Versioned()
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Groups({"detail" , "list"})
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Gedmo\Versioned()
     * @ORM\Column(name="surname", type="string", length=255, nullable=false)
     * @Groups({"detail" , "list"})
     */
    private $surname;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Gedmo\Versioned()
     * @ORM\Column(name="phone", type="string", length=30, nullable=false)
     * @Assert\Length(min = 8, max = 20, minMessage = "Příliš krátké telefonní číslo", maxMessage = "Příliš dlouhé telefonní číslo")
     * @Assert\Regex(pattern="/^[\+420]?[0-9]*$/", message="Zadejte telefonní číslo ve formátu +420608599235 nebo 608599235")
     * @Groups({"detail" , "list"})
     */
    private $phone;

    /**
     * @var string|null
     * @ORM\Column(name="city", type="string", length=100, nullable=true, options={"default"="NULL"})
     * @Groups({"detail"})
     */
    private $city = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="street", type="string", length=100, nullable=true, options={"default"="NULL"})
     * @Groups({"detail"})
     */
    private $street = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="zip", type="string", length=20, nullable=true, options={"default"="NULL"})
     * @Groups({"detail"})
     */
    private $zip = '';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_company", type="boolean", nullable=true, options={"default"="0"})
     * @Groups({"detail" , "list"})
     */
    private $isCompany = false;

    /**
     * @var string|null
     *
     * @ORM\Column(name="company_name", type="string", length=255, nullable=true, options={"default"="NULL"})
     * @Groups({"detail" , "list"})
     */
    private $companyName = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="cin", type="string", length=255, nullable=true, options={"default"="NULL"})
     * @Groups({"detail"})
     */
    private $cin = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="vatin", type="string", length=255, nullable=true, options={"default"="NULL"})
     * @Groups({"detail"})
     */
    private $vatin = '';

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     * @Groups({"detail" , "list"})
     */
    private $country;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="ProductVariant", inversedBy="customer")
     * @ORM\JoinTable(name="customer_favorite",
     *   joinColumns={
     *     @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="product_variant_id", referencedColumnName="id")
     *   }
     * )
     */
    private $productVariant;

    /**
     * @ORM\OneToMany(targetEntity="CustomerAddress", mappedBy="customer", orphanRemoval=true)
     */
    private $addresses;

    /**
     * @ORM\Column(type="json")
     * @Gedmo\Versioned()
     */
    private $roles = [];

    /**
     * @var string|null
     *
     * @ORM\Column(name="house_number", type="string", length=255, nullable=true, options={"default"="NULL"})
     * @Groups({"list", "detail"})
     */
    private $houseNumber = '';


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productVariant = new ArrayCollection();
        $this->addresses = new ArrayCollection();
    }

    public function buildRegisterForm(&$form)
    {
        $form
            ->add('name', TextType::class, ['attr' => ['placeholder' => false, 'autocomplete' => 'given-name'], 'required' => true])
            ->add('surname', TextType::class, ['attr' => ['placeholder' => false, 'autocomplete' => 'family-name'], 'required' => true])
            ->add('phone', TelType::class, ['attr' => ['placeholder' => false], 'required' => true])
            ->add('email', EmailType::class, ['attr' => ['placeholder' => false], 'required' => true])
            ->add('plainPassword', RepeatedType::class, [
                'mapped' => false,
                'attr' => ['placeholder' => false],
                'required' => true, 'type' => PasswordType::class,
                'invalid_message' => 'Hesla se neshodují',
                'first_options' => [ 'attr' => ['autocomplete' => 'new-password']],
                'second_options' => [ 'attr' =>  ['autocomplete' => 'new-password']],
            ]);
    }

    public function buildProfileForm(&$form)
    {
        $form
            ->add('name', TextType::class, ['attr' => ['placeholder' => false], 'required' => true])
            ->add('surname', TextType::class, ['attr' => ['placeholder' => false], 'required' => true])
            ->add('phone', TelType::class, ['attr' => ['placeholder' => false], 'required' => true])
            ->add('email', EmailType::class, ['attr' => ['placeholder' => false], 'required' => true])
            ->add('plainPassword', RepeatedType::class, [
                'mapped' => false,
                'attr' => ['placeholder' => false, 'autocomplete' => 'new-password'],
                'required' => false,
                'type' => PasswordType::class,
                'invalid_message' => 'Hesla se neshodují',
                'first_options' => [ 'attr' => ['autocomplete' => 'new-password']],
                'second_options' => [ 'attr' =>  ['autocomplete' => 'new-password']],
            ]);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        if ($email === '' || !$email) {
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Email ');
        }
        $this->email = $email;

        return $this;
    }

    public function getPasswordHash(): ?string
    {
        return $this->passwordHash;
    }

    public function setPasswordHash(string $passwordHash): self
    {
//        $this->passwordHash = $passwordHash;
//
//        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        if ($name === '' || !$name) {
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Name ');
        }
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        if ($surname === '' || !$surname) {
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Surname ');
        }
        $this->surname = $surname;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        if ($phone === '' || !$phone) {
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Phone ');
        }
        $this->phone = $phone;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(?string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getIsCompany(): ?bool
    {
        return $this->isCompany;
    }

    public function setIsCompany(?bool $isCompany): self
    {
        $this->isCompany = $isCompany;

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(?string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getCin(): ?string
    {
        return $this->cin;
    }

    public function setCin(?string $cin): self
    {
        $this->cin = $cin;

        return $this;
    }

    public function getVatin(): ?string
    {
        return $this->vatin;
    }

    public function setVatin(?string $vatin): self
    {
        $this->vatin = $vatin;

        return $this;
    }


    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Collection|ProductVariant[]
     */
    public function getProductVariant(): Collection
    {
        return $this->productVariant;
    }

    public function addProductVariant(ProductVariant $productVariant): self
    {
        if (!$this->productVariant->contains($productVariant)) {
            $this->productVariant[] = $productVariant;
        }

        return $this;
    }

    public function removeProductVariant(ProductVariant $productVariant): self
    {
        if ($this->productVariant->contains($productVariant)) {
            $this->productVariant->removeElement($productVariant);
        }

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     * @Groups({"list", "detail"})
     */
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function getPassword(): string
    {
        return (string)$this->passwordHash;
    }

    public function setPassword(string $password): self
    {
        $this->passwordHash = $password;

        return $this;
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function getUsername(): string
    {
        return (string)$this->email;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function load($data)
    {
        $this->setEmail($data['email']);
        $this->setName($data['name']);
        $this->setSurname($data['surname']);
        $this->setPhone($data['phone']);
        $this->setCity($data['city']);
        $this->setStreet($data['street']);
        $this->setZip($data['zip']);
        $this->setIsCompany($data['isCompany']);
        if ($this->isCompany === true) {
            $this->setCompanyName($data['companyName']);
            $this->setCin($data['cin']);
            $this->setVatin($data['vatin']);
        } else {
            $this->setCompanyName('');
            $this->setCin('');
            $this->setVatin('');
        }
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return Collection|CustomerAddress[]
     * @Groups({"detail"})
     */
    public function getAddresses(): Collection
    {
        return $this->addresses;
    }

    public function addAddress(CustomerAddress $address): self
    {
        if (!$this->addresses->contains($address)) {
            $this->addresses[] = $address;
            $address->setCustomer($this);
        }

        return $this;
    }

    public function removeAddress(CustomerAddress $address): self
    {
        if ($this->addresses->contains($address)) {
            $this->addresses->removeElement($address);
            // set the owning side to null (unless already changed)
            if ($address->getCustomer() === $this) {
                $address->setCustomer(null);
            }
        }

        return $this;
    }

    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    public function setHouseNumber(?string $houseNumber): self
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }
}
