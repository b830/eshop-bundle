<?php

namespace App\Akip\EshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * CustomerAddress
 *
 * @ORM\Table(name="customer_address", indexes={@ORM\Index(name="customer_id", columns={"customer_id"}), @ORM\Index(name="country_id", columns={"country_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\CustomerAddressRepository")
 */
class CustomerAddress
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail"})
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true, options={"default"="NULL"})
     * @Groups({"detail", "list"})
     */
    private $name = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="city", type="string", length=100, nullable=true, options={"default"="NULL"})
     * @Groups({"detail", "list"})
     */
    private $city = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="street", type="string", length=100, nullable=true, options={"default"="NULL"})
     * @Groups({"detail", "list"})
     */
    private $street = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="zip", type="string", length=20, nullable=true, options={"default"="NULL"})
     * @Groups({"detail", "list"})
     */
    private $zip = '';

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     * @Groups({"detail", "list"})
     */
    private $country;

    /**
     * @var Customer
     *
     * @ORM\ManyToOne(targetEntity="Customer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     * })
     */
    private $customer;

    /**
     * @var string|null
     *
     * @ORM\Column(name="house_number", type="string", length=255, nullable=true, options={"default"="NULL"})
     * @Groups({"list", "detail"})
     */
    private $houseNumber = '';

    public function buildForm(&$form)
    {
        $form
            ->add('name', TextType::class, ['attr' => ['placeholder' => 'Název'], 'required' => false])
            ->add('street', TextType::class, ['attr' => ['placeholder' => 'Ulice'], 'required' => false])
            ->add('zip', TextType::class, ['attr' => ['placeholder' => 'PSČ'], 'required' => false])
            ->add('houseNumber', TextType::class, ['attr' => ['placeholder' => 'Číslo popisné'], 'required' => false])
            ->add('city', TextType::class, ['attr' => ['placeholder' => 'Město'], 'required' => false])
            ->add('country', EntityType::class, [
                'class' => Country::class,
                'choice_label' => 'name',
                'attr' => ['class' => 'nice-select wide'],
                'required' => false
            ]);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(?string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function load($data)
    {
        if ($data['name'])
            $this->setName($data['name']);
        else
            $this->setName("{$data['city']} {$data['street']}");
        $this->setCity($data['city']);
        $this->setStreet($data['street']);
        $this->setZip($data['zip']);
    }

    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    public function setHouseNumber(?string $houseNumber): self
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }


}
