<?php


namespace App\Akip\EshopBundle\Entity;


class CzechPost extends BaseDelivery
{
    public function __construct()
    {
        $this->heurekaSlug = 'CESKA_POSTA';
        $this->zboziSlug = 'CESKA_POSTA';
        $this->options = [
            'api-key' => [
                'value' => '',
                'label' => 'Api klíč'
            ]
        ];
    }
}
