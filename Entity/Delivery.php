<?php

namespace App\Akip\EshopBundle\Entity;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\BrandTranslation;
use App\Akip\FileManagerBundle\Entity\File;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Gedmo\Loggable\Loggable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="delivery", uniqueConstraints={@ORM\UniqueConstraint(name="slug", columns={"slug", "deleted_at"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\DeliveryRepository")
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 * @UniqueEntity("slug")
 */
class Delivery
{
    use SoftDeleteableEntity;
    use TimestampableEntity;

    const PERSONAL_PICKUP = 'personal-pickup';
    const ZASILKOVNA = 'zasilkovna';
    const ZASILKOVNA_SK = 'zasilkovna-sk';
    const CZECH_POST = 'czech-post';
    const PPL = 'ppl';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     * @Groups({"detail" , "list"})
     * @Gedmo\Versioned()
     */
    private $slug;

    /**
     * @var string
     * @Groups({"detail", "list"})
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     * @Gedmo\Versioned()
     */
    private $description = '';

    /**
     *
     * @ORM\Column(name="options", type="text", nullable=false)
     * @Gedmo\Versioned()
     */
    private $options = '[]';

    /**
     *
     * @ORM\Column(name="fields", type="text", nullable=false)
     * @Groups({"detail" , "list"})
     * @Gedmo\Versioned()
     */
    private $fields = '[]';

    /**
     * @var Collection|Payment[]
     * @ORM\ManyToMany(targetEntity="Payment", inversedBy="deliveries")
     * @Groups({"deliveryDetail"})
     */
    private $payments;

    /** @var bool
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     * @Groups({"detail" , "list"})
     * @Gedmo\Versioned()
     */
    private $enabled = true;

    /**
     * @var \phpDocumentor\Reflection\Types\Collection|DeliveryPrice[]
     * @ORM\OneToMany(targetEntity="DeliveryPrice", mappedBy="delivery", cascade={"persist"})
     * @Groups({"detail", "orderDetail"})
     */
    private $deliveryPrices;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->payments = new ArrayCollection();
        $this->deliveryPrices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        if ($slug === '' || !$slug) {
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Slug ');
        }

        $this->slug = $slug;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Delivery
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Delivery
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @Groups({"detail" , "list"})
     */
    public function getOptions()
    {
        return json_decode($this->options, true);
    }

    /**
     * @param string $options
     * @return Delivery
     */
    public function setOptions(string $options): Delivery
    {
        $this->options = $options;
        return $this;
    }

    /**
     * @return string
     */
    public function getFields(): string
    {
        return $this->fields;
    }

    /**
     * @param string $fields
     * @return Delivery
     */
    public function setFields(string $fields): Delivery
    {
        $this->fields = $fields;
        return $this;
    }

    /**
     * @return Collection|Payment[]
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    public function addPayment(Payment $payment): self
    {
        if (!$this->payments->contains($payment)) {
            $this->payments[] = $payment;
        }
        return $this;
    }

    public function removePayment(Payment $payment): self
    {
        if ($this->payments->contains($payment)) {
            $this->payments->removeElement($payment);
        }
        return $this;
    }

    /**
     * @return bool
     */
    public function getEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     * @return Delivery
     */
    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;
        return $this;
    }



    public function load($data)
    {
        $this->setEnabled($data['enabled']);
        $this->setDescription($data['description']);
        $this->setName($data['name']);
        $this->setSlug($data['slug']);
        $this->setOptions(json_encode($data['options']));
    }

    public function checkOptions()
    {
        $data = json_decode($this->options, true);
        switch ($this->slug) {
            case self::PERSONAL_PICKUP:
                $personalPickUp = new PersonlPickUp();
                $personalPickUp->checkFields($data);
                break;
            case self::ZASILKOVNA:
                $zasilkovna = new Zasilkovna();
                $zasilkovna->checkFields($data);;
                break;
            case self::ZASILKOVNA_SK:
                $zasilkovnaSk = new ZasilkovnaSk();
                $zasilkovnaSk->checkFields($data);;
                break;
            case self::CZECH_POST:
                $czechPost = new CzechPost();
                $czechPost->checkFields($data);
                break;
            case self::PPL:
                $ppl = new PPL();
                $ppl->checkFields($data);
                break;
        }
        $this->setOptions(json_encode($data));
    }

    private function getDeliveryBySlug()
    {
        switch ($this->slug) {
            case self::PERSONAL_PICKUP:
                return new PersonlPickUp();
                break;
            case self::ZASILKOVNA:
                return new Zasilkovna();
                break;
            case self::ZASILKOVNA_SK:
                return new ZasilkovnaSk();
                break;
            case self::CZECH_POST:
                return new CzechPost();
                break;
            case self::PPL:
                return new PPL();
                break;
        }
        return null;
    }

    public function getButton($data = []) {
        $this->checkOptions();
        $delivery = $this->getDeliveryBySlug();
        if (!is_array($data)) {
            $data = json_decode($data, true);
        }
        if ($delivery !== null) {
            return $delivery->getButton(json_decode($this->options, true),$data);
        } else {
            return '';
        }
    }

    public function getHeurekaSlug()
    {
        $delivery = $this->getDeliveryBySlug();
        if ($delivery) {
            return $delivery->heurekaSlug;
        }
    }

    public function getZboziSlug()
    {
        $delivery = $this->getDeliveryBySlug();
        if ($delivery) {
            return $delivery->zboziSlug;
        }
    }

    public function getAllowedPayments($returnArray = false) {
        $ids = [];
        foreach ($this->payments as $payment) {
            $ids[] = $payment->getId();
        }
        if ($returnArray) {
            return $ids;
        } else {
            return implode(',', $ids);
        }
    }

    public function isAddressRequired() {
        $delivery = $this->getDeliveryBySlug();
        if ($delivery) {
            return $delivery->isAddressRequired;
        } else {
            return true;
        }
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|OrderProduct[]
     */
    public function getDeliveryPrices(): \Doctrine\Common\Collections\Collection
    {
        return $this->deliveryPrices;
    }

    public function addDeliveryPrice(DeliveryPrice $deliveryPrice): self
    {
        if (!$this->deliveryPrices->contains($deliveryPrice)) {
            $this->deliveryPrices[] = $deliveryPrice;
            $deliveryPrice->setDelivery($this);
        }

        return $this;
    }

    public function removeDeliveryPrice(DeliveryPrice $deliveryPrice): self
    {
        if ($this->deliveryPrices->contains($deliveryPrice)) {
            $this->deliveryPrices->removeElement($deliveryPrice);
            if ($deliveryPrice->getDelivery() === $this) {
                $deliveryPrice->setDelivery(null);
            }
        }

        return $this;
    }

    public function getPriceBasedOnOrderPrice($orderPrice) {
        $targetDeliveryPrice = null;
        foreach ($this->deliveryPrices as $deliveryPrice) {
            if ($deliveryPrice->getPriceMin() <= $orderPrice and  $deliveryPrice->getPriceMax() >= $orderPrice) {
                $targetDeliveryPrice = $deliveryPrice;
                break;
            }
        }
        if ($targetDeliveryPrice === null) {
            $targetDeliveryPrice = $this->deliveryPrices->first();
        }
        return $targetDeliveryPrice;
    }
}
