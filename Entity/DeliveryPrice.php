<?php

namespace App\Akip\EshopBundle\Entity;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\BrandTranslation;
use App\Akip\FileManagerBundle\Entity\File;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Gedmo\Loggable\Loggable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="delivery_price")
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\DeliveryPriceRepository")
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 * @UniqueEntity("slug")
 */
class DeliveryPrice
{
    use SoftDeleteableEntity;
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private $id;


    /**
     * @var float
     * @ORM\Column(name="price_min", type="float", precision=10, scale=0, nullable=false)
     * @Gedmo\Versioned()
     * @Groups({"detail" , "list"})
     */
    private $priceMin = 0;


    /**
     * @var float
     * @ORM\Column(name="price_max", type="float", precision=10, scale=0, nullable=false)
     * @Gedmo\Versioned()
     * @Groups({"detail" , "list"})
     */
    private $priceMax = 999999;


    /**
     * @var float
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=false)
     * @Gedmo\Versioned()
     * @Groups({"detail" , "list"})
     */
    private $price;

    /**
     * @var bool
     * @Groups({"list", "detail"})
     * @ORM\Column(name="vat", type="boolean", nullable=false)
     * @Gedmo\Versioned()
     */
    private $vat;

    /**
     * @var Vat
     * @Groups({"list", "detail"})
     * @ORM\ManyToOne(targetEntity="Vat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="vat_id", referencedColumnName="id")
     * })
     */
    private $vat2;

    /**
     * @var Delivery
     *
     * @ORM\ManyToOne(targetEntity="Delivery")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="delivery_id", referencedColumnName="id")
     * })
     */
    private $delivery;

    /**
     * Constructor
     */
    public function __construct()
    {

    }

    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Delivery
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return float
     */
    public function getPriceMin(): float
    {
        return $this->priceMin;
    }

    /**
     * @param float $priceMin
     * @return Delivery
     */
    public function setPriceMin(float $priceMin): self
    {
        $this->priceMin = $priceMin;

        return $this;
    }

    /**
     * @return float
     */
    public function getPriceMax(): float
    {
        return $this->priceMax;
    }

    /**
     * @param float $priceMin
     * @return Delivery
     */
    public function setPriceMax(float $priceMax): self
    {
        $this->priceMax = $priceMax;

        return $this;
    }


    public function getVat(): ?bool
    {
        return $this->vat;
    }

    public function setVat(bool $vat): self
    {
        $this->vat = $vat;

        return $this;
    }

    public function getVat2(): ?Vat
    {
        return $this->vat2;
    }

    public function setVat2(?Vat $vat2): self
    {
        $this->vat2 = $vat2;

        return $this;
    }

    public function load($data, Vat $vat)
    {
        if (isset($data['price'])) {
            $this->setPrice($data['price']);
        } else {
            $this->setPrice(0);
        }
        $this->setPriceMin($data['priceMin']);
        $this->setPriceMax($data['priceMax']);
        $this->setVat($data['vat']);
        $this->setVat2($vat);
    }

    private function calcPrice()
    {
        if ($this->getVat() === true) {
            $priceVat = $this->getPrice();
            if ($this->getVat2()->getRate() == 1) {
                $priceWithoutVat = $priceVat;
            } else {
                $priceWithoutVat = $this->getPrice() * (1 - $this->getVat2()->getRate());
            }
        } else {
            $priceWithoutVat = $this->getPrice();
            if ($this->getVat2()->getRate() == 1) {
                $priceVat = $priceWithoutVat;
            } else {
                $priceVat = $this->getPrice() * (1 + $this->getVat2()->getRate());
            }
        }
        return [
            'withVat' => round($priceVat),
            'withoutVat' => round($priceWithoutVat),
        ];
    }

    /**
     * @Groups({"list", "detail"})
     */
    public function getWithVat()
    {
        return $this->calcPrice()['withVat'];
    }

    /**
     * @Groups({"list", "detail"})
     */
    public function getWithoutVat()
    {
        return $this->calcPrice()['withoutVat'];
    }

    /**
     * @Groups({"list", "detail"})
     */
    public function getVatRate()
    {
        return $this->getVat2()->getRate();
    }

    /**
     * @Groups({"list", "detail"})
     */
    public function getVatId()
    {
        return $this->getVat2()->getId();
    }

    /**
     * @return Delivery
     */
    public function getDelivery(): ?Delivery
    {
        return $this->delivery;
    }

    /**
     * @param Delivery $delivery
     */
    public function setDelivery(?Delivery $delivery): self
    {
        $this->delivery = $delivery;

        return $this;
    }
}
