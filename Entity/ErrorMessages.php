<?php


namespace App\Akip\EshopBundle\Entity;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ErrorMessages
{
    const SLUG_EXISTS = [
        'code' => 'unique_failed',
        'message' => "This slug already exists",
        'status' => '422'
    ];

    const NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Entity with specified id not found",
        'status' => '404'
    ];

    const CANNOT_BE_EMPTY = [
        'code' => 'cannot_be_empty',
        'message' => "cannot be empty",
        'status' => '422'
    ];

    const AUTHORIZATION_FAILED = [
        'code' => 'auth-0001',
        'message' => "Incorrect username or password",
        'status' => '403'
    ];
    const CUSTOM_ERROR = [
        'code' => 'validation_failed',
        'message' => "",
        'status' => '422'
    ];
    const IS_NOT_EMPTY = [
        'code' => 'is_not_empty',
        'message' => "is not empty",
        'status' => '422'
    ];
    const COMBINATION_ALREADY_EXISTS = [
        'code' => 'is_not_empty',
        'message' => "Combination of this parameters already exists",
        'status' => '422'
    ];

    static function message($errorMsg, $prefix = '', $suffix = ''){
        $errorMsg['message'] = $prefix . $errorMsg['message'] . $suffix;
        throw new HttpException($errorMsg['status'], $errorMsg['message']);
        die();
    }
}
