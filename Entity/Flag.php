<?php

namespace App\Akip\EshopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use http\Encoding\Stream\Deflate;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Flag
 *
 * @ORM\Table(name="flag", uniqueConstraints={@ORM\UniqueConstraint(name="slug", columns={"slug"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\FlagRepository")
 * @UniqueEntity("slug")
 */
class Flag
{
    const NEW = 'new';
    const SALE = 'sale';
    const BOY = 'boy';
    const GIRL = 'girl';
    const UNISEX = 'unisex';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Groups({"detail", "list"})
     */
    private $name;


    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Product", inversedBy="flags")
     * @ORM\JoinTable(name="product_flags",
     *   joinColumns={
     *     @ORM\JoinColumn(name="flags_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     *   }
     * )
     */
    private $product;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="ProductVariant", mappedBy="flags")
     */
    private $productVariant;

    /**
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\FlagTranslation", mappedBy="flag", orphanRemoval=true)
     */
    private $translations;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     * @Groups({"list", "detail"})
     */
    private $slug;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->product = new \Doctrine\Common\Collections\ArrayCollection();
        $this->productVariant = new ArrayCollection();
        $this->translations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProduct(): Collection
    {
        return $this->product;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->product->contains($product)) {
            $this->product[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->product->contains($product)) {
            $this->product->removeElement($product);
        }

        return $this;
    }
    public function load($data): self
    {
        $this->name = $data['locale'];
        return $this;
    }

    /**
     * @return Collection|ProductVariant[]
     */
    public function getProductVariant(): Collection
    {
        return $this->productVariant;
    }

    public function addProductVariant(ProductVariant $productVariant): self
    {
        if (!$this->productVariant->contains($productVariant)) {
            $this->productVariant[] = $productVariant;
            $productVariant->addFlag($this);
        }

        return $this;
    }

    public function removeProductVariant(ProductVariant $productVariant): self
    {
        if ($this->productVariant->contains($productVariant)) {
            $this->productVariant->removeElement($productVariant);
            $productVariant->removeFlag($this);
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|FlagTranslation[]
     */
    public function get_Translations(): Collection
    {
        return $this->translations;
    }

    /**
     * @return array
     * @Groups({"list"})
     */
    public function getTranslations(): array
    {
        $items = array();
        foreach ($this->translations as $translation){
            $items[$translation->getLocale()] = $translation;
        }
        return $items;
    }


    public function addTranslation(FlagTranslation $translation): self
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setFlag($this);
        }

        return $this;
    }

    public function removeTranslation(FlagTranslation $translation): self
    {
        if ($this->translations->contains($translation)) {
            $this->translations->removeElement($translation);
            // set the owning side to null (unless already changed)
            if ($translation->getFlag() === $this) {
                $translation->setFlag(null);
            }
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;
        return $this;
    }
}
