<?php

namespace App\Akip\EshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * FlagTranslation
 *
 * @ORM\Table(name="flag_translation", uniqueConstraints={@ORM\UniqueConstraint(name="value", columns={"value"})}, indexes={@ORM\Index(name="fk_flag_id", columns={"flag_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\FlagTranslationRepository")
 */
class FlagTranslation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=false)
     * @Groups({"detail", "list"})
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=10, nullable=false)
     */
    private $locale;

    /**
     * @var Flag
     *
     * @ORM\ManyToOne(targetEntity="Flag")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="flag_id", referencedColumnName="id")
     * })
     */
    private $flag;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getFlag(): ?Flag
    {
        return $this->flag;
    }

    public function setFlag(?Flag $flag): self
    {
        $this->flag = $flag;

        return $this;
    }


}
