<?php

namespace App\Akip\EshopBundle\Entity;

use App\Akip\CmsBundle\Controller\api\BaseController;
use App\Akip\CmsBundle\Entity\ErrorMessages;
use DateTime as DateTimeAlias;
use Defr\QRPlatba\QRPlatba;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use phpDocumentor\Reflection\Types\Collection;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Order
 *
 * @ORM\Table(name=" `order` ", indexes={@ORM\Index(name="currency_id", columns={"currency_id"}), @ORM\Index(name="customer_id", columns={"customer_id"}), @ORM\Index(name="country_id", columns={"country_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\OrderRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 * @Gedmo\Loggable()
 */
class Order extends AbstractType
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"list", "detail", "orderDetail"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="customer_name", type="string", length=255, nullable=false)
     * @Groups({"orderDetail", "list"})
     */
    private $customerName;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="customer_surname", type="string", length=255, nullable=false)
     * @Groups({"orderDetail", "list"})
     */
    private $customerSurname;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min = 8, max = 20, minMessage = "Příliš krátké telefonní číslo", maxMessage = "Příliš dlouhé telefonní číslo")
     * @Assert\Regex(pattern="/^[\+420]?[0-9]*$/", message="Zadejte telefonní číslo ve formátu +420608599235 nebo 608599235")
     * @ORM\Column(name="customer_phone", type="string", length=30, nullable=false)
     * @Groups({"orderDetail", "list"})
     */
    private $customerPhone;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "Zadaná hodnota {{ value }} není platná e-mailová adresa."
     * )
     * @ORM\Column(name="customer_email", type="string", length=255, nullable=false)
     * @Groups({"orderDetail", "list"})
     */
    private $customerEmail;

    /**
     * @var string
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     * @Groups({"orderDetail"})
     */
    private $city = '';

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=false)
     * @Groups({"orderDetail"})
     */
    private $street = '';

    /**
     * @var string
     *
     * @ORM\Column(name="house_number", type="string", length=255, nullable=true)
     * @Groups({"orderDetail"})
     */
    private $houseNumber = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="zip", type="string", length=20, nullable=true)
     * @Groups({"orderDetail"})
     */
    private $zip = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="address_specification", type="string", length=255, nullable=true)
     * @Groups({"orderDetail"})
     */
    private $addressSpecification = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="municipality", type="string", length=255, nullable=true)
     * @Groups({"orderDetail"})
     */
    private $municipality = '';

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     * @Groups({"orderDetail"})
     */
    private $country;

    /**
     * @var string|null
     * @ORM\Column(name="note", type="text", length=0, nullable=true, options={"default"="NULL"})
     * @Groups({"orderDetail"})
     */
    private $note = '';

    /**
     * @var Currency
     *
     * @ORM\ManyToOne(targetEntity="Currency")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     * })
     * @Groups({"orderDetail"})
     */
    private $currency;

    /**
     * @var Customer
     *
     * @ORM\ManyToOne(targetEntity="Customer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     * })
     * @Groups({"orderDetail"})
     */
    private $customer;

    /**
     * @var Collection|OrderProduct[]
     * @ORM\OneToMany(targetEntity="OrderProduct", mappedBy="order", cascade={"persist"})
     * @Groups({"detail", "orderDetail"})
     */
    private $productVariants;

    /**
     * @var float
     *
     * @var float
     * @ORM\Column(name="delivery_price", type="float", precision=10, scale=0, nullable=false)
     * @Gedmo\Versioned()
     * @Groups({"orderDetail"})
     */
    private $deliveryPrice = 0;

    /**
     * @var float
     *
     * @var float
     * @ORM\Column(name="payment_price", type="float", precision=10, scale=0, nullable=false)
     * @Gedmo\Versioned()
     * @Groups({"orderDetail"})
     */
    private $paymentPrice = 0;

    /**
     * @var Delivery
     *
     * @ORM\ManyToOne(targetEntity="Delivery", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="delivery_id", referencedColumnName="id")
     * })
     * @Groups({"orderDetail", "list"})
     */
    private $delivery;

    /**
     * @var Payment
     *
     * @ORM\ManyToOne(targetEntity="Payment", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_id", referencedColumnName="id")
     * })
     * @Groups({"orderDetail", "list"})
     */
    private $payment;

    /**
     * @var OrderStatus
     *
     * @ORM\ManyToOne(targetEntity="OrderStatus", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="order_status_id", referencedColumnName="id")
     * })
     * @Groups({"orderDetail", "list"})
     */
    private $orderStatus;

    /**
     * @ORM\Column(name="delivery_info", type="text", nullable=false)
     * @Gedmo\Versioned()
     */
    private $deliveryInfo = '{}';

    /**
     * @var DateTimeAlias|null
     * @ORM\Column(name="payment_date", type="datetime", nullable=true)
     * @Groups({"demandDetail", "detail", "list"})
     * @Gedmo\Versioned()
     */
    private $paymentDate = null;

    /**
     * @var Collection|OrderPaymentLog[]
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\OrderPaymentLog", mappedBy="order", cascade={"persist"})
     * @Groups({"detail", "orderDetail"})
     */
    private $paymentLogs;


    public function __construct()
    {
        $this->productVariants = new ArrayCollection();
        $this->paymentLogs = new ArrayCollection();
    }


    public function buildAddressForm(&$form)
    {
        $form
            ->add('customerName', TextType::class, ['attr' => ['placeholder' => false, 'autocomplete' => 'given-name'], 'required' => true])
            ->add('customerSurname', TextType::class, ['attr' => ['placeholder' => false], 'required' => true])
            ->add('customerPhone', TelType::class, ['attr' => ['placeholder' => false], 'required' => true])
            ->add('customerEmail', EmailType::class, ['attr' => ['placeholder' => false], 'required' => true]);
        if ($this->delivery && $this->delivery->isAddressRequired()) {
            $form
                ->add('city', TextType::class, ['attr' => ['placeholder' => false], 'required' => true])
                ->add('street', TextType::class, ['attr' => ['placeholder' => false], 'required' => true])
                ->add('houseNumber', TextType::class, ['attr' => ['placeholder' => false], 'required' => false])
                ->add('zip', TextType::class, ['attr' => ['placeholder' => false], 'required' => false])
                ->add('municipality', TextType::class, ['attr' => ['placeholder' => false], 'required' => false])
                ->add('addressSpecification', TextType::class, ['attr' => ['placeholder' => false], 'required' => false])
                ->add('country', EntityType::class, [
                    'class' => Country::class,
                    'choice_label' => 'name',
                    'attr' => ['class' => 'nice-select wide'],
                    'required' => false,
                ])
                ->add('note', TextareaType::class, ['attr' => ['placeholder' => false], 'required' => false]);
        }
    }

    public function setCustomerData($customer): self
    {
        $this->setCustomerName($customer->getName());
        $this->setCustomerSurname($customer->getSurname());
        if ($this->getCustomerEmail() === '' || $this->getCustomerEmail() === null)
            $this->setCustomerEmail($customer->getEmail());
        $this->setCustomerPhone($customer->getPhone());
        $this->setStreet($customer->getStreet());
        $this->setCity($customer->getCity());
        $this->setZip($customer->getZip());
        $this->setCountry($customer->getCountry());

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCustomerName(): ?string
    {
        return $this->customerName;
    }

    public function setCustomerName(string $customerName): self
    {
        if ($customerName === '' || !$customerName) {
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Customer name ');
        }
        $this->customerName = $customerName;

        return $this;
    }

    public function getCustomerSurname(): ?string
    {
        return $this->customerSurname;
    }

    public function getCustomerPhone(): ?string
    {
        return $this->customerPhone;
    }

    public function setCustomerPhone(string $customerPhone): self
    {
        if ($customerPhone === '' || !$customerPhone) {
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Customer phone ');
        }
        $this->customerPhone = $customerPhone;

        return $this;
    }

    public function setCustomerSurname(string $customerSurname): self
    {
        if ($customerSurname === '' || !$customerSurname) {
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Customer surname ');
        }
        $this->customerSurname = $customerSurname;

        return $this;
    }

    public function getCustomerEmail(): ?string
    {
        return $this->customerEmail;
    }

    public function setCustomerEmail(string $customerEmail): self
    {
        if ($customerEmail === '' || !$customerEmail) {
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Customer email ');
        }
        $this->customerEmail = $customerEmail;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
//        if ($city === '' || !$city) {
//            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'City ');
//        }
        $this->city = $city;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
//        if ($street === '' || !$street) {
//            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Street ');
//        }
        $this->street = $street;

        return $this;
    }

    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    public function setHouseNumber(?string $houseNumber): self
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(?string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getAddressSpecification(): ?string
    {
        return $this->addressSpecification;
    }

    public function setAddressSpecification(?string $addressSpecification): self
    {
        $this->addressSpecification = $addressSpecification;

        return $this;
    }

    public function getMunicipality(): ?string
    {
        return $this->municipality;
    }

    public function setMunicipality(?string $municipality): self
    {
        $this->municipality = $municipality;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|OrderProduct[]
     */
    public function getProductVariants(): \Doctrine\Common\Collections\Collection
    {
        return $this->productVariants;
    }

    public function addProductVariant(OrderProduct $productVariant): self
    {
        if (!$this->productVariants->contains($productVariant)) {
            $this->productVariants[] = $productVariant;
            $productVariant->setOrder($this);
        }

        return $this;
    }

    public function removeProductVariant(OrderProduct $productVariant): self
    {
        if ($this->productVariants->contains($productVariant)) {
            $this->productVariants->removeElement($productVariant);
            // set the owning side to null (unless already changed)
            if ($productVariant->getOrder() === $this) {
                $productVariant->setOrder(null);
            }
        }

        return $this;
    }


    /**
     * @return int
     */
    public function getDeliveryPrice(): int
    {
        return $this->deliveryPrice;
    }

    /**
     * @param float $deliveryPrice
     * @return Order
     */
    public function setDeliveryPrice(float $deliveryPrice): self
    {
        $this->deliveryPrice = $deliveryPrice;
        return $this;
    }

    /**
     * @return int
     */
    public function getPaymentPrice(): int
    {
        return $this->paymentPrice;
    }

    /**
     * @param float $paymentPrice
     * @return Order
     */
    public function setPaymentPrice(float $paymentPrice): self
    {
        $this->paymentPrice = $paymentPrice;
        return $this;
    }

    /**
     * @Groups({"orderDetail", "list"})
     */
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function load($data)
    {
        $this->setStreet($data['street']);
        $this->setHouseNumber($data['houseNumber']);
        $this->setCity($data['city']);
        $this->setMunicipality($data['municipality']);
        $this->setZip($data['zip']);
        $this->setAddressSpecification($data['addressSpecification']);
        $this->setNote($data['note']);
        $this->setCustomerName($data['customerName']);
        $this->setCustomerSurname($data['customerSurname']);
        $this->setCustomerPhone($data['customerPhone']);
        $this->setCustomerEmail($data['customerEmail']);


        return $this;
    }

    public function check(): bool
    {
        if ($this->getCustomerEmail() === '' or $this->getCustomerEmail() === null)
            return false;
        else if ($this->getCustomerName() === '' or $this->getCustomerName() === null)
            return false;
        else if ($this->getCustomerSurname() === '' or $this->getCustomerSurname() === null)
            return false;
        else if ($this->getCustomerPhone() === '' or $this->getCustomerPhone() === null)
            return false;
        else
            return true;
    }

    /**
     * @return Delivery
     */
    public function getDelivery(): ?Delivery
    {
        return $this->delivery;
    }

    /**
     * @param Delivery $delivery
     */
    public function setDelivery(Delivery $delivery): self
    {
        $this->delivery = $delivery;
        return $this;
    }

    /**
     * @return Payment
     */
    public function getPayment(): ?Payment
    {
        return $this->payment;
    }

    /**
     * @param Payment $payment
     * @return Order
     */
    public function setPayment(Payment $payment): self
    {
        $this->payment = $payment;
        return $this;
    }

    public function getOrderStatus(): ?OrderStatus
    {
        return $this->orderStatus;
    }

    public function setOrderStatus(?OrderStatus $orderStatus): self
    {
        $this->orderStatus = $orderStatus;

        return $this;
    }

    /**
     * @Groups({"list", "detail", "orderDetail"})
     */
    public function getTotalPrice() : float
    {
        $totalPrice = 0;
        if ($this->productVariants->count() > 0) {
            foreach ($this->productVariants as $orderProduct) {
                /** @var OrderProduct $orderProduct */
                $totalPrice += $orderProduct->getPrice()['Kč']['actualWithVat'] > 0 ? ($orderProduct->getPrice()['Kč']['actualWithVat'] * $orderProduct->getCount()) : ($orderProduct->getPrice()['Kč']['withVat'] * $orderProduct->getCount());
            }
        }
        return $totalPrice;
    }

    /**
     * @Groups({"list", "detail", "orderDetail"})
     */
    public function getFullPrice(): float {
        return $this->getTotalPrice() + $this->getDeliveryPrice() + $this->getPaymentPrice();
    }

    /**
     * @Groups({"detail", "orderDetail"})
     */
    public function getDeliveryInfo()
    {
        return json_decode($this->deliveryInfo, true);
    }

    /**
     * @param string $deliveryInfo
     * @return Order
     */
    public function setDeliveryInfo(string $deliveryInfo): self
    {
        $this->deliveryInfo = $deliveryInfo;
        return $this;
    }

    /**
     * @Groups({"detail", "orderDetail", "list"})
     */
    public function getOrderNumber() :string
    {
        $variable = ['year' => date('Y', $this->getCreatedAt()->getTimestamp()), 'number' => $this->id];
        $pattern = $_ENV['ORDER_NUMBER_PATTERN'];
        foreach ($variable as $pName => $pValue) {
            $format = $this->substituteFormatVariable($pattern, $pName,
                function($value) use ($pValue) {
                    return str_pad(substr($pValue, -$value), $value, '0', STR_PAD_LEFT);
                }
            );
        }
        return $format;
    }

    private function substituteFormatVariable(&$format, $variable, callable $callback)
    {
        $pattern = "/%{$variable}\{([^}]+)\}/i";

        $match = [];
        preg_match($pattern, $format, $match);

        if (count($match) > 0) {
            $format = preg_replace($pattern, call_user_func($callback, $match[1]), $format);
        }

        return $format;
    }


    public function getQrCode($html = true) {

        if ($this->getPayment()->getSlug() !== Payment::BANK_ACCOUNT) {
            die('xxxx');
            return '';
        }
        $qr = new QRPlatba();
        if ($html) {
            return $qr->create("{$this->getPayment()->getOptions()['bank-account']['value']}/{$this->getPayment()->getOptions()['bank-code']['value']}", $this->getTotalPrice() + $this->getPaymentPrice() + $this->getDeliveryPrice())
                ->setVariableSymbol($this->getOrderNumber())
                ->setMessage('Platba objednávky č.' . $this->getOrderNumber())
                ->getQRCodeImage();
            } else {
            return $qr->create("{$this->getPayment()->getOptions()['bank-account']['value']}/{$this->getPayment()->getOptions()['bank-code']['value']}", $this->getTotalPrice() + $this->getPaymentPrice() + $this->getDeliveryPrice())
                ->setVariableSymbol($this->getOrderNumber())
                ->setMessage('Platba objednávky č.' . $this->getOrderNumber())
                ->getQRCodeInstance()->writeString();
        }
    }

    /**
     * @return DateTimeAlias|null
     */
    public function getPaymentDate(): ?DateTimeAlias
    {
        return $this->paymentDate;
    }

    /**
     * @param DateTimeAlias|null $paymentDate
     */
    public function setPaymentDate(?DateTimeAlias $paymentDate): self
    {
        $this->paymentDate = $paymentDate;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|OrderPaymentLog[]
     */
    public function getPaymentLogs(): \Doctrine\Common\Collections\Collection
    {
        return $this->paymentLogs;
    }

    public function addPaymentLog(OrderPaymentLog $paymentLog): self
    {
        if (!$this->paymentLogs->contains($paymentLog)) {
            $this->paymentLogs[] = $paymentLog;
            $paymentLog->setOrder($this);
        }

        return $this;
    }

    public function removePaymentLog(OrderPaymentLog $paymentLog): self
    {
        if ($this->paymentLogs->contains($paymentLog)) {
            $this->paymentLogs->removeElement($paymentLog);
            // set the owning side to null (unless already changed)
            if ($paymentLog->getOrder() === $this) {
                $paymentLog->setOrder(null);
            }
        }

        return $this;
    }

    public function getNextPaymentNumber() {
        if ($this->paymentLogs->count() > 0) {
            /** @var OrderPaymentLog $lastPaymentLog */
            $lastPaymentLog = $this->paymentLogs->last();
            $nextNumber = str_pad((int)substr($lastPaymentLog->getPaymentNumber(), -2) + 1, 2, '0', STR_PAD_LEFT);
        } else {
            $nextNumber = '01';
        }
        return "{$this->getOrderNumber()}{$nextNumber}";
    }
}
