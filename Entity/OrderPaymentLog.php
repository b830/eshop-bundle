<?php

namespace App\Akip\EshopBundle\Entity;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use phpDocumentor\Reflection\Types\This;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * OrderProduct
 *
 * @ORM\Table(name="order_payment_log", indexes={@ORM\Index(name="order_id", columns={"order_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\OrderPaymentLogRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 * @Gedmo\Loggable()
 */
class OrderPaymentLog
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    const PAID = 'paid';
    const CANCELLED = 'cancelled';
    const WAITING = 'waiting';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"orderDetail"})
     */
    private $id;

    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="Order")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     * })
     */
    private $order;

    /**
     * @var string
     * @Groups({"detail", "list", "orderDetail"})
     * @ORM\Column(name="status", type="string", columnDefinition="ENUM('paid','cancelled', 'waiting')", length=0, nullable=false)
     * @Gedmo\Versioned()
     */
    private $status;

    /** @var string
     * @Groups({"detail", "list", "orderDetail"})
     * @ORM\Column(name="payment_number", type="string", length=255, nullable=false)
     * @Gedmo\Versioned()
     */
    private $paymentNumber;

    /**
     * @var Payment
     *
     * @ORM\ManyToOne(targetEntity="Payment", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_id", referencedColumnName="id")
     * })
     * @Groups({"orderDetail", "list"})
     */
    private $paymentType;

    /**
     * @var float
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=false)
     * @Gedmo\Versioned()
     */
    private $price;

    /**
     *
     * @ORM\Column(name="request_data", type="text", nullable=true)
     * @Gedmo\Versioned()
     */
    private $requestData = '';

    /**
     * @ORM\Column(name="response_data", type="text", nullable=true)
     * @Gedmo\Versioned()
     */
    private $responseData = '';


    public function getId(): ?int
    {
        return $this->id;
    }


    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function setOrder(?Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $type
     */
    public function setStatus(string $status): self
    {
        if (!in_array($status, [self::PAID, self::CANCELLED, self::WAITING])) {
            ErrorMessages::message(ErrorMessages::INVALID_TYPE, 'Status ');
        }
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentNumber(): string
    {
        return $this->paymentNumber;
    }

    /**
     * @param string $paymentNumber
     */
    public function setPaymentNumber(string $paymentNumber): self
    {
        $this->paymentNumber = $paymentNumber;

        return $this;
    }

    /**
     * @return Payment
     */
    public function getPaymentType(): ?Payment
    {
        return $this->paymentType;
    }

    /**
     * @param Payment $payment
     * @return OrderPaymentLog
     */
    public function setPaymentType(Payment $payment): self
    {
        $this->paymentType = $payment;
        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        if ($price === ''){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Price ');
        }
        $this->price = $price;

        return $this;
    }

    /**
     * @return string
     */
    public function getRequestData(): string
    {
        return $this->requestData;
    }

    /**
     * @param string $requestData
     */
    public function setRequestData(string $requestData): self
    {
        $this->requestData = $requestData;

        return $this;
    }

    /**
     * @return string
     */
    public function getResponseData(): string
    {
        return $this->responseData;
    }

    /**
     * @param string $responseData
     */
    public function setResponseData(string $responseData): self
    {
        $this->responseData = $responseData;

        return $this;
    }

}
