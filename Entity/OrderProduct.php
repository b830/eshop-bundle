<?php

namespace App\Akip\EshopBundle\Entity;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use phpDocumentor\Reflection\Types\This;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * OrderProduct
 *
 * @ORM\Table(name="order_product", indexes={@ORM\Index(name="product_variant_id", columns={"product_variant_id"}), @ORM\Index(name="vat_id", columns={"vat_id"}), @ORM\Index(name="order_id", columns={"order_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\OrderProductRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 * @Gedmo\Loggable()
 */
class OrderProduct
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"orderDetail"})
     */
    private $id;

    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Column(name="count", type="integer", nullable=false)
     * @Groups({"orderDetail"})
     */
    private $count;

    /**
     * @var float
     * @Assert\NotBlank()
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=false)
     */
    private $price;

    /**
     * @var float
     * @Assert\NotBlank()
     * @ORM\Column(name="actual_price", type="float", precision=10, scale=0, nullable=false)
     */
    private $actualPrice;

    /**
     * @var bool
     * @Assert\NotBlank()
     * @ORM\Column(name="vat", type="boolean", nullable=false)
     */
    private $vat;

    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="Order")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     * })
     */
    private $order;

    /**
     * @var ProductVariant
     *
     * @ORM\ManyToOne(targetEntity="ProductVariant")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_variant_id", referencedColumnName="id")
     * })
     */
    private $productVariant;

    /**
     * @var Vat
     *
     * @ORM\ManyToOne(targetEntity="Vat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="vat_id", referencedColumnName="id")
     * })
     */
    private $vat2;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }

//    public function getPrice(): ?float
//    {
//        return $this->price;
//    }

    /**
     * @Groups({"list", "orderDetail"})
     */
    public function getProductVariantId()
    {
        return $this->productVariant->getId();
    }

    /**
     * @Groups({"list", "orderDetail"})
     */
    public function getProductId()
    {
        return $this->productVariant->getProduct()->getId();
    }

    /**
     * @Groups({"list", "orderDetail"})
     */
    public function getProductVariantName()
    {
        return $this->productVariant->getName();
    }

    /**
     * @Groups({"list", "orderDetail"})
     */
    public function getProductName()
    {
        return $this->productVariant->getProduct()->getName();
    }

    /**
     * @Groups({"list", "orderDetail"})
     */
    public function getFullName()
    {
        $delimiter = $this->productVariant->getName() === '' ? '' : '-';
        return "{$this->productVariant->getProduct()->getName()} {$delimiter} {$this->productVariant->getName()}";
    }

    /**
     * @Groups({"list", "orderDetail"})
     */
    public function getPrice()
    {
        $data = array();


        if ($this->getVat() === true) {
            $priceVat = $this->price;
            $actualWithVat = $this->actualPrice;
            if ($this->getVat2()->getRate() == 1) {
                $priceWithoutVat = $priceVat;
                $actualWithoutVat = $priceVat;
            } else {
                $priceWithoutVat = $this->price * (1 - $this->getVat2()->getRate());
                $actualWithoutVat = $this->actualPrice * (1 - $this->getVat2()->getRate());
            }
        } else {
            $priceWithoutVat = $this->price;
            $actualWithoutVat = $this->actualPrice;
            if ($this->getVat2()->getRate() == 1) {
                $priceVat = $priceWithoutVat;
                $actualWithVat = $actualWithoutVat;
            } else {
                $priceVat = $this->price * (1 + $this->getVat2()->getRate());
                $actualWithVat = $this->actualPrice * (1 + $this->getVat2()->getRate());
            }
        }
        $data[$this->order->getCurrency()->getSymbol()] = [
            'withVat' => round($priceVat),
            'withoutVat' => round($priceWithoutVat),
            'actualWithVat' => round($actualWithVat),
            'actualWithoutVat' => round($actualWithoutVat),
            'vatRate' => $this->getVat2()->getRate(),
            'vat' => $this->getVat() === true ? 'true' : 'false',
            'symbol' => $this->order->getCurrency()->getSymbol(),
        ];


        return $data;
    }

    public function setPrice(float $price): self
    {
        if ($price === '') {
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Price ');
        }
        $this->price = $price;

        return $this;
    }

    public function setActualPrice(float $price): self
    {
        if ($price === '' || $price === null) {
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Actual price ');
        }
        $this->actualPrice = $price;

        return $this;
    }

    public function getVat(): ?bool
    {
        return $this->vat;
    }

    public function setVat(bool $vat): self
    {
        if ($vat === '' || !$vat) {
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Vat ');
        }
        $this->vat = $vat;

        return $this;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function setOrder(?Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getProductVariant(): ?ProductVariant
    {
        return $this->productVariant;
    }

    public function setProductVariant(?ProductVariant $productVariant): self
    {
        $this->productVariant = $productVariant;

        return $this;
    }

    public function getVat2(): ?Vat
    {
        return $this->vat2;
    }

    public function setVat2(?Vat $vat2): self
    {
        $this->vat2 = $vat2;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @Groups({"list", "orderDetail"})
     */
    public function getProductJoins()
    {
        return $this->productVariant->getProduct()->getProductJoin();
    }

    /**
     * @Groups({"orderDetail"})
     */
    public function getInStock()
    {
        return $this->productVariant->getInStock();
    }


}
