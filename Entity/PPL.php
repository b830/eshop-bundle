<?php


namespace App\Akip\EshopBundle\Entity;


class PPL extends BaseDelivery
{
    public function __construct()
    {
        $this->heurekaSlug = 'PPL';
        $this->zboziSlug = 'PPL';
        $this->options = [
            'api-key' => [
                'value' => '',
                'label' => 'Api klíč'
            ]
        ];
    }
}
