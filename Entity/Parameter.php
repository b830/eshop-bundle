<?php

namespace App\Akip\EshopBundle\Entity;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\ExpressionLanguage\ExpressionFunction;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Parameter
 *
 * @ORM\Table(name="parameter")
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\ParameterRepository")
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 */
class Parameter
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    //'int','float','boolean','color','choice','multi_choice'
    const TYPE_INT = 'int';
    const TYPE_FLOAT = 'float';
    const TYPE_BOOLEAN = 'boolean';
    const TYPE_COLOR = 'color';
    const TYPE_CHOICE = 'choice';
    const TYPE_MULTI_CHOICE = 'multi_choice';
    const TYPE_STRING = 'string';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $name;

    /**
     * @var string
     * @Groups({"detail", "list"})
     * @ORM\Column(name="type", type="string", columnDefinition="ENUM('int','float','boolean','color','choice','multi_choice','string')", length=0, nullable=false)
     * @Gedmo\Versioned()
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="sort", type="integer", nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $sort;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Category", mappedBy="parameter")
     */
    private $category;
    /**
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\ParameterTranslation", mappedBy="parameter", orphanRemoval=true)
     */
    private $translations;

    /**
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\ParameterValue", mappedBy="parameter", orphanRemoval=true)
     */
    private $values;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->category = new \Doctrine\Common\Collections\ArrayCollection();
        $this->translations = new ArrayCollection();
        $this->values = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        if ($name === '' || !$name){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Name ');
        }
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategory(): Collection
    {
        return $this->category;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
            $category->addParameter($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->category->contains($category)) {
            $this->category->removeElement($category);
            $category->removeParameter($this);
        }

        return $this;
    }

    /**
     * @return Collection|ParameterTranslation[]
     */
    public function get_Translations(): Collection
    {
        return $this->translations;
    }

    public function addTranslation(ParameterTranslation $translation): self
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setParameter($this);
        }

        return $this;
    }

    public function removeTranslation(ParameterTranslation $translation): self
    {
        if ($this->translations->contains($translation)) {
            $this->translations->removeElement($translation);
            // set the owning side to null (unless already changed)
            if ($translation->getParameter() === $this) {
                $translation->setParameter(null);
            }
        }

        return $this;
    }

    /**
     * @Groups({"detail", "list"})
     */
    public function getTranslations()
    {
        if ($this->get_Translations()->isEmpty())
            return [];
        foreach ($this->get_Translations() as $translation) {
            $items[$translation->getLocale()] = $translation;
        }
        return $items;
    }

    public function load($data)
    {
        $this->setName($data['name']);
        $this->setType($data['type']);
        $this->setSort($data['sort']);
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        if ($type === '' || !$type){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Type ');
        }
        if (!in_array($type, [self::TYPE_BOOLEAN, self::TYPE_CHOICE, self::TYPE_COLOR, self::TYPE_FLOAT, self::TYPE_INT, self::TYPE_MULTI_CHOICE, self::TYPE_STRING]))
            ErrorMessages::message(ErrorMessages::INVALID_TYPE, '');

        $this->type = $type;

        return $this;
    }

    public function checkTypes():bool
    {
        if (!in_array($this->type, [self::TYPE_MULTI_CHOICE, self::TYPE_CHOICE, self::TYPE_COLOR]))
            return false;
        return true;
    }

    public function getSort(): ?int
    {
        return $this->sort;
    }

    public function setSort(int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * @return Collection|ParameterValue[]
     */
    public function getValues(): Collection
    {
        return $this->values;
    }

    public function addValue(ParameterValue $value): self
    {
        if (!$this->values->contains($value)) {
            $this->values[] = $value;
            $value->setParameter($this);
        }

        return $this;
    }

    public function removeValue(ParameterValue $value): self
    {
        if ($this->values->contains($value)) {
            $this->values->removeElement($value);
            // set the owning side to null (unless already changed)
            if ($value->getParameter() === $this) {
                $value->setParameter(null);
            }
        }
        return $this;
    }

    public function types(){
        return [self::TYPE_INT, self::TYPE_FLOAT, self::TYPE_COLOR, self::TYPE_BOOLEAN, self::TYPE_CHOICE, self::TYPE_MULTI_CHOICE, self::TYPE_STRING];
    }

    /**
     * @return array
     * @Groups({"detail"})
     */
    public function getValuesList(){
        $items = array();
        foreach ($this->values as $value){
            $items[] = [
                'id'=> $value->getId(),
                'name' => $value->getName()
            ];
        }
        usort($items, function ($item1, $item2) {
            return $item1['name'] <=> $item2['name'];
        });
        return $items;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setTempValues($values){
        $this->values = $values;
    }
}
