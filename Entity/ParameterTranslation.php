<?php

namespace App\Akip\EshopBundle\Entity;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ParameterTranslation
 *
 * @ORM\Table(name="parameter_translation", indexes={@ORM\Index(name="parameter_id", columns={"parameter_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\ParameterTranslationRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=false)
 * @Gedmo\Loggable()
 */
class ParameterTranslation
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Groups({"detail", "list"})
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Gedmo\Versioned()
     */
    private $name;

    /**
     * @var string
     * @Groups({"detail", "list"})
     * @Assert\NotBlank()
     * @ORM\Column(name="locale", type="string", length=10, nullable=false)
     * @Gedmo\Versioned()
     */
    private $locale;

    /**
     * @var string
     * @Groups({"detail", "list"})
     * @Assert\NotBlank()
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     * @Gedmo\Versioned()
     */
    private $slug;

    /**
     * @var string|null
     * @Groups({"detail", "list"})
     * @ORM\Column(name="prefix", type="string", length=255, nullable=true, options={"default"="NULL"})
     * @Gedmo\Versioned()
     */
    private $prefix = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="suffix", type="string", length=255, nullable=true, options={"default"="NULL"})
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $suffix = '';

    /**
     * @var Parameter
     *
     * @ORM\ManyToOne(targetEntity="Parameter")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parameter_id", referencedColumnName="id")
     * })
     */
    private $parameter;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        if ($name === '' || !$name){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Name ');
        }
        $this->name = $name;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return int|null
     * @Groups({"detail", "list"})
     */
    public function getParameterId()
    {
        return $this->parameter->getId();
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        if ($slug === '' || !$slug){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Slug ');
        }
        $this->slug = $slug;

        return $this;
    }

    public function getParameter(): ?Parameter
    {
        return $this->parameter;
    }

    public function setParameter(?Parameter $parameter): self
    {
        $this->parameter = $parameter;

        return $this;
    }

//    /**
//     * @Groups({"detail", "list"})
//     */
//    public function getTranslation()
//    {
//        return [
//          $this->locale => [
//              'id' => $this->id,
//              'name' => $this->name,
//              'slug' => $this->slug
//          ]
//        ];
//    }


    public function getPrefix(): ?string
    {
        return $this->prefix;
    }

    public function setPrefix(?string $prefix): self
    {
        $this->prefix = $prefix;

        return $this;
    }

    public function getSuffix(): ?string
    {
        return $this->suffix;
    }

    public function setSuffix(?string $suffix): self
    {
        $this->suffix = $suffix;

        return $this;
    }

    public function load($key, $data)
    {
        $this->setLocale($key);
        $this->setName($data['name']);
        $this->setSlug($data['slug']);
        $this->setPrefix($data['prefix']);
        $this->setSuffix($data['suffix']);
    }
}
