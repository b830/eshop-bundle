<?php

namespace App\Akip\EshopBundle\Entity;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use phpDocumentor\Reflection\Types\Collection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * ParameterValue
 *
 * @ORM\Table(name="parameter_value", indexes={@ORM\Index(name="parameter_id", columns={"parameter_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\ParameterValueRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 * @Gedmo\Loggable()
 */
class ParameterValue
{
    use SoftDeleteableEntity;
    use TimestampableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"list", "detail"})
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="sort", type="integer", nullable=false)
     * @Groups({"list", "detail"})
     * @Gedmo\Versioned()
     */
    private $sort;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Groups({"list", "detail"})
     * @Gedmo\Versioned()
     */
    private $name;

    /**
     * @var Parameter
     *
     * @ORM\ManyToOne(targetEntity="Parameter")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parameter_id", referencedColumnName="id")
     * })
     */
    private $parameter;

    /**
     * @var Collection|ParameterValueTranslation[]
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\ParameterValueTranslation", mappedBy="parameterValue", orphanRemoval=true)
     */
    private $translations;

    /**
     *
     * @ORM\Column(name="options", type="text", nullable=false)
     * @Groups({"detail" , "list"})
     * @Gedmo\Versioned()
     */
    private $options = '{}';

    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSort(): ?int
    {
        return $this->sort;
    }

    public function setSort(int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        if ($name === '' || !$name) {
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Name ');
        }
        $this->name = $name;

        return $this;
    }

    public function getParameter(): ?Parameter
    {
        return $this->parameter;
    }

    public function setParameter(?Parameter $parameter): self
    {
        $this->parameter = $parameter;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|ParameterValueTranslation[]
     */
    public function get_Translations(): \Doctrine\Common\Collections\Collection
    {
        return $this->translations;
    }

    /**
     * @return array
     * @Groups({"list", "detail"})
     */
    public function getTranslations(){
        if ($this->translations->isEmpty())
            return [];
        else{
            $items = array();
            foreach ($this->translations as $translation){
                $items[$translation->getLocale()] = $translation;
            }
        }
        return $items;
    }

    public function addTranslation(ParameterValueTranslation $translation): self
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setParameterValue($this);
        }

        return $this;
    }

    public function removeTranslation(ParameterValueTranslation $translation): self
    {
        if ($this->translations->contains($translation)) {
            $this->translations->removeElement($translation);
            // set the owning side to null (unless already changed)
            if ($translation->getParameterValue() === $this) {
                $translation->setParameterValue(null);
            }
        }

        return $this;
    }

    public function getOptions(): ?string
    {
        return $this->options;
    }

    public function setOptions($options): self
    {
        if (is_array($options)) {
            $this->options = json_encode($options);
        } else {
            $this->options = $options;
        }

        return $this;
    }

    public function load($data)
    {
        $this->setName($data['name']);
        $this->setSort(0);
        if (isset($data['sort']))
            $this->setSort($data['sort']);
        if (isset($data['options'])) {
            $this->setOptions($data['options']);
        }
    }


}
