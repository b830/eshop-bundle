<?php

namespace App\Akip\EshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * ParameterValueTranslation
 *
 * @ORM\Table(name="parameter_value_translation", indexes={@ORM\Index(name="fk_parameter_value_id", columns={"parameter_value_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\ParameterValueTranslationRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=false)
 * @Gedmo\Loggable()
 */
class ParameterValueTranslation
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @Groups({"list", "detail"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=false)
     * @Groups({"list", "detail"})
     * @Gedmo\Versioned()
     */
    private $value;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true, options={"default"="NULL"})
     * @Groups({"list", "detail"})
     * @Gedmo\Versioned()
     */
    private $description = '';

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=10, nullable=false)
     * @Gedmo\Versioned()
     */
    private $locale;

    /**
     * @var int
     *
     * @ORM\Column(name="sort", type="integer", nullable=false)
     * @Groups({"list", "detail"})
     * @Gedmo\Versioned()
     */
    private $sort;

    /**
     * @var ParameterValue
     *
     * @ORM\ManyToOne(targetEntity="ParameterValue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parameter_value_id", referencedColumnName="id")
     * })
     */
    private $parameterValue;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        if ($value === '' || !$value){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Value ');
        }
        $this->value = $value;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getSort(): ?int
    {
        return $this->sort;
    }

    public function setSort(int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    public function getParameterValue(): ?ParameterValue
    {
        return $this->parameterValue;
    }

    public function setParameterValue(?ParameterValue $parameterValue): self
    {
        $this->parameterValue = $parameterValue;

        return $this;
    }

    public function load($key, $data)
    {
        $this->setLocale($key);
        $this->setValue($data['value']);
        $this->setDescription($data['description']);
        $this->setSort(0);
        if (isset($data['sort']))
            $this->setSort($data['sort']);
    }


}
