<?php

namespace App\Akip\EshopBundle\Entity;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="payment", uniqueConstraints={@ORM\UniqueConstraint(name="slug", columns={"slug", "deleted_at"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\PaymentRepository")
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 * @UniqueEntity("slug")
 */
class Payment
{
    use SoftDeleteableEntity;
    use TimestampableEntity;

    const CASH = 'cash';
    const BANK_ACCOUNT = 'bank-account';
    const ONLINE = 'online';
    const CASH_ON_DELIVERY = 'cash-on-delivery';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private int $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private string $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     * @Groups({"detail" , "list"})
     * @Gedmo\Versioned()
     */
    private string $slug;

    /**
     * @var string
     * @Groups({"detail", "list"})
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     * @Gedmo\Versioned()
     */
    private ?string $description = '';

    /**
     * @var float
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=false)
     * @Gedmo\Versioned()
     * @Groups({"detail" , "list"})
     */
    private float $price = 0;

    /**
     *
     * @ORM\Column(name="options", type="text", nullable=false)
     * @Gedmo\Versioned()
     */
    private string $options = '{}';

    /**
     *
     * @ORM\Column(name="fields", type="text", nullable=false)
     * @Groups({"detail" , "list"})
     * @Gedmo\Versioned()
     */
    private string $fields = '{}';

    /**
     * @var Collection|Delivery[]
     * @ORM\ManyToMany(targetEntity="Delivery", inversedBy="payments")
     * @Groups({"paymentDetail"})
     */
    private $deliveries;

    /**
     * @var bool
     * @ORM\Column(name="enabled",type="boolean", nullable=false)
     * @Groups({"detail" , "list"})
     * @Gedmo\Versioned()
     */
    private bool $enabled = true;

    /**
     * @var bool
     * @Groups({"list", "detail"})
     * @ORM\Column(name="vat", type="boolean", nullable=false)
     * @Gedmo\Versioned()
     */
    private $vat;

    /**
     * @var Vat
     * @Groups({"list", "detail"})
     * @ORM\ManyToOne(targetEntity="Vat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="vat_id", referencedColumnName="id")
     * })
     */
    private $vat2;

    /**
     * @var bool
     * @ORM\Column(name="send_email",type="boolean", nullable=false)
     * @Groups({"detail" , "list"})
     * @Gedmo\Versioned()
     */
    private $sendEmail = true;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->deliveries = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        if ($slug === '' || !$slug) {
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Slug ');
        }

        $this->slug = $slug;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Payment
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Payment
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
    /**
     * @Groups({"detail" , "list"})
     */
    public function getOptions()
    {
        return json_decode($this->options, true);
    }

    /**
     * @param string $options
     * @return Payment
     */
    public function setOptions(string $options): self
    {
        $this->options = $options;
        return $this;
    }

    /**
     * @return string
     */
    public function getFields(): string
    {
        return $this->fields;
    }

    /**
     * @param string $fields
     * @return Payment
     */
    public function setFields(string $fields): self
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * @return Collection|Delivery[]
     */
    public function getDeliveries(): Collection
    {
        return $this->deliveries;
    }

    public function addDelivery(Delivery $delivery): self
    {
        if (!$this->deliveries->contains($delivery)) {
            $this->deliveries[] = $delivery;
        }
        return $this;
    }

    public function removeDelivery(Delivery $delivery): self
    {
        if ($this->deliveries->contains($delivery)) {
            $this->deliveries->removeElement($delivery);
        }
        return $this;
    }

    /**
     * @return bool
     */
    public function getEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     * @return Payment
     */
    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function getVat(): ?bool
    {
        return $this->vat;
    }

    public function setVat(bool $vat): self
    {
        $this->vat = $vat;

        return $this;
    }

    public function getVat2(): ?Vat
    {
        return $this->vat2;
    }

    public function setVat2(?Vat $vat2): self
    {
        $this->vat2 = $vat2;

        return $this;
    }

    public function load($data, Vat $vat)
    {
        $this->setEnabled($data['enabled']);
        $this->setName($data['name']);
        $this->setSlug($data['slug']);
        if (isset($data['price'])) {
            $this->setPrice($data['price']);
        } else {
            $this->setPrice(0);
        }
        $this->setVat($data['vat']);
        $this->setVat2($vat);
        $this->setOptions(json_encode($data['options']));
        $this->setSendEmail($data['sendEmail']);
    }

    public function checkOptions()
    {
        $data = json_decode($this->options, true);
        switch ($this->slug) {
            case self::CASH:
                $personalPickUp = new PersonlPickUp();
                $personalPickUp->checkFields($data);
                break;
            case self::BANK_ACCOUNT:
                $bankAccount = new BankAccount();
                $bankAccount->checkFields($data);;
                break;
            case self::ONLINE:
                $online = new Online();
                $online->checkFields($data);
                break;
            case self::CASH_ON_DELIVERY:
                $cashOnDelivery = new CashOnDelivery();
                $cashOnDelivery->checkFields($data);
                break;
        }
        $this->setOptions(json_encode($data));
    }

    private function calcPrice()
    {
        if ($this->getVat() === true) {
            $priceVat = $this->getPrice();
            if ($this->getVat2()->getRate() == 1){
                $priceWithoutVat = $priceVat;
            } else {
                $priceWithoutVat =$this->getPrice() * (1 - $this->getVat2()->getRate());
            }
        } else {
            $priceWithoutVat = $this->getPrice();
            if($this->getVat2()->getRate() == 1){
                $priceVat = $priceWithoutVat;
            } else {
                $priceVat = $this->getPrice() * (1 + $this->getVat2()->getRate());
            }
        }
        return [
            'withVat' => round($priceVat),
            'withoutVat' => round($priceWithoutVat),
        ];
    }

    /**
     * @Groups({"list", "detail"})
     */
    public function getWithVat()
    {
        return $this->calcPrice()['withVat'];
    }

    /**
     * @Groups({"list", "detail"})
     */
    public function getWithoutVat()
    {
        return $this->calcPrice()['withoutVat'];
    }

    /**
     * @Groups({"list", "detail"})
     */
    public function getVatRate()
    {
        return $this->getVat2()->getRate();
    }

    /**
     * @Groups({"list", "detail"})
     */
    public function getVatId()
    {
        return $this->getVat2()->getId();
    }

    public function getAllowedDeliveries() {
        $ids = [];
        foreach ($this->deliveries as $delivery) {
            $ids[] = $delivery->getId();
        }
        return implode(',',$ids);
    }

    /**
     * @return bool
     */
    public function isSendEmail(): bool
    {
        return $this->sendEmail;
    }

    /**
     * @param bool $sendEmail
     */
    public function setSendEmail(bool $sendEmail): self
    {
        $this->sendEmail = $sendEmail;

        return $this;
    }
}
