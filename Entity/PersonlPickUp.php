<?php


namespace App\Akip\EshopBundle\Entity;


class PersonlPickUp extends BaseDelivery
{
    public function __construct()
    {
        $this->zboziSlug = 'VLASTNI_VYDEJNI_MISTA';
        $this->options = [];
        $this->isAddressRequired = false;
    }
}
