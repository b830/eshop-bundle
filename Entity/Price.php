<?php

namespace App\Akip\EshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use phpDocumentor\Reflection\Types\This;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Price
 *
 * @ORM\Table(name="price", indexes={@ORM\Index(name="currency_id", columns={"currency_id"}), @ORM\Index(name="product_variant_id", columns={"product_variant_id"}), @ORM\Index(name="vat_id", columns={"vat_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\PriceRepository")
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 */
class Price
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"list", "detail"})
     */
    private $id;

    /**
     * @var float
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=false)
     * @Gedmo\Versioned()
     */
    private $price;

    /**
     * @var float
     * @ORM\Column(name="actual_price", type="float", precision=10, scale=0, nullable=true)
     * @Gedmo\Versioned()
     */
    private $actualPrice = null;

    /**
     * @var bool
     * @Groups({"list", "detail"})
     * @ORM\Column(name="vat", type="boolean", nullable=false)
     * @Gedmo\Versioned()
     */
    private $vat;

    /**
     * @var Vat
     * @Groups({"list", "detail"})
     * @ORM\ManyToOne(targetEntity="Vat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="vat_id", referencedColumnName="id")
     * })
     */
    private $vat2;

    /**
     * @var Currency
     * @Groups({"list"})
     * @ORM\ManyToOne(targetEntity="Currency")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="currency_id", referencedColumnName="id", columnDefinition="INT NOT NULL DEFAULT 1")
     * })
     */
    private $currency;

    /**
     * @var ProductVariant
     *
     * @ORM\ManyToOne(targetEntity="ProductVariant")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_variant_id", referencedColumnName="id")
     * })
     */
    private $productVariant;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        if ($price === ''){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Price ');
        }
        $this->price = $price;

        return $this;
    }

    public function getActualPrice(): ?float
    {
        return $this->actualPrice;
    }

    public function setActualPrice(float $actualPrice): self
    {
        $this->actualPrice = $actualPrice;

        return $this;
    }

    public function getVat(): ?bool
    {
        return $this->vat;
    }

    public function setVat(bool $vat): self
    {
        $this->vat = $vat;

        return $this;
    }

    public function getVat2(): ?Vat
    {
        return $this->vat2;
    }

    public function setVat2(?Vat $vat2): self
    {
        $this->vat2 = $vat2;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getProductVariant(): ?ProductVariant
    {
        return $this->productVariant;
    }

    public function setProductVariant(?ProductVariant $productVariant): self
    {
        $this->productVariant = $productVariant;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function load($data, Currency $currency, Vat $vat)
    {
        if (isset($data['price'])) {
            $this->setPrice($data['price']);
        } else {
            $this->setPrice(0);
        }
        if (isset($data['actualPrice'])) {
            $this->setActualPrice($data['actualPrice']);
        } else {
            $this->setActualPrice(0);
        }
        $this->setVat($data['vat']);
        $this->setCurrency($currency);
        $this->setVat2($vat);

        return $this;
    }

    /**
     * @return float|int|null
     * @Groups({"detail"})
     */
    public function getWithVat(){
        if ($this->getVat() === true) {
            return $this->getPrice();
        } elseif ($this->getVat() === false) {
            return $this->getPrice() * (1 + $this->getVat2()->getRate());
        }
    }
    /**
     * @return float|int|null
     * @Groups({"detail"})
     */
    public function getWithoutVat(){
        if ($this->getVat() === true) {
            return $this->getPrice() * (1 - $this->getVat2()->getRate());
        } elseif ($this->getVat() === false) {
            return $this->getPrice();
        }
    }

    public function __clone()
    {
        if ($this->id) {
            $this->id = null;
        }
    }
}
