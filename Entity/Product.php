<?php

namespace App\Akip\EshopBundle\Entity;

use App\Akip\CmsBundle\Entity\MenuItem;
use App\Akip\CmsBundle\Entity\Page;
use App\Akip\EshopBundle\Repository\ParameterRepository;
use App\Akip\FileManagerBundle\Entity\File;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\ProductRepository")
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 */
class Product
{
    use SoftDeleteableEntity;
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotNull()
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     * @Groups({"list", "detail"})
     * @Gedmo\Versioned()
     */
    private $enabled;

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="Flag", mappedBy="product", cascade={"persist"}))
     * @Groups({"detail"})
     */
    private $flags;

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="App\Akip\EshopBundle\Entity\Sign", mappedBy="product", cascade={"persist"}))
     * @Groups({"detail"})
     */
    private $signs;

    /**
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\ProductTranslation", mappedBy="product", orphanRemoval=true, cascade={"persist"})
     */
    private $translations;

    /**
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\ProductPhoto", mappedBy="product",  cascade={"persist"})
     */
    private $photos;


    /**
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\ProductParameter", mappedBy="product", cascade={"persist"})
     */
    private $parameters;

    /**
     * @var Collection|ProductVariant[]
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\ProductVariant", mappedBy="product", orphanRemoval=true, cascade={"persist"})
     */
    private $productVariants;

    /**
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\ProductCategory", mappedBy="product", cascade={"persist"})
     * @Groups({"detail"})
     */
    private $itemsCategory;

    /**
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\ProductRelated", mappedBy="product",  cascade={"persist"})
     */
    private $relatedProduct;

//    /**
//     * @var \Doctrine\Common\Collections\Collection
//     *
//     * @ORM\ManyToMany(targetEntity="App\Akip\FileManagerBundle\Entity\File", inversedBy="productInspiration")
//     * @ORM\JoinTable(name="product_inspiration",
//     *   joinColumns={
//     *     @ORM\JoinColumn(name="product_id", referencedColumnName="id")
//     *   },
//     *   inverseJoinColumns={
//     *     @ORM\JoinColumn(name="file_uuid", referencedColumnName="uuid")
//     *   }
//     * )
//     */
//    private $productInspiration;

    /**
     * @var int
     * @Groups({"detail", "list"})
     * @ORM\Column(name="sort", type="integer", nullable=false)
     * @Gedmo\Versioned()
     */
    private $sort = 999999;


    /**
     * @var bool
     *
     * @ORM\Column(name="with_variant", type="boolean", nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $withVariant = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\ProductAccessory", mappedBy="product",  cascade={"persist"})
     */
    private $accessories;

    /**
     * @var string
     * @ORM\Column(name="product_code", type="string", length=255, nullable=true)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
    */
    private $productCode = '';

    /**
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\ProductJoin", mappedBy="product",  cascade={"persist"})
     */
    private $productJoin;

    /**
     * @var bool
     *
     * @ORM\Column(name="display_on_homepage", type="boolean", nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $displayOnHomepage = false;

    /**
     * @var int
     *
     * @ORM\Column(name="home_page_sort", type="integer", nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $homePageSort = 999999;

    /**
     * @var Brand
     *
     * @ORM\ManyToOne(targetEntity="App\Akip\EshopBundle\Entity\Brand")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brand_id", referencedColumnName="id", nullable=true)
     * })
     * @Groups({"detail", "list"})
     */
    private $brand = null;

    /**
     * @var Page
     *
     * @ORM\ManyToOne(targetEntity="App\Akip\CmsBundle\Entity\Page")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="page_id", referencedColumnName="id", nullable=true)
     * })
     * @Groups({"detail", "list"})
     */
    private $page = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="variants_defined_by_size", type="boolean", nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $variantsDefinedBySize = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="variants_defined_by_color", type="boolean", nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $variantsDefinedByColor = false;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->category = new ArrayCollection();
        $this->flags = new ArrayCollection();
        $this->signs = new ArrayCollection();
        $this->translations = new ArrayCollection();
        $this->photos = new ArrayCollection();
        $this->parameters = new ArrayCollection();
        $this->productVariants = new ArrayCollection();
//        $this->productCategory = new ArrayCollection();
        $this->relatedProduct = new ArrayCollection();
        $this->itemsCategory = new ArrayCollection();
//        $this->productInspiration = new ArrayCollection();
        $this->accessories = new ArrayCollection();
        $this->productJoin = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @Groups({"list"})
     */
    public function getVariantCount(){
        return $this->productVariants ? $this->productVariants->count() : 0;
    }
    /**
     * @return Collection|Category[]
     */
//    public function getCategory(): Collection
//    {
//        return $this->category;
//    }

    public function variants()
    {
        $cr = Criteria::create()
            ->andWhere(Criteria::expr()->eq('main', false));
        $this->productVariants->matching($cr);
    }

    public function addCategory(Category $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
            $category->addProduct($this);
        }

        return $this;
    }

//    public function removeCategory(Category $category): self
//    {
//        if ($this->category->contains($category)) {
//            $this->category->removeElement($category);
//            $category->removeProduct($this);
//        }
//
//        return $this;
//    }

    /**
     * @return Collection|Flag[]
     */
    public function getFlagsObj(): Collection
    {
        return $this->flags;
    }

    public function getFlags()
    {
        $items = array();
        foreach ($this->getFlagsObj() as $flag) {
            $items[] = $flag;
        }
        return $items;
    }

    public function addFlag(Flag $flag): self
    {
        if (!$this->flags->contains($flag)) {
            $this->flags[] = $flag;
            $flag->addProduct($this);
        }

        return $this;
    }

    public function removeFlag(Flag $flag): self
    {
        if ($this->flags->contains($flag)) {
            $this->flags->removeElement($flag);
            $flag->removeProduct($this);
        }

        return $this;
    }

    /**
     * @return Collection|Sign[]
     */
    public function getSignsObj(): Collection
    {
        return $this->signs;
    }

    public function getSigns()
    {
        $items = array();
        foreach ($this->getSignsObj() as $sign) {
            $items[] = $sign;
        }
        return $items;
    }

    public function addSign(Sign $sign): self
    {
        if (!$this->signs->contains($sign)) {
            $this->signs[] = $sign;
            $sign->addProduct($this);
        }

        return $this;
    }

    public function removeSign(Sign $sign): self
    {
        if ($this->signs->contains($sign)) {
            $this->signs->removeElement($sign);
            $sign->removeProduct($this);
        }

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    //all product translations

    /**
     * @return Collection|ProductTranslation[]
     */
    public function getTranslationsObj(): Collection
    {
        return $this->translations;
    }

    //data for ProductController

    /**
     * @return array
     */
    public function getTranslations()
    {
        $items = array();
        foreach ($this->translations as $translation) {
            /**
             * @var $translation ProductTranslation
             */
            $items[$translation->getLocale()] = $translation;
        }
        return $items;
    }

    public function addTranslation(ProductTranslation $translation): self
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setProduct($this);
        }

        return $this;
    }

    public function removeTranslation(ProductTranslation $translation): self
    {
        if ($this->translations->contains($translation)) {
            $this->translations->removeElement($translation);
            // set the owning side to null (unless already changed)
            if ($translation->getProduct() === $this) {
                $translation->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductPhoto[]
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    public function addPhoto(ProductPhoto $photo): self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos[] = $photo;
            $photo->setProduct($this);
        }

        return $this;
    }

    public function removePhoto(ProductPhoto $photo): self
    {
        if ($this->photos->contains($photo)) {
            $this->photos->removeElement($photo);
            // set the owning side to null (unless already changed)
            if ($photo->getProduct() === $this) {
                $photo->setProduct(null);
            }
        }

        return $this;
    }

    // kontrala zda uz dana fotka existuje u tohoto produktu
    public function checkPhotoExist($productPhoto)
    {
        foreach ($this->getPhotos() as $photo) {
            if ($photo->getName() === $productPhoto->getName()) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * @return Collection|ProductParameter[]
     */
    public function getParameters(): Collection
    {
        $data = new ArrayCollection();
        foreach ($this->parameters as $item) {
            try {
                $item->getParameter()->getDeletedAt();
                $data->add($item);
            } catch (EntityNotFoundException $e) {
            }
//            if (!$item->getCategoryDeletedAt()){
//                $data[] = $item->getCategoryDeletedAt();
//                $data->add($item
//            }
        }
        return $data;
    }

    public function uniqueParameters()
    {
        $params = $this->parameters;
        if ($params->count() === 0) {
            return null;
        } else {
            foreach ($params as $param)
                $data[] = $param->getParameter();
            return array_unique($data, SORT_REGULAR);
        }
    }

    public function addParameter(ProductParameter $parameter): self
    {
        if (!$this->parameters->contains($parameter)) {
            $this->parameters[] = $parameter;
            $parameter->setProduct($this);
        }

        return $this;
    }

    public function removeParameter(ProductParameter $parameter): self
    {
        if ($this->parameters->contains($parameter)) {
            $this->parameters->removeElement($parameter);
            // set the owning side to null (unless already changed)
            if ($parameter->getProduct() === $this) {
                $parameter->setProduct(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection|ProductVariant[]
     */
    public function getProductVariants(): Collection
    {
        return $this->productVariants;
    }

    public function addProductVariant(ProductVariant $productVariant): self
    {
        if (!$this->productVariants->contains($productVariant)) {
            $this->productVariants[] = $productVariant;
            $productVariant->setProduct($this);
        }

        return $this;
    }

    public function removeProductVariant(ProductVariant $productVariant): self
    {
        if ($this->productVariants->contains($productVariant)) {
            $this->productVariants->removeElement($productVariant);
            // set the owning side to null (unless already changed)
            if ($productVariant->getProduct() === $this) {
                $productVariant->setProduct(null);
            }
        }

        return $this;
    }

    //mainVariant
    public function getMainProductVariants()
    {
        $cr = Criteria::create()
            ->andWhere(Criteria::expr()->eq('main', true));
        /**
         * @var $variant ProductVariant
         */
        $variant = $this->productVariants->matching($cr)[0];
        if ($variant)
            return $variant;
        else
            return null;
    }

    //mainPhoto
    public function getMainPhoto()
    {
        $cr = Criteria::create()
            ->andWhere(Criteria::expr()->eq('main', true));
        /**
         * @var $variant ProductPhoto
         */
        $variant = $this->photos->matching($cr)[0];
        if ($variant)
            return $variant;
        else
            return null;
    }

    public function getMainCount($collection)
    {
        $cr = Criteria::create()
            ->andWhere(Criteria::expr()->eq('main', true));
        return $collection->matching($cr)->count();
    }

//    /**
//     * @return Collection|ProductCategory[]
//     */
//    public function getProductCategories(): Collection
//    {
//        return $this->productCategory;
//    }
//
//    public function addProductCategory(ProductCategory $productCategory): self
//    {
//        if (!$this->productCategory->contains($productCategory)) {
//            $this->productCategory[] = $productCategory;
//            $productCategory->addProduct($this);
//        }
//
//        return $this;
//    }
//
//    public function removeProductCategory(ProductCategory $productCategory): self
//    {
//        if ($this->productCategory->contains($productCategory)) {
//            $this->productCategory->removeElement($productCategory);
//            $productCategory->removeProduct($this);
//        }
//
//        return $this;
//    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        if ($name === '' || !$name) {
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Name ');
        }
        $this->name = $name;

        return $this;
    }

    public function getSort(): ?int
    {
        return $this->sort;
    }

    public function setSort(int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

//    /**
//     * @return Collection|Product[]
//     */
//    public function getRelatedProduct(): Collection
//    {
//        return $this->relatedProduct;
//    }
//
//    public function addRelatedProduct(Product $relatedProduct): self
//    {
//        if (!$this->relatedProduct->contains($relatedProduct)) {
//            $this->relatedProduct[] = $relatedProduct;
//        }
//
//        return $this;
//    }
//
//    public function removeRelatedProduct(Product $relatedProduct): self
//    {
//        if ($this->relatedProduct->contains($relatedProduct)) {
//            $this->relatedProduct->removeElement($relatedProduct);
//        }
//
//        return $this;
//    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return Collection|ProductCategory[]
     *
     */
    public function getItemsCategory(): Collection
    {
        $data = new ArrayCollection();
        foreach ($this->itemsCategory as $item) {
            try {
                $item->getCategory()->getDeletedAt();
                $data->add($item);
            } catch (EntityNotFoundException $e) {
            }
//            if (!$item->getCategoryDeletedAt()){
//                $data[] = $item->getCategoryDeletedAt();
//                $data->add($item
//            }
        }
        return $data;
    }

    public function addItemsCategory(ProductCategory $itemsCategory): self
    {
        if (!$this->itemsCategory->contains($itemsCategory)) {
            $this->itemsCategory[] = $itemsCategory;
            $itemsCategory->setProduct($this);
        }

        return $this;
    }

    public function getMainCategory()
    {
        return $this->itemsCategory->filter(function (ProductCategory $c) {
            return $c->getIsMain() === true;
        })->first()->getCategory();
    }

    public function removeItemsCategory(ProductCategory $itemsCategory): self
    {
        if ($this->itemsCategory->contains($itemsCategory)) {
            $this->itemsCategory->removeElement($itemsCategory);
//            $itemsCategory->removeProduct($this);
//             set the owning side to null (unless already changed)
            if ($itemsCategory->getProduct() === $this) {
                $itemsCategory->setProduct(null);
            }
        }
        return $this;
    }

//    /**
//     * @return Collection|File[]
//     */
//    public function getProductInspiration(): Collection
//    {
//        return $this->productInspiration;
//    }
//
//    public function addProductInspiration(File $productInspiration): self
//    {
//        if (!$this->productInspiration->contains($productInspiration)) {
//            $this->productInspiration[] = $productInspiration;
//        }
//
//        return $this;
//    }
//
//    public function removeProductInspiration(File $productInspiration): self
//    {
//        if ($this->productInspiration->contains($productInspiration)) {
//            $this->productInspiration->removeElement($productInspiration);
//        }
//
//        return $this;
//    }

    public function getWithVariant(): ?bool
    {
        return $this->withVariant;
    }

    public function setWithVariant(bool $withVariant): self
    {
        $this->withVariant = $withVariant;

        return $this;
    }

//    /**
//     * @return Collection|Product[]
//     */
//    public function getAccessories(): Collection
//    {
//        return $this->accessories;
//    }
//
//    public function addAccessory(Product $accessory): self
//    {
//        if (!$this->accessories->contains($accessory)) {
//            $this->accessories[] = $accessory;
//        }
//
//        return $this;
//    }
//
//    public function removeAccessory(Product $accessory): self
//    {
//        if ($this->accessories->contains($accessory)) {
//            $this->accessories->removeElement($accessory);
//        }
//
//        return $this;
//    }

    /**
     * @return string|null
     * @Groups({"list"})
     */
    public function getVariantName(){
        if ($this->getMainProductVariants()) {
            return $this->getMainProductVariants()->getName();
        } else {
            return '';
        }
    }

    public function getPrice(){
        if ($this->getMainProductVariants()){
            return $this->getMainProductVariants()->getPrice();
        }
        return null;
    }

    /**
     * @return string|null
     */
    public function getFullName(){
//        $variantName = $this->getMainProductVariants() ? $this->getMainProductVariants()->getName() : '';
//        $delimiter= $variantName === '' ? '' : ' - ';
//        return "{$this->getName()}{$delimiter}{$variantName}";
        return "{$this->getName()}";
    }

    /**
     * @Groups({"allList", "list"})
     * @return string|null
     */
    public function getDisplay(){
        $variantName = $this->getMainProductVariants() ? $this->getMainProductVariants()->getName() : '';
        return "{$this->getName()} ({$variantName})";
    }

    /**
     * @Groups({"allList", "list"})
     * @return string|null
     */
    public function getUrl(){
        return $this->getMainPhoto() ? $this->getMainPhoto()->getPhoto()->getChildren()[1]->getUrl() : 'https://i.redd.it/s8lk86v3r2m11.png';
    }

    public function getProductCode(): ?string
    {
        return $this->productCode;
    }

    public function setProductCode(string $code): self
    {
        $this->productCode = $code;

        return $this;
    }

    /**
     * @return Collection|ProductJoin[]
     */
    public function getProductJoin(): Collection
    {
        return $this->productJoin;
    }

    public function addProductJoin(ProductJoin $productJoin): self
    {
        if (!$this->photos->contains($productJoin)) {
            $this->photos[] = $productJoin;
            $productJoin->setProduct($this);
        }

        return $this;
    }

    public function removeProductJoin(ProductJoin $productJoin): self
    {
        if ($this->photos->contains($productJoin)) {
            $this->photos->removeElement($productJoin);
            // set the owning side to null (unless already changed)
            if ($productJoin->getProduct() === $this) {
                $productJoin->setProduct(null);
            }
        }

        return $this;
    }

    public function getDisplayOnHomepage(): ?bool
    {
        return $this->displayOnHomepage;
    }

    public function setDisplayOnHomepage(bool $displayOnHomepage): self
    {
        $this->displayOnHomepage = $displayOnHomepage;

        return $this;
    }

    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    public function setBrand(?Brand $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getPage(): ?Page
    {
        return $this->page;
    }

    public function setPage(?Page $page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return bool
     */
    public function getVariantsDefinedBySize(): bool
    {
        return $this->variantsDefinedBySize;
    }

    /**
     * @param bool $variantsDefinedBySize
     */
    public function setVariantsDefinedBySize(bool $variantsDefinedBySize): self
    {
        $this->variantsDefinedBySize = $variantsDefinedBySize;

        return $this;
    }

    /**
     * @return bool
     */
    public function getVariantsDefinedByColor(): bool
    {
        return $this->variantsDefinedByColor;
    }

    /**
     * @param bool $variantsDefinedByColor
     */
    public function setVariantsDefinedByColor(bool $variantsDefinedByColor): self
    {
        $this->variantsDefinedByColor = $variantsDefinedByColor;

        return $this;
    }


    public function __clone() {
        if ($this->id) {
            $this->id = null;
            $newPhotos = new ArrayCollection();
            /** @var ProductPhoto $photo */
            foreach ($this->photos as $photo) {
                $photoClone = clone $photo;
                $photoClone->setProduct($this);
                $newPhotos->add($photoClone);
            }
            $newParameters = new ArrayCollection();
            /** @var ProductParameter $parameter */
            foreach ($this->parameters as $parameter) {
                $parameterClone = clone $parameter;
                $parameterClone->setProduct($this);
                $newParameters->add($parameterClone);
            }
            $newTranslations = new ArrayCollection();
            /** @var ProductTranslation $productTranslation */
            foreach ($this->translations as $productTranslation) {
                $newTranslation = clone $productTranslation;
                $newTranslation->setProduct($this);
                $newTranslation->setSlug($productTranslation->getSlug().'-1');
                $newTranslations->add($newTranslation);
            }
            $newCategories = new ArrayCollection();
            /** @var ProductCategory $itemCategory */
            foreach ($this->itemsCategory as $itemCategory) {
                $newItemCategory = clone $itemCategory;
                $newItemCategory->setProduct($this);
                $newCategories->add($newItemCategory);
            }

            $newFlags = new ArrayCollection();
            /** @var Flag $flag */
            foreach ($this->flags as $flag) {
                $newFlag = clone $flag;
                $newFlag->addProduct($this);
                $newFlags->add($newFlag);
            }
            $newVariants = new ArrayCollection();
            foreach ($this->productVariants as $variant) {
                $newVariant = clone $variant;
                if ($variant->getMain()) {
                    $newVariant->setMain(true);
                }
                if ($this->getVariantsDefinedBySize() || $this->getVariantsDefinedByColor()) {
                    foreach ($variant->getParameters() as $parameter) {
                        foreach ($parameter->getParameter()->get_Translations() as $translation) {
                            if (
                                ($translation->getSlug() === ParameterRepository::COLOR_SLUG && $this->getVariantsDefinedByColor())
                                ||
                                ($translation->getSlug() === ParameterRepository::SIZE_SLUG && $this->getVariantsDefinedBySize())
                            ) {
                                $newParameter = clone $parameter;
                                $newParameter->setProductVariant($newVariant);
                                $newVariant->addParameter($newParameter);
                            }
                        }
                    }
                }
                $newVariant->setProduct($this);
                $newVariants->add($newVariant);
            }
            $newRelated = new ArrayCollection();
            /** @var ProductRelated $item */
            foreach ($this->relatedProduct as $item) {
                $newItem = clone $item;
                $newItem->setProduct($this);
                $newRelated->add($newItem);
            }

            $newAccessories = new ArrayCollection();
            /** @var ProductAccessory $item */
            foreach ($this->accessories as $item) {
                $newItem = clone $item;
                $newItem->setProduct($this);
                $newAccessories->add($newItem);
            }
            $this->photos = $newPhotos;
            $this->parameters = $newParameters;
            $this->translations = $newTranslations;
            $this->itemsCategory = $newCategories;
            $this->accessories = $newAccessories;
            $this->relatedProduct = $newRelated;
            $this->productVariants = $newVariants;
//            $this->flags = $newFlags;
        }
    }

    /**
     * @return int
     */
    public function getHomePageSort(): int
    {
        return $this->homePageSort;
    }

    /**
     * @param int $homePageSort
     * @return Product
     */
    public function setHomePageSort(int $homePageSort): self
    {
        $this->homePageSort = $homePageSort;

        return $this;
    }

    public function getVariantBySizeId($sizeId){
        if (!$this->variantsDefinedBySize || !$sizeId) {
            return null;
        }
        $sizeId = count($sizeId) === 1 ? $sizeId[0] : $sizeId;
        foreach ($this->productVariants as $variant) {
            if (is_array($sizeId) && in_array($variant->getSizeParameterId(), $sizeId)) {
                return $variant;
            } elseif ($sizeId == $variant->getSizeParameterId()) {
                return $variant;
            }
        }
        return null;
    }
}
