<?php

namespace App\Akip\EshopBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\ProductCategoryRepository")
 * @ORM\Table(name="product_category", uniqueConstraints={@ORM\UniqueConstraint(name="pair", columns={"product_id", "category_id"})})
 * @Gedmo\Loggable()
 */
class ProductCategory
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Akip\EshopBundle\Entity\Product", cascade={"persist"})
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="App\Akip\EshopBundle\Entity\Category", cascade={"persist"})
     */
    private $category;

    /**
     * @Groups({"list", "detail"})
     * @ORM\Column(name="is_main", type="boolean")
     * @ORM\Column(type="boolean")
     * @Gedmo\Versioned()
     */
    private $isMain = false;

    /**
     * @var int
     * @Groups({"detail", "list"})
     * @ORM\Column(name="sort", type="integer", nullable=false)
     * @Gedmo\Versioned()
     */
    private $sort = 999999;

    public function __construct()
    {
    }

    /**
     * @return mixed
     * @Groups({"list", "detail"})
     */
    public function getId()
    {
        return $this->category->getId();
    }

    /**
     * @return mixed
     * @Groups({"list", "detail"})
     */
    public function getName()
    {
        return $this->category->getName();
    }

    /**
     * @Groups({"list", "detail"})
     */
    public function getIcon()
    {
       return $this->category->getIcon();
    }

    /**
     * @return Collection|Product[]
     */
    public function getProduct()//: Collection
    {
        return $this->product;
    }

    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @return bool|null
     */
    public function getIsMain(): ?bool
    {
        return $this->isMain;
    }

    public function getCategoryDeletedAt(): ?\DateTimeInterface
    {
        return $this->category->getDeletedAt();
    }

    public function setIsMain(bool $isMain): self
    {
        $this->isMain = $isMain;

        return $this;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getSort(): ?int
    {
        return $this->sort;
    }

    public function setSort(int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }


    public function categoryWithIsMain()
    {
        return $this->category;
    }

    public function __clone()
    {
        if ($this->id) {
            $this->id = null;
        }
    }
}
