<?php

namespace App\Akip\EshopBundle\Entity;


use App\Akip\FileManagerBundle\Entity\File;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\ProductInspirationRepository")
 * @ORM\Table(name="product_inspiration", uniqueConstraints={@ORM\UniqueConstraint(name="fk_unique", columns={"product_id", "file_uuid"})})
 * @Gedmo\Loggable()
 */
class ProductInspiration
{

    use TimestampableEntity;
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Akip\EshopBundle\Entity\Product", cascade={"persist"})
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="App\Akip\FileManagerBundle\Entity\File", cascade={"persist"})
     * @ORM\JoinColumn(name="file_uuid", referencedColumnName="uuid")
     */
    private $file;

    /**
     * @Groups({"list", "detail"})
     * @ORM\Column(name="sort", type="integer", nullable=true)
     * @ORM\Column(type="integer")
     * @Gedmo\Versioned()
     */
    private $sort = 0;

    /**
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\ProductInspirationTranslation", mappedBy="productInspiration", orphanRemoval=true, cascade={"persist"})
     */
    private $translations;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    public function get_Id(): ?int
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product->getId();
    }

    /**
     * @return mixed
     */
    public function getFileUuid()
    {
        return $this->file->getUuid();
    }


    /**
     * @return Collection|Product[]
     */
    public function getProduct()//: Collection
    {
        return $this->product;
    }

    /**
     * @return File
     * @Groups({"list"})
     */
    public function getFile(): File
    {
        return $this->file;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    public function setSort(int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function setFile(?File $file): self
    {
        $this->file = $file;

        return $this;
    }
    /**
     * @return Collection|ProductInspirationTranslation[]
     */
    public function getTranslationsObj(): Collection
    {
        return $this->translations;
    }

    public function getTranslations()
    {
        $items = array();
        foreach ($this->translations as $translation) {
            $items[$translation->getLocale()] = $translation;
        }
        return $items;
    }

    public function addTranslation(ProductInspirationTranslation $translation): self
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setProductInspiration($this);
        }

        return $this;
    }

    public function removeTranslation(ProductInspirationTranslation $translation): self
    {
        if ($this->translations->contains($translation)) {
            $this->translations->removeElement($translation);
            // set the owning side to null (unless already changed)
            if ($translation->getProductInspiration() === $this) {
                $translation->setProductInspiration(null);
            }
        }

        return $this;
    }

//    public function categoryWithIsMain()
//    {
//        return $this->category;
//    }
}
