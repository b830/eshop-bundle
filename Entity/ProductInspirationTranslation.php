<?php

namespace App\Akip\EshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProductInspirationTranslation
 *
 * @ORM\Table(name="product_inspiration_translation", indexes={@ORM\Index(name="product_inspiration_id", columns={"product_inspiration_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\ProductInspirationTranslationRepository")
 * @Gedmo\Loggable()
 */
class ProductInspirationTranslation
{
    use TimestampableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"list","detail"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="description", type="text", length=0, nullable=false)
     * @Groups({"list","detail"})
     * @Gedmo\Versioned()
     */
    private $description = '';

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="locale", type="string", length=10, nullable=false)
     * @Groups({"list","detail"})
     * @Gedmo\Versioned()
     */
    private $locale;

    /**
     * @var string
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     * @Gedmo\Versioned()
     */
    private $slug = '';

    /**
     * @var ProductInspiration
     *
     * @ORM\ManyToOne(targetEntity="ProductInspiration")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_inspiration_id", referencedColumnName="id")
     * })
     */
    private $productInspiration;

    public function getId(): ?int
    {
        return $this->id;
    }
    public function getProductInspirationId(): ?int
    {
        return $this->productInspiration->getId();
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        if ($slug === '' || !$slug){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Slug ');
        }
        $this->slug = $slug;

        return $this;
    }

    public function getProductInspiration(): ?ProductInspiration
    {
        return $this->productInspiration;
    }

    public function setProductInspiration(?ProductInspiration $productInspiration): self
    {
        $this->productInspiration = $productInspiration;

        return $this;
    }

    public function load($data, $locale, ProductInspiration $productInspiration)
    {
        $this->setLocale($locale);
        $this->setDescription($data['description']);
        $this->setProductInspiration($productInspiration);
        if (isset($data['slug'])){
            $this->setSlug($data['slug']);
        }
    }


}
