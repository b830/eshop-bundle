<?php

namespace App\Akip\EshopBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\ProductJoinRepository")
 * @ORM\Table(name="product_join")
 */
class ProductJoin
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     * @Groups({"list", "detail", "demandDetail"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Akip\EshopBundle\Entity\Product", cascade={"persist"})
     */
    private $product;

    /**
     * @Groups({"list", "detail"})
     * @ORM\Column(name="join_id", type="integer", nullable=false)
     * @ORM\Column(type="integer")
     * @Groups({"list", "detail", "demandDetail"})
     */
    private $joinId;

    /**
     * @Groups({"list", "detail"})
     * @ORM\Column(name="join_name", type="string", nullable=false)
     * @ORM\Column(type="string")
     * @Groups({"list", "detail", "demandDetail"})
     */
    private $joinName;

    /**
     * @Groups({"list", "detail"})
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     * @ORM\Column(type="integer")
     * @Groups({"list", "detail", "demandDetail"})
     */
    private $quantity;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product->getId();
    }

    /**
     * @return Collection|Product[]
     */
    public function getProduct()//: Collection
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return int
     */
    public function getJoinId(): int
    {
        return $this->joinId;
    }

    public function setJoinId(int $joinId): self
    {
        $this->joinId = $joinId;

        return $this;
    }

    /**
     * @return string
     */
    public function getJoinName(): string
    {
        return $this->joinName;
    }

    public function setJoinName(string $joinName): self
    {
        $this->joinName = $joinName;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

}
