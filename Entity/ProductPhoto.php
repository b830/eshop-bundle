<?php

namespace App\Akip\EshopBundle\Entity;

use App\Akip\FileManagerBundle\Entity\File;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProductPhoto
 *
 * @ORM\Table(name="product_photo", indexes={@ORM\Index(name="product_id", columns={"product_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\ProductPhotoRepository")
 * @Gedmo\Loggable()
 */
class ProductPhoto
{
    use TimestampableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private $id;

    /**
     * @var File
     *
     * @ORM\ManyToOne(targetEntity="App\Akip\FileManagerBundle\Entity\File")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="photo", referencedColumnName="uuid")
     * })
     *
     */
    private $photo;

    /**
     * @var bool
     * @Assert\NotBlank()
     * @ORM\Column(name="main", type="boolean", nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $main;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $name;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     *
     */
    private $product;

    /**
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\ProductPhotoTranslation", mappedBy="productPhoto", orphanRemoval=true, cascade={"persist"})
     */
    private $translations;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="sort", type="integer", nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $sort;


    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getMain(): ?bool
    {
        return $this->main;
    }

    public function setMain(bool $main): self
    {
        $this->main = $main;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        if ($name === '' || !$name){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Name ');
        }
        $this->name = $name;

        return $this;
    }

    public function getSort(): ?int
    {
        return $this->sort;
    }

    public function setSort(int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|ProductPhotoTranslation[]
     */
    public function getTranslationsObj(): Collection
    {
        return $this->translations;
    }

    /**
     * @return array
     */
    public function getTranslations()
    {
        $items = array();
        foreach ($this->translations as $translation) {
            /**
             * @var $translation ProductPhotoTranslation
             */
            $items[$translation->getLocale()] = $translation;
        }
        return $items;
    }

    public function addTranslation(ProductPhotoTranslation $translation): self
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setProductPhoto($this);
        }

        return $this;
    }

    public function removeTranslation(ProductPhotoTranslation $translation): self
    {
        if ($this->translations->contains($translation)) {
            $this->translations->removeElement($translation);
            // set the owning side to null (unless already changed)
            if ($translation->getProductPhoto() === $this) {
                $translation->setProductPhoto(null);
            }
        }

        return $this;
    }

    public function getPhoto(): ?File
    {
        return $this->photo;
    }

    public function setPhoto(?File $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getUrl(){
        return $this->photo->getUrl();
    }

    public function __clone()
    {
        if ($this->id) {
            $this->id = null;
        }
    }
}
