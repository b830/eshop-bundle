<?php

namespace App\Akip\EshopBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\ProductRelatedRepository")
 * @ORM\Table(name="product_related", uniqueConstraints={@ORM\UniqueConstraint(name="pair", columns={"product_id", "related_product_id"})})
 * @Gedmo\Loggable()
 */
class ProductRelated
{

    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Akip\EshopBundle\Entity\Product", cascade={"persist"})
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="App\Akip\EshopBundle\Entity\Product", cascade={"persist"})
     * @ORM\JoinColumn(name="related_product_id", referencedColumnName="id")
     */
    private $related;

    /**
     * @Groups({"list", "detail"})
     * @ORM\Column(name="sort", type="integer", nullable=true)
     * @ORM\Column(type="integer")
     * @Gedmo\Versioned()
     */
    private $sort = 0;

    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product->getId();
    }

    /**
     * @return mixed
     */
    public function getRelatedId()
    {
        return $this->related->getId();
    }


    /**
     * @return Collection|Product[]
     */
    public function getProduct()//: Collection
    {
        return $this->product;
    }

    /**
     * @return Product
     * @Groups({"list"})
     */
    public function getRelated(): Product
    {
        return $this->related;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    public function setSort(int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function setRelated(?Product $related): self
    {
        $this->related = $related;

        return $this;
    }


//    public function categoryWithIsMain()
//    {
//        return $this->category;
//    }

    public function __clone()
    {
        if ($this->id) {
            $this->id = null;
        }
    }
}
