<?php

namespace App\Akip\EshopBundle\Entity;

use App\Akip\EshopBundle\Repository\ParameterRepository;
use App\Akip\FileManagerBundle\Entity\File;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use phpDocumentor\Reflection\Element;
use phpDocumentor\Reflection\Types\This;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use function Symfony\Component\DependencyInjection\Loader\Configurator\expr;

/**
 * ProductVariant
 *
 * @ORM\Table(name="product_variant", indexes={@ORM\Index(name="product_id", columns={"product_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\ProductVariantRepository")
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 */
class ProductVariant
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"list", "detail", "allList"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotNull()
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Groups({"detail", "list", "allList"})
     * @Gedmo\Versioned()
     */
    private $name = '';


    /**
     * @var bool
     *
     * @ORM\Column(name="main", type="boolean", nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $main;

    /**
     * @var int
     * @Groups({"list", "detail"})
     * @ORM\Column(name="sort", type="integer", nullable=false)
     * @Gedmo\Versioned()
     */
    private $sort = '0';

    /**
     * @var integer
     * @Groups({"list", "detail"})
     * @ORM\Column(name="in_stock", type="integer", nullable=false)
     * @Gedmo\Versioned()
     */
    private $inStock = 0;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     * @Groups({"list", "detail", "allList"})
     * @Gedmo\Versioned()
     */
    private $enabled = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="flag_over_write", type="boolean", nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $flagOverWrite = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="sign_over_write", type="boolean", nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $signOverWrite = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="inspiration_over_write", type="boolean", nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $inspirationOverWrite = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="photo_over_write", type="boolean", nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $photoOverWrite = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="category_over_write", type="boolean", nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $categoryOverWrite = false;

    /**
     * @var Product
     * @Gedmo\Versioned()
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Customer", mappedBy="productVariant")
     */
    private $customer;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Flag", inversedBy="productVariant")
     * @ORM\JoinTable(name="product_variant_flag",
     *   joinColumns={
     *     @ORM\JoinColumn(name="product_variant_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="flags_id", referencedColumnName="id")
     *   }
     * )
     */
    private $flags;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Akip\EshopBundle\Entity\Sign", inversedBy="productVariant")
     * @ORM\JoinTable(name="product_variant_sign",
     *   joinColumns={
     *     @ORM\JoinColumn(name="product_variant_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="signs_id", referencedColumnName="id")
     *   }
     * )
     */
    private $signs;

    /**
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\ProductVariantTranslation", mappedBy="productVariant", orphanRemoval=true, cascade={"persist"})
     * @Groups({"list"})
     */
    private $translations;

    /**
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\ProductVariantPhoto", mappedBy="productVariant", orphanRemoval=true, cascade={"persist"})
     */
    private $photos;

    /**
     * @var float
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\Price", mappedBy="productVariant", orphanRemoval=true, cascade={"persist"}))
     */
    private $price;

    /**
     * @var ArrayCollection
     */
    private $categories;


    /**
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\ProductVariantParameter", mappedBy="productVariant", orphanRemoval=true, cascade={"persist"})
     */
    private $parameters;

//    /**
//     * @var \Doctrine\Common\Collections\Collection
//     *
//     * @ORM\ManyToMany(targetEntity="App\Akip\FileManagerBundle\Entity\File", inversedBy="productInspiration")
//     * @ORM\JoinTable(name="product_variant_inspiration",
//     *   joinColumns={
//     *     @ORM\JoinColumn(name="product_variant_id", referencedColumnName="id")
//     *   },
//     *   inverseJoinColumns={
//     *     @ORM\JoinColumn(name="file_uuid", referencedColumnName="uuid")
//     *   }
//     * )
//     */
//    private $productVariantInspiration;

    /**
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\ProductVariantCategory", mappedBy="productVariant", cascade={"persist"})
     * @Groups({"detail"})
     */
    private $itemsCategory;

    /**
     * @var StockStatus
     *
     * @ORM\ManyToOne(targetEntity="App\Akip\EshopBundle\Entity\StockStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="actual_stock_status_id", referencedColumnName="id", nullable=true)
     * })
     * @Groups({"detail"})
     *
     */
    private $actualStockStatus = null;

    /**
     * @var StockStatus
     *
     * @ORM\ManyToOne(targetEntity="App\Akip\EshopBundle\Entity\StockStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="out_of_stock_status_id", referencedColumnName="id", nullable=true)
     * })
     * @Groups({"detail"})
     *
     */
    private $outOfStockStatus = null;

    /**
     * @var string
     * @ORM\Column(name="ean", type="string", length=255, nullable=true)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $ean = '';

    /**
     * @var bool
     *
     * @ORM\Column(name="add_product_slug", type="boolean", nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $addProductSlug = true;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->customer = new ArrayCollection();
        $this->translations = new ArrayCollection();
        $this->photos = new ArrayCollection();
        $this->price = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->flags = new ArrayCollection();
        $this->signs = new ArrayCollection();
        $this->parameters = new ArrayCollection();
        $this->itemsCategory = new ArrayCollection();
        $this->productVariantInspiration = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMain(): ?bool
    {
        return $this->main;
    }

    public function setMain(bool $main): self
    {
        $this->main = $main;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @Groups({"allList", "list"})
     * @return string|null
     */
    public function getDisplay()
    {
        $name = "{$this->product->getName()} ({$this->getName()})";
        return $name;
    }

    /**
     * @return string|null
     */
    public function getFullName()
    {
        $name = $this->product->getName();
        if ($this->name !== '') {
            $name .= " {$this->name}";
        }
        return $name;
    }

    /**
     * @Groups({"allList", "list"})
     * @return string|null
     */
    public function getUrl()
    {
        return $this->getMainPhoto() ? $this->getMainPhoto()->getPhoto()->getChildren()[1]->getUrl() : 'https://i.redd.it/s8lk86v3r2m11.png';
    }

    public function uniqueParameters()
    {
        $params = $this->parameters;
        if ($params->count() === 0) {
            return null;
        } else {
            foreach ($params as $param)
                $data[] = $param->getParameter();
            return array_unique($data, SORT_REGULAR);
        }
    }

    /**
     * @Groups({"detail"})
     */
    public function getVariants()
    {
        return $this->product->variants();
//        $variants->matching($cr);
//        return $variants;
    }

    /**
     * @return Collection|Customer[]
     */
    public function getCustomer(): Collection
    {
        return $this->customer;
    }

    public function addCustomer(Customer $customer): self
    {
        if (!$this->customer->contains($customer)) {
            $this->customer[] = $customer;
            $customer->addProductVariant($this);
        }

        return $this;
    }

    public function removeCustomer(Customer $customer): self
    {
        if ($this->customer->contains($customer)) {
            $this->customer->removeElement($customer);
            $customer->removeProductVariant($this);
        }

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return Collection|ProductVariantTranslation[]
     */
    public function getTranslationsObj(): Collection
    {
        return $this->translations;
    }

    /**
     * @return array
     * @Groups({"list", "detail"})
     */
    public function getTranslations()
    {
        $items = array();
        /**
         * @var $translation ProductVariantTranslation
         */
        foreach ($this->getTranslationsObj() as $translation) {

            $items[$translation->getLocale()] =
                $translation;
        }
        return $items;
    }

    public function addTranslation(ProductVariantTranslation $translation): self
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setProductVariant($this);
        }

        return $this;
    }

    public function removeTranslation(ProductVariantTranslation $translation): self
    {
        if ($this->translations->contains($translation)) {
            $this->translations->removeElement($translation);
            // set the owning side to null (unless already changed)
            if ($translation->getProductVariant() === $this) {
                $translation->setProductVariant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductVariantPhoto[]
     */
    public function getPhotosObj(): Collection
    {
        return $this->photos;
    }

    public function addPhoto(ProductVariantPhoto $photo): self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos[] = $photo;
            $photo->setProductVariant($this);
        }

        return $this;
    }

    public function removePhoto(ProductVariantPhoto $photo): self
    {
        if ($this->photos->contains($photo)) {
            $this->photos->removeElement($photo);
            // set the owning side to null (unless already changed)
            if ($photo->getProductVariant() === $this) {
                $photo->setProductVariant(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|Price[]
     */
    public function getPriceObj() //: Collection
    {
        return $this->price;
    }

    /**
     * @Groups({"list", "detail"})
     */
    public function getPrice()
    {
        $data = array();
        /**
         * @var $item Price
         */
        foreach ($this->price as $item) {
            if ($item->getVat() === true) {
                $priceVat = $item->getPrice();
                $actualPriceVat = $item->getActualPrice();
                if ($item->getVat2()->getRate() == 1) {
                    $priceWithoutVat = $priceVat;
                    $actualPriceWithoutVat = $actualPriceVat;
                } else {
                    $priceWithoutVat = $item->getPrice() * (1 - $item->getVat2()->getRate());
                    $actualPriceWithoutVat = $item->getActualPrice() * (1 - $item->getVat2()->getRate());
                }
            } else {
                $priceWithoutVat = $item->getPrice();
                $actualPriceWithoutVat = $item->getActualPrice();
                if ($item->getVat2()->getRate() == 1) {
                    $priceVat = $priceWithoutVat;
                    $actualPriceVat = $actualPriceWithoutVat;
                } else {
                    $priceVat = $item->getPrice() * (1 + $item->getVat2()->getRate());
                    $actualPriceVat = $item->getActualPrice() * (1 + $item->getVat2()->getRate());
                }
            }
            $data[$item->getCurrency()->getSymbol()] = [
                'withVat' => round($priceVat),
                'withoutVat' => round($priceWithoutVat),
                'actualWithVat' => round($actualPriceVat),
                'actualWithoutVat' => round($actualPriceWithoutVat),
                'vatId' => $item->getVat2()->getId(),
                'vatRate' => $item->getVat2()->getRate(),
                'vat' => $item->getVat() === true ? 'true' : 'false',
                'symbol' => $item->getCurrency()->getSymbol(),
            ];
        }
        return $data;
    }

    public function addPrice(Price $price): self
    {
        if (!$this->price->contains($price)) {
            $this->price[] = $price;
            $price->setProductVariant($this);
        }

        return $this;
    }

    public function removePrice(Price $price): self
    {
        if ($this->price->contains($price)) {
            $this->price->removeElement($price);
            // set the owning side to null (unless already changed)
            if ($price->getProductVariant() === $this) {
                $price->setProductVariant(null);
            }
        }

        return $this;
    }

    /**
     * @Groups({"list"})
     */
    public function getMainPhoto()
    {
        $cr = Criteria::create()
            ->andWhere(Criteria::expr()->eq('main', true));
        /**
         * @var $variant ProductVariantPhoto
         */
        $variant = $this->photos->matching($cr)[0];
        if ($variant)
            return $variant;
        else
            return null;
    }

    public function getMainCount($collection)
    {
        $cr = Criteria::create()
            ->andWhere(Criteria::expr()->eq('main', true));
        return $collection->matching($cr)->count();
    }

    public function getSort(): ?int
    {
        return $this->sort;
    }

    public function setSort(int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * @return Collection|ProductVariantPhoto[]
     * @Groups({"detail"})
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }
//
//    /**
//     * @return Collection|Category[]
//     */
    public function getCategories()//: Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
        }

        return $this;
    }

    public function getInStock(): ?int
    {
        return $this->inStock;
    }

    public function setInStock(int $inStock): self
    {
        $this->inStock = $inStock;

        return $this;
    }

    /**
     * @return Collection|Flag[]
     */
    public function getFlagsObj(): Collection
    {
        return $this->flags;
    }

    /**
     * @return Collection|Flag[]
     * @Groups({"detail"})
     */
    public function getFlags(): Collection
    {
        if ($this->getFlagOverWrite() === true) {
            return $this->getFlagsObj();
        } else {
            return $this->getProduct()->getFlagsObj();
        }
    }

    public function addFlag(Flag $flag): self
    {
        if (!$this->flags->contains($flag)) {
            $this->flags[] = $flag;
        }

        return $this;
    }

    public function removeFlag(Flag $flag): self
    {
        if ($this->flags->contains($flag)) {
            $this->flags->removeElement($flag);
        }

        return $this;
    }

    /**
     * @return Collection|Sign[]
     */
    public function getSignsObj(): Collection
    {
        return $this->signs;
    }

    /**
     * @return Collection|Sign[]
     * @Groups({"detail"})
     */
    public function getSigns(): Collection
    {
        if ($this->getSignOverWrite() === true) {
            return $this->getSignsObj();
        } else {
            return $this->getProduct()->getSignsObj();
        }
    }

    public function addSign(Sign $sign): self
    {
        if (!$this->signs->contains($sign)) {
            $this->signs[] = $sign;
        }

        return $this;
    }

    public function removeSign(Sign $sign): self
    {
        if ($this->signs->contains($sign)) {
            $this->signs->removeElement($sign);
        }

        return $this;
    }


    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getFlagOverWrite(): ?bool
    {
        return $this->flagOverWrite;
    }

    public function setFlagOverWrite(bool $flagOverWrite): self
    {
        $this->flagOverWrite = $flagOverWrite;

        return $this;
    }

    public function getSignOverWrite(): ?bool
    {
        return $this->signOverWrite;
    }

    public function setSignOverWrite(bool $signOverWrite): self
    {
        $this->signOverWrite = $signOverWrite;

        return $this;
    }

    public function getInspirationOverWrite(): ?bool
    {
        return $this->inspirationOverWrite;
    }

    public function setInspirationOverWrite(bool $inspirationOverWrite): self
    {
        $this->inspirationOverWrite = $inspirationOverWrite;

        return $this;
    }

    public function getPhotoOverWrite(): ?bool
    {
        return $this->photoOverWrite;
    }

    public function setPhotoOverWrite(bool $photoOverWrite): self
    {
        $this->photoOverWrite = $photoOverWrite;

        return $this;
    }

    public function getCategoryOverWrite(): ?bool
    {
        return $this->categoryOverWrite;
    }

    public function setCategoryOverWrite(bool $categoryOverWrite): self
    {
        $this->categoryOverWrite = $categoryOverWrite;

        return $this;
    }

    /**
     * @return Collection|ProductVariantParameter[]
     */
    public function getParameters(): Collection
    {
        return $this->parameters;
    }

    public function addParameter(ProductVariantParameter $parameter): self
    {
        if (!$this->parameters->contains($parameter)) {
            $this->parameters[] = $parameter;
            $parameter->setProductVariant($this);
        }

        return $this;
    }

    public function removeParameter(ProductVariantParameter $parameter): self
    {
        if ($this->parameters->contains($parameter)) {
            $this->parameters->removeElement($parameter);
            // set the owning side to null (unless already changed)
            if ($parameter->getProductVariant() === $this) {
                $parameter->setProductVariant(null);
            }
        }

        return $this;
    }
//    /**
//     * @return Collection|File[]
//     */
//    public function getProductVariantInspiration(): Collection
//    {
//        return $this->productVariantInspiration;
//    }
//
//    public function addProductVariantInspiration(File $productInspiration): self
//    {
//        if (!$this->productVariantInspiration->contains($productInspiration)) {
//            $this->productVariantInspiration[] = $productInspiration;
//        }
//
//        return $this;
//    }
//
//    public function removeProductVariantInspiration(File $productInspiration): self
//    {
//        if ($this->productVariantInspiration->contains($productInspiration)) {
//            $this->productVariantInspiration->removeElement($productInspiration);
//        }
//
//        return $this;
//    }

    /**
     * @return Collection|ProductVariantCategory[]
     *
     */
    public function getItemsCategory(): Collection
    {
        $data = new ArrayCollection();
        foreach ($this->itemsCategory as $item) {
            try {
                $item->getCategory()->getDeletedAt();
                $data->add($item);
            } catch (EntityNotFoundException $e) {
            }
//            if (!$item->getCategoryDeletedAt()){
//                $data[] = $item->getCategoryDeletedAt();
//                $data->add($item
//            }
        }
        return $data;
    }

    public function addItemsCategory(ProductVariantCategory $itemsCategory): self
    {
        if (!$this->itemsCategory->contains($itemsCategory)) {
            $this->itemsCategory[] = $itemsCategory;
            $itemsCategory->setProductVariant($this);
        }

        return $this;
    }

    public function removeItemsCategory(ProductVariantCategory $itemsCategory): self
    {
        if ($this->itemsCategory->contains($itemsCategory)) {
            $this->itemsCategory->removeElement($itemsCategory);
//            $itemsCategory->removeProduct($this);
//             set the owning side to null (unless already changed)
            if ($itemsCategory->getProductVariant() === $this) {
                $itemsCategory->setProductVariant(null);
            }
        }
        return $this;
    }


    public function getMainCategory()
    {
        return $this->itemsCategory->filter(function (ProductVariantCategory $c) {
            return $c->getIsMain() === true;
        })->first()->getCategory();
    }

    // kontrala zda uz dana fotka existuje u tohoto produktu
    public function checkPhotoExist($productPhoto)
    {
        foreach ($this->getPhotos() as $photo) {
            if ($photo->getName() === $productPhoto->getName()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function getActualStockStatus(): ?StockStatus
    {
        return $this->actualStockStatus;
    }

    public function setActualStockStatus(?StockStatus $stockStatus): self
    {
        $this->actualStockStatus = $stockStatus;

        return $this;
    }

    public function getOutOfStockStatus(): ?StockStatus
    {
        return $this->outOfStockStatus;
    }

    public function setOutOfStockStatus(?StockStatus $stockStatus): self
    {
        $this->outOfStockStatus = $stockStatus;

        return $this;
    }

    public function calcPercentSale($price, $actualPrice)
    {
        if ($price == 0) {
            $price = 1;
        }
        if ($actualPrice == 0) {
            return 0;
        }
        return round(abs((($actualPrice / $price) -1) *100));
    }

    /**
     * @return string
     */
    public function getEan(): ?string
    {
        return $this->ean;
    }

    /**
     * @param string $ean
     * @return ProductVariant
     */
    public function setEan(string $ean): self
    {
        $this->ean = $ean;
        return $this;
    }

    public function getColorParameterId() {
        if ($this->product->getVariantsDefinedByColor()) {
            /** @var ProductVariantParameter $parameter */
            foreach ($this->parameters as $parameter) {
                if ($parameter->getParameter()->getTranslations()['cs']->getSlug() === ParameterRepository::COLOR_SLUG) {
                    return $parameter->getParameterValue()->getId();
                }
            }
        } else {
            return null;
        }
    }

    public function getSizeParameterId() {
        if ($this->product->getVariantsDefinedBySize()) {
            /** @var ProductVariantParameter $parameter */;
            foreach ($this->parameters as $parameter) {
                if ($parameter->getParameter()->getTranslations()['cs']->getSlug() === ParameterRepository::SIZE_SLUG) {
                    return $parameter->getParameterValue()->getId();
                }
            }
        } else {
            return null;
        }
    }

    public function __clone() {
        if ($this->id) {
            $this->id = null;
            $this->main = false;
            $newPhotos = new ArrayCollection();
            /** @var ProductVariantPhoto $photo */
            foreach ($this->photos as $photo) {
                $photoClone = clone $photo;
                $photoClone->setProductVariant($this);
                $newPhotos->add($photoClone);
            }
            $newParameters = new ArrayCollection();
            /** @var ProductVariantParameter $parameter */
            foreach ($this->parameters as $parameter) {
                $continue = false;
                if ($this->product->getVariantsDefinedBySize() || $this->product->getVariantsDefinedByColor()) {
                    foreach ($parameter->getParameter()->get_Translations() as $translation) {
                        if (
                            ($translation->getSlug() === ParameterRepository::COLOR_SLUG && $this->product->getVariantsDefinedByColor())
                            ||
                            ($translation->getSlug() === ParameterRepository::SIZE_SLUG && $this->product->getVariantsDefinedBySize())
                        ) {
                            $continue = true;
                        }
                    }
                }
                if ($continue) {
                    continue;
                }
                $parameterClone = clone $parameter;
                $parameterClone->setProductVariant($this);
                $newParameters->add($parameterClone);
            }
            $newTranslations = new ArrayCollection();
            /** @var ProductVariantTranslation $variantTranslation */
            foreach ($this->translations as $variantTranslation) {
                $newTranslation = clone $variantTranslation;
                $newTranslation->setProductVariant($this);
                $newTranslation->setSlug($variantTranslation->getSlug().'-1');
                $newTranslations->add($newTranslation);
            }
            $newPrices = new ArrayCollection();
            /** @var Price $item */
            foreach ($this->price as $item) {
                $newPrice = clone $item;
                $newPrice->setProductVariant($this);
                $newPrices->add($newPrice);
            }
            $this->photos = $newPhotos;
            $this->parameters = $newParameters;
            $this->translations = $newTranslations;
            $this->price = $newPrices;
        }
    }

    /**
     * @return bool
     */
    public function getAddProductSlug(): bool
    {
        return $this->addProductSlug;
    }

    /**
     * @param bool $addProductSlug
     */
    public function setAddProductSlug(bool $addProductSlug): self
    {
        $this->addProductSlug = $addProductSlug;

        return $this;
    }

    /**
     * @Groups({"allList", "list"})
     */
    public function getProductId() {
        return $this->product->getId();
    }

    public function getParams(){
        return;
    }
}
