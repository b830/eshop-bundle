<?php

namespace App\Akip\EshopBundle\Entity;


use App\Akip\FileManagerBundle\Entity\File;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\ProductVariantInspirationRepository")
 * @ORM\Table(name="product_variant_inspiration",uniqueConstraints={@ORM\UniqueConstraint(name="pair", columns={"product_variant_id", "file_uuid"})})
 * @Gedmo\Loggable()
 */
class ProductVariantInspiration
{

    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Akip\EshopBundle\Entity\ProductVariant", cascade={"persist"})
     */
    private $productVariant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Akip\FileManagerBundle\Entity\File", cascade={"persist"})
     * @ORM\JoinColumn(name="file_uuid", referencedColumnName="uuid")
     */
    private $file;

    /**
     * @Groups({"list", "detail"})
     * @ORM\Column(name="sort", type="integer", nullable=true)
     * @ORM\Column(type="integer")
     * @Gedmo\Versioned()
     */
    private $sort = 0;

    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getProductVariantId()
    {
        return $this->productVariant->getId();
    }

    /**
     * @return mixed
     */
    public function getFileUuid()
    {
        return $this->file->getUuid();
    }


    /**
     * @return Collection|ProductVariant[]
     */
    public function getProductVariant()//: Collection
    {
        return $this->productVariant;
    }

    /**
     * @return File
     * @Groups({"list"})
     */
    public function getFile(): File
    {
        return $this->file;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    public function setSort(int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    public function setProductVariant(?ProductVariant $productVariant): self
    {
        $this->productVariant = $productVariant;

        return $this;
    }

    public function setFile(?File $file): self
    {
        $this->file = $file;

        return $this;
    }


//    public function categoryWithIsMain()
//    {
//        return $this->category;
//    }
}
