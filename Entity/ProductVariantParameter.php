<?php

namespace App\Akip\EshopBundle\Entity;

use App\Akip\EshopBundle\Repository\ParameterRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProductVariantParameter
 *
 * @ORM\Table(name="product_variant_parameter", indexes={@ORM\Index(name="product_parameter_ibfk_3", columns={"parameter_value_id"}), @ORM\Index(name="product_id", columns={"product_variant_id"}), @ORM\Index(name="parameter_id", columns={"parameter_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\ProductVariantParameterRepository")
 * @Gedmo\Loggable()
 */
class ProductVariantParameter
{

    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var string|null
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=true, options={"default"="NULL"})
     * @Gedmo\Versioned()
     */
    private $value = '';

    /**
     * @var Parameter
     *
     * @ORM\ManyToOne(targetEntity="Parameter")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parameter_id", referencedColumnName="id")
     * })
     */
    private $parameter;

    /**
     * @var ParameterValue
     *
     * @ORM\ManyToOne(targetEntity="ParameterValue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parameter_value_id", referencedColumnName="id")
     * })
     */
    private $parameterValue;

    /**
     * @var ProductVariant
     *
     * @ORM\ManyToOne(targetEntity="ProductVariant")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_variant_id", referencedColumnName="id")
     * })
     */
    private $productVariant;

    /**
     * @var integer
     * @ORM\Column(name="sort", type="integer", nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $sort = 0;

    /**
     * @return int|null
     * @Groups({"list"})
     */
    public function getParameterId(): ?int
    {
        return $this->getParameter()->getId();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @Groups({"list", "detail"})
     */
    public function getName(): string
    {
        return $this->parameter->getName();
    }

    /**
     * @return string|null
     * @Groups({"list"})
     */
    public function getType()
    {
        return $this->parameter->getType();
    }
    /**
     * @Groups({"list"})
     */
    public function getValues()
    {
        if (!empty($this->value) or $this->value !== '' or $this->value != null) {
            switch ($this->getType()) {
                case Parameter::TYPE_INT:
                    return (int)$this->value;
                    break;
                case Parameter::TYPE_FLOAT:
                    return (float)$this->value;
                    break;
                case Parameter::TYPE_BOOLEAN:
                    return $this->value === 'true' ? true : false;
                    break;
                case Parameter::TYPE_STRING:
                    return $this->value;
                    break;
            }
        } elseif ($this->parameterValue !== null) {
            try {
                return ['id' => $this->parameterValue->getId(), 'name' => $this->parameterValue->getName()];
            } catch (\Exception $e){

            }
        }
    }
    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue($value): self
    {
        switch ($this->getType()) {
            case Parameter::TYPE_FLOAT:
            case Parameter::TYPE_INT:
                if (!is_numeric($value))
                    throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, "Value - [{$value}] is not acceptable for type [{$this->getType()}]. Only numbers are acceptable.");
                else
                    $this->value = $value;
                break;
            case Parameter::TYPE_BOOLEAN:
                if (!is_bool($value))
                    throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, "Value - [{$value}] is not acceptable for type [{$this->getType()}]. Values can be only 'true' or 'false'.");
                else
                    $this->value = $value == true ? 'true' : 'false';
                break;
            case Parameter::TYPE_STRING:
                $this->value = $value;
                break;
            case Parameter::TYPE_CHOICE:
            case Parameter::TYPE_MULTI_CHOICE:
            case Parameter::TYPE_COLOR:
                $this->value = '';
        }
        return $this;
    }

    /**
     * @Groups({"list"})
     */
    public function getTranslations()
    {
        $items = array();
        foreach ($this->getParameter()->get_Translations() as $translation) {
            $items[$translation->getLocale()] = $translation;
        }
        return $items;
    }

    public function getParameter(): ?Parameter
    {
        return $this->parameter;
    }

    public function setParameter(?Parameter $parameter): self
    {
        $this->parameter = $parameter;

        return $this;
    }

    public function getParameterValue(): ?ParameterValue
    {
        return $this->parameterValue;
    }

    public function setParameterValue(?ParameterValue $parameterValue): self
    {
        if (in_array($this->getType(), [Parameter::TYPE_MULTI_CHOICE, Parameter::TYPE_CHOICE, Parameter::TYPE_COLOR]))
            $this->parameterValue = $parameterValue;
        else
            $this->parameterValue = null;
        return $this;
    }

    public function getProductVariant(): ?ProductVariant
    {
        return $this->productVariant;
    }

    public function setProductVariant(?ProductVariant $productVariant): self
    {
        $this->productVariant = $productVariant;

        return $this;
    }
    /**
     * @Groups({"list"})
     */
    public function getValuesList()
    {
        return $this->parameter->getValuesList();
    }

    public function getSort(): ?int
    {
        return $this->sort;
    }

    public function setSort(int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    public function load(Parameter $parameter, ProductVariant $variant, $sort, ParameterValue $parameterValue = null, $value = ''){
        $this->setParameter($parameter);
        $this->setProductVariant($variant);
        $this->setSort($sort);
        if (is_array($parameterValue)){
            $this->setParameterValue($parameterValue[0]);
        } else {
            $this->setParameterValue($parameterValue);
        }

        if (is_array($value)) {
            $this->setValue($value[0]);
        } else {
            $this->setValue($value);
        }
    }

    /**
     * @Groups({"list", "detail"})
     */
    public function getRequired() {
        $slug = $this->getParameter()->getTranslations()['cs']->getSlug();
        /** @var Product $product */
        $product = $this->getProductVariant()->getProduct();
        $required = false;
        if (($slug === ParameterRepository::COLOR_SLUG && $product->getVariantsDefinedByColor()) || ($slug === ParameterRepository::SIZE_SLUG && $product->getVariantsDefinedBySize())) {
            $required = true;
        }
        return $required;
    }

    public function __clone()
    {
        if ($this->id) {
            $this->id = null;
        }
    }
}
