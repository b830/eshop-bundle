<?php

namespace App\Akip\EshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProductVariantPhotoTranslation
 *
 * @ORM\Table(name="product_variant_photo_translation", indexes={@ORM\Index(name="product_variant_photo_id", columns={"product_variant_photo_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\ProductVariantPhotoTranslationRepository")
 * @Gedmo\Loggable()
 */
class ProductVariantPhotoTranslation
{
    use TimestampableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="description", type="text", length=0, nullable=false)
     * @Gedmo\Versioned()
     * @Groups({"list","detail"})
     */
    private $description = '';

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="locale", type="string", length=10, nullable=false)
     * @Gedmo\Versioned()
     * @Groups({"list","detail"})
     */
    private $locale;

    /**
     * @var string
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     * @Gedmo\Versioned()
     */
    private $slug = '';

    /**
     * @var ProductVariantPhoto
     *
     * @ORM\ManyToOne(targetEntity="ProductVariantPhoto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_variant_photo_id", referencedColumnName="id")
     * })
     */
    private $productVariantPhoto;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        if ($slug === '' || !$slug){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Slug ');
        }
        $this->slug = $slug;

        return $this;
    }

    public function getProductVariantPhoto(): ?ProductVariantPhoto
    {
        return $this->productVariantPhoto;
    }

    public function setProductVariantPhoto(?ProductVariantPhoto $productVariantPhoto): self
    {
        $this->productVariantPhoto = $productVariantPhoto;

        return $this;
    }

    public function load($data, $locale, ProductVariantPhoto $productPhoto)
    {
        $this->setLocale($locale);
        $this->setDescription($data['description']);
        $this->setProductVariantPhoto($productPhoto);
        if (isset($data['slug'])){
            $this->setSlug($data['slug']);
        }
    }

}
