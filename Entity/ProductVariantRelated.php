<?php

namespace App\Akip\EshopBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\ProductVariantRelatedRepository")
 * @ORM\Table(name="product_variant_related", uniqueConstraints={@ORM\UniqueConstraint(name="pair", columns={"product_variant_id", "related_product_variant_id"})})
 * @Gedmo\Loggable()
 */
class ProductVariantRelated
{

    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Akip\EshopBundle\Entity\ProductVariant", cascade={"persist"})
     */
    private $productVariant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Akip\EshopBundle\Entity\ProductVariant", cascade={"persist"})
     * @ORM\JoinColumn(name="related_product_variant_id", referencedColumnName="id")
     */
    private $relatedProductVariant;

    /**
     * @Groups({"list", "detail"})
     * @ORM\Column(name="sort", type="integer", nullable=true)
     * @ORM\Column(type="integer")
     * @Gedmo\Versioned()
     */
    private $sort = 0;

    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getProductVariantId()
    {
        return $this->productVariant->getId();
    }

    /**
     * @return mixed
     */
    public function getRelatedId()
    {
        return $this->relatedProductVariant->getId();
    }


    /**
     * @return Collection|ProductVariant[]
     */
    public function getProductVariant()//: Collection
    {
        return $this->productVariant;
    }

    /**
     * @return ProductVariant
     * @Groups({"list"})
     */
    public function getRelated(): ProductVariant
    {
        return $this->relatedProductVariant;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    public function setSort(int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    public function setProductVariant(?ProductVariant $variant): self
    {
        $this->productVariant = $variant;

        return $this;
    }

    public function setRelated(?ProductVariant $related): self
    {
        $this->relatedProductVariant = $related;

        return $this;
    }


//    public function categoryWithIsMain()
//    {
//        return $this->category;
//    }
}
