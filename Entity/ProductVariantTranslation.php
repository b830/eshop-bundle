<?php

namespace App\Akip\EshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProductVariantTranslation
 *
 * @ORM\Table(name="product_variant_translation", uniqueConstraints={@ORM\UniqueConstraint(name="slug", columns={"slug", "deleted_at"})}, indexes={@ORM\Index(name="product_variant_id", columns={"product_variant_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\ProductVariantTranslationRepository")
 * @UniqueEntity("slug")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=false)
 * @Gedmo\Loggable()
 */
class ProductVariantTranslation
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"list", "detail"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Groups({"list", "detail"})
     * @Gedmo\Versioned()
     */
    private $name = '';

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="locale", type="string", length=10, nullable=false)
     * @Groups({"list", "detail"})
     * @Gedmo\Versioned()
     */
    private $locale;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Groups({"list"})
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     * @Groups({"list", "detail"})
     * @Gedmo\Versioned()
     */
    private $slug = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="short_description", type="text", length=0, nullable=true, options={"default"="NULL"})
     * @Groups({"list", "detail"})
     * @Gedmo\Versioned()
     */
    private $shortDescription = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true, options={"default"="NULL"})
     * @Groups({"list", "detail"})
     * @Gedmo\Versioned()
     */
    private $description = '';

    /**
     * @var ProductVariant
     *
     * @ORM\ManyToOne(targetEntity="ProductVariant")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_variant_id", referencedColumnName="id")
     * })
     */
    private $productVariant;

    /**
     * @var bool
     *
     * @ORM\Column(name="short_description_over_write", type="boolean", nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $shortDescriptionOverWrite = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="description_over_write", type="boolean", nullable=false)
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $descriptionOverWrite = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getProductVariant(): ?ProductVariant
    {
        return $this->productVariant;
    }

    public function setProductVariant(?ProductVariant $productVariant): self
    {
        $this->productVariant = $productVariant;

        return $this;
    }

    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    public function setShortDescription(?string $shortDescription): self
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function load($key, $data)
    {
        $this->setLocale($key);
        $this->setName($data['name']);
        $this->setSlug($data['slug']);

        if (isset($data['descriptionOverWrite']))
            $this->setDescriptionOverWrite($data['descriptionOverWrite']);

        if (isset($data['shortDescriptionOverWrite']))
            $this->setShortDescriptionOverWrite($data['shortDescriptionOverWrite']);

        if ($this->shortDescriptionOverWrite === true)
            $this->setShortDescription($data['shortDescription']);
        else
            $this->setShortDescription('');

        if ($this->descriptionOverWrite === true)
            $this->setDescription($data['description']);
        else
            $this->setDescription('');

        return $this;
    }

    public function build()
    {
        return [$this->locale => $this];
    }

    public function getShortDescriptionOverWrite(): ?bool
    {
        return $this->shortDescriptionOverWrite;
    }

    public function setShortDescriptionOverWrite(bool $shortDescriptionOverWrite): self
    {
        $this->shortDescriptionOverWrite = $shortDescriptionOverWrite;

        return $this;
    }

    public function getDescriptionOverWrite(): ?bool
    {
        return $this->descriptionOverWrite;
    }

    public function setDescriptionOverWrite(bool $descriptionOverWrite): self
    {
        $this->descriptionOverWrite = $descriptionOverWrite;

        return $this;
    }

    public function __clone()
    {
        if ($this->id) {
            $this->id = null;
        }
    }

}
