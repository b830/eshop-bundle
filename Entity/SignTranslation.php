<?php

namespace App\Akip\EshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SignTranslation
 *
 * @ORM\Table(name="sign_translation", uniqueConstraints={@ORM\UniqueConstraint(name="text", columns={"text"})}, indexes={@ORM\Index(name="fk_sign_id", columns={"sign_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\SignTranslationRepository")
 */
class SignTranslation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=255, nullable=false)
     * @Groups({"detail", "list"})
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=10, nullable=false)
     */
    private $locale;


    /**
     * @var Sign
     *
     * @ORM\ManyToOne(targetEntity="App\Akip\EshopBundle\Entity\Sign")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sign_id", referencedColumnName="id")
     * })
     */
    private $sign;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getSign(): ?Sign
    {
        return $this->sign;
    }

    public function setSign(?Sign $sign): self
    {
        $this->sign = $sign;

        return $this;
    }


}
