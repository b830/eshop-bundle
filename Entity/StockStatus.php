<?php

namespace App\Akip\EshopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * StockStatus
 *
 * @ORM\Table(name="stock_status", uniqueConstraints={@ORM\UniqueConstraint(name="slug", columns={"slug", "deleted_at"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\StockStatusRepository")
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 * @UniqueEntity("slug")
 */
class StockStatus
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    const IN_STOCK = 'in-stock';
    const SOLD_OUT = 'sold-out';
    const ON_REQUEST = 'on-request';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Groups({"detail", "list"})
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     * @Groups({"detail" , "list"})
     * @Gedmo\Versioned()
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\StockStatusTranslation", mappedBy="stockStatus", orphanRemoval=true)
     */
    private $translations;

    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Column(name="heureka_delivery_date", type="integer", nullable=false)
     * @Groups({"detail", "list"})
     */
    private $heurekaDeliveryDate;

    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Column(name="zbozi_delivery_date", type="integer", nullable=false)
     * @Groups({"detail", "list"})
     */
    private $zboziDeliveryDate;

    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Column(name="google_delivery_date", type="integer", nullable=false)
     * @Groups({"detail", "list"})
     */
    private $googleDeliveryDate;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        if ($slug === '' || !$slug) {
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Slug ');
        }

        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|StockStatusTranslation[]
     */
    public function getTranslationsObj(): Collection
    {
        return $this->translations;
    }

    /**
     * @return array
     * @Groups({"list"})
     */
    public function getTranslations(): array
    {
        $items = array();
        foreach ($this->translations as $translation){
            $items[$translation->getLocale()] = $translation;
        }
        return $items;
    }


    public function addTranslation(StockStatusTranslation $translation): self
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setStockStatus($this);
        }

        return $this;
    }

    public function removeTranslation(StockStatusTranslation $translation): self
    {
        if ($this->translations->contains($translation)) {
            $this->translations->removeElement($translation);
            // set the owning side to null (unless already changed)
            if ($translation->getStockStatus() === $this) {
                $translation->setStockStatus(null);
            }
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getHeurekaDeliveryDate(): int
    {
        return $this->heurekaDeliveryDate;
    }

    /**
     * @param int $heurekaDeliveryDate
     */
    public function setHeurekaDeliveryDate(int $heurekaDeliveryDate): self
    {
        $this->heurekaDeliveryDate = $heurekaDeliveryDate;
        return $this;
    }

    /**
     * @return int
     */
    public function getZboziDeliveryDate(): int
    {
        return $this->zboziDeliveryDate;
    }

    /**
     * @param int $zboziDeliveryDate
     */
    public function setZboziDeliveryDate(int $zboziDeliveryDate): self
    {
        $this->zboziDeliveryDate = $zboziDeliveryDate;
        return $this;
    }

    /**
     * @return int
     */
    public function getGoogleDeliveryDate(): int
    {
        return $this->googleDeliveryDate;
    }

    /**
     * @param int $googleDeliveryDate
     */
    public function setGoogleDeliveryDate(int $googleDeliveryDate): self
    {
        $this->googleDeliveryDate = $googleDeliveryDate;
        return $this;
    }
}
