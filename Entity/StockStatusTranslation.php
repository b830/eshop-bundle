<?php

namespace App\Akip\EshopBundle\Entity;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BrandTranslation
 *
 * @ORM\Table(name="stock_status_translation", indexes={@ORM\Index(name="stock_status_id", columns={"stock_status_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\StockStatusTranslationRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 * @Gedmo\Loggable()
 */
class StockStatusTranslation
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private $id;


    /**
     * @var string
     * @Groups({"detail", "list"})
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Gedmo\Versioned()
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="locale", type="string", length=10, nullable=false)
     * @Gedmo\Versioned()
     */
    private $locale;

    /**
     * @var StockStatus
     *
     * @ORM\ManyToOne(targetEntity="stockStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="stock_status_id", referencedColumnName="id")
     * })
     */
    private $stockStatus;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStockStatus(): ?StockStatus
    {
        return $this->stockStatus;
    }

    public function setStockStatus(?StockStatus $stockStatus): self
    {
        $this->stockStatus = $stockStatus;

        return $this;
    }

}
