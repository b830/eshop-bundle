<?php

namespace App\Akip\EshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Vat
 *
 * @ORM\Table(name="vat")
 * @ORM\Entity(repositoryClass="App\Akip\EshopBundle\Repository\VatRepository")
 */
class Vat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"list", "detail"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="label", type="string", length=255, nullable=false)
     * @Groups({"list", "detail"})
     */
    private $label;

    /**
     * @var float
     * @Assert\NotBlank()
     * @ORM\Column(name="rate", type="float", precision=10, scale=0, nullable=false)
     * @Groups({"list", "detail"})
     */
    private $rate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getRate(): ?float
    {
        return $this->rate;
    }

    public function setRate(float $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function load($data){
        $this->label = $data['label'];
        $this->rate = $data['rate'];
    }

}
