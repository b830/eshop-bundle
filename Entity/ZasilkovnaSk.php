<?php


namespace App\Akip\EshopBundle\Entity;


class ZasilkovnaSk extends BaseDelivery
{
    public function __construct()
    {
        $this->heurekaSlug = 'ZASILKOVNA';
        $this->zboziSlug = 'ZASILKOVNA';
        $this->options = [
            'api-key' => [
                'value' => '',
                'label' => 'Api klíč'
            ]
        ];
        $this->isAddressRequired = false;
    }

    public function getButton($options, $data = []): string
    {
        $this->options = $options;
        $name = isset($data['nameStreet']) ? $data['nameStreet'] : 'Vyberte si pobočku';
        return "
                <script src=\"https://widget.packeta.com/www/js/library.js\"></script>
                <script>
                    var packetaApiKey = '{$this->options['api-key']['value']}';
                    function showSelectedPickupPointSk(point)
                    {
                        var idElement = document.getElementById('deliveryInfo');
                        var radioElement = document.getElementById('zasilkovna-sk');
                        var placeSelected = document.getElementById('placeSelected');
                        var zasilkovnaLink = document.getElementById('zasilkovna-choose-sk');
                        if (point) {
                            radioElement.checked = true;
                            idElement.value = JSON.stringify(point);
                            zasilkovnaLink.innerHTML = point.nameStreet;
                        } else {
                            radioElement.checked = false;
                        }
                    };
                </script>
                <a onclick=\"Packeta.Widget.pick(packetaApiKey, showSelectedPickupPointSk, {'language': 'sk'})\" id='zasilkovna-choose-sk' class='spec'>{$name}</a>
        ";
    }

}
