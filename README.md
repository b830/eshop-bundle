# Bundle obsahující REST api pro ESHOP

## Obsah:
* Parametry s různými typy možností (choice, multi-choice, color, float, int, string)
* Různé typy dopravy (s možností napojení, např.: zásilkovna) a také platby (pokud se jedná o platbu převodem - generování QR kódu)
    * včetně vytváření kombinací plateb a doprav
    * lze definovat různé ceny dopravy (závísí na ceně objednávky)
* Produkty s N variantami
    * produkt může mít N fotek, doplňků, inspirací, příznaků... (to samé i variaty)
        * parametry, fotky... se defaultně dědí do variant
* Měny
* Logování online plateb
* Kategorie
    * kategorii lze definovat parametry, které se automaticky přenesou do produktu, který byl v této kategorii vytvořen
* Ceny
    * jsou vázány na varianty
* Značky

