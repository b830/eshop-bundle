<?php


namespace App\Akip\EshopBundle\Repository;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\CompositeExpression;
use phpDocumentor\Reflection\Type;

abstract class BaseEntityRepository extends ServiceEntityRepository
{

    public static abstract function searchedColumns(): array;

    public function getSearchCriteria($value): CompositeExpression
    {
        $exp = new CompositeExpression(CompositeExpression::TYPE_OR,
            array_map(function ($field) use ($value) {
                if (is_int($value))
                    return Criteria::expr()->eq($field, $value);
                else
                    return Criteria::expr()->contains($field, $value);
            }, $this::searchedColumns())
        );

        return $exp;
    }


}
