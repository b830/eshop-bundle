<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\BrandTranslation;
use App\Akip\EshopBundle\Entity\Brand;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Mapping\ClassMetadata;

/**
 * @method BrandTranslation|null find($id, $lockMode = null, $lockVersion = null)
 * @method BrandTranslation|null findOneBy(array $criteria, array $orderBy = null)
 * @method BrandTranslation[]    findAll()
 * @method BrandTranslation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BrandTranslationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BrandTranslation::class);
    }

    /**
     * @param string $slug
     * @return BrandTranslation|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findTranslationBySlug(string $slug, string $locale)
    {
        return $this->createQueryBuilder('brandTranslation')
            ->leftJoin('brandTranslation.brand', 'brand')
            ->where('brand.slug = :slug')
            ->andWhere('brandTranslation.locale = :locale')
            ->setParameters(['slug' => $slug, 'locale' => $locale])
            ->getQuery()
            ->setFetchMode(BrandTranslation::class, 'brand', ClassMetadata::FETCH_EAGER)
            ->getOneOrNullResult();
    }

    // /**
    //  * @return BrandTranslation[] Returns an array of FlagTranslation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FlagTranslation
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
