<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\Category;
use App\Akip\EshopBundle\Entity\CategoryTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\DocBlock\Tags\Author;
use phpDocumentor\Reflection\Types\This;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends BaseEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    /**
     * @param $slug
     * @return Category|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findBySlug($slug)
    {
        return $this->createQueryBuilder('c')
            ->join('c.translations', 't')
            ->where('t.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findMain()
    {
        return $this->createQueryBuilder('c')
            ->select(['c', 't', 'ic'])
            ->leftJoin('c.translations', 't')
            ->leftJoin('c.icon', 'ic')
            ->where('c.parent is null')
            ->andWhere('c.enabled = true')
            ->orderBy('c.sort', 'ASC')
            ->getQuery()
            ->getArrayResult();
    }

    public function getTranslationsByLocale($locale)
    {
        return $this->createQueryBuilder('c')
            ->innerJoin('c.translations', 't')
            ->where('c.parent is NULL')
            ->andWhere('t.locale = :locale')
            ->setParameter('locale', $locale)
            ->orderBy('c.sort', 'ASC')
            ->getQuery()
            ->getResult();
//        >createQuery(
//        'SELECT c, t FROM App\Akip\EshopBundle\Entity\Category c
//            LEFT JOIN c.translations t
//            WHERE c.main = null and t.locale = :l'
//    )->setParameter('l', $locale);
    }

    /**
     * @param Category $category
     * @return ArrayCollection
     */
    public function findChildCategories(Category $category = null)
    {
        $qb = $this->createQueryBuilder('c');

        if ($category) {
            $categories = $qb
                ->select(['c', 'translations'])
                ->leftJoin('c.translations', 'translations')
                ->where('c.lft > :lft')
                ->andWhere('c.rgt < :rgt')
                ->andWhere('c.depth = :depth + 1')
                ->andWhere('c.parent = :parent')
                ->setParameters([
                    'lft' => $category->getLft(),
                    'rgt' => $category->getRgt(),
                    'depth' => $category->getDepth(),
                    'parent' => $category,
                ]);
        } else {
            $categories = $qb->where('c.depth = 1');
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $slug
     * @return Category|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findBySlugAndDepth($slug, $depth)
    {
        return $this->createQueryBuilder('c')
            ->select(['c', 'categories'])
            ->join('c.translations', 'translations')
            ->join('c.categoryItems', 'categories')
            ->where('translations.slug = :slug')
            ->andWhere('categories.depth = :depth')
            ->andWhere('categories.parent = c')
            ->setParameters([
                'slug' => $slug,
                'depth' => $depth,
                ])
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param $id
     * @param $locale
     * @return Category|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getByIdAndLocale($id, $locale)
    {
        return $this->createQueryBuilder('category')
            ->select(['category', 'categories','translations', 'icon'])
            ->leftJoin('category.translations', 'translations')
            ->leftJoin('category.categoryItems', 'categories')
            ->leftJoin('category.icon', 'icon')
            ->andWhere('category.id = :id')
            ->andWhere('translations.locale = :locale')
            ->setParameters([
                'id' => $id,
                'locale' => $locale,
                ])
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Metodu je potreba zavolat po kazde uprave menu (pridani/uprava/smazani polozky, zmena razeni, zmena rodice apod.)
     * @see https://php.vrana.cz/traverzovani-kolem-stromu-prakticky.php
     * @see https://www.interval.cz/clanky/metody-ukladani-stromovych-dat-v-relacnich-databazich/
     *
     * @param $mainCategoryId
     * @param null $parentId
     * @param int $left
     * @param int $depth
     * @return int|mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function rebuildTree($mainCategoryId, $parentId = null, $left = 0, $depth = 0)
    {
        $right = $left + 1;

        $items = $this->findBy(['main' => $mainCategoryId, 'parent' => $parentId]);

        foreach ($items as $item) {
            $right = $this->rebuildTree($mainCategoryId, $item->getId(), $right, $depth + 1);
        }

        if ($parentId) {
            $item = $this->find($parentId);
            $item->setLft($left);
            $item->setRgt($right);
            $item->setDepth($depth);

            $em = $this->getEntityManager();
            $em->persist($item);
            $em->flush();
        }

        return $right + 1;
    }
    public function getMaxDpth(Category $main){
        foreach($this->findBy(['main' => $main]) as $item){
            $depth_arr[] = $item->getDepth();
            $maxDpth = max($depth_arr);
        }
        /** @var int $maxDpth */
        if (!$maxDpth)
            return 1;
        return $maxDpth;
    }
    public function build(Category $main, $maxDpth, $data = null, $depth = 1)
    {
        if ($data === null) {
            if ($main->getParent() === null)
                $parent = null;
            else
                $parent = $main->getParent()->getId();
            $data = $this->setFields($main, $parent);
        }
        $items = $this->findBy(['parent' => $data['id']], ['sort' => 'ASC']);
        foreach ($items as $item) {
            $ar = $this->setFields($item, $item->getParent()->getId());
            array_push($data['children'], $ar);
        }
        if ($depth <= $maxDpth) {
            for ($i = 0; $i < count($data['children']); $i++) {
                if($data['children'][$i]['parent'] === $data['id']) {
                    $data['children'][$i] = $this->build($main, $maxDpth, $data['children'][$i], $depth + 1);
                } else
                    $data['children'] = [];
            }
            return $data;
        }
    }
    public function setFields(Category $item, $parent = null)
    {
        return [
            'id' => $item->getId(),
            'name' => $item->getName(),
            'depth' => $item->getDepth(),
            'parent' => $parent,
            'translations' => $item->translations(),
            'parameters' => $item->getParameter(),
            'sort' => $item->getSort(),
            'icon' => $item->getIcon() ? $item->getIcon()->getUrl() : '',
            'uuid' => $item->getIcon() ? $item->getIcon()->getUuid() : '',
            'children' => [],
            'heurekaCategory' => $item->getHeurekaCategory(),
            'googleCategory' => $item->getGoogleCategory(),
            'zboziCategory' => $item->getZboziCategory()
        ];
    }
    // /**
    //  * @return Category[] Returns an array of Category objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Category
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public static function searchedColumns(): array
    {
        return [
            'id',
            'name'
        ];
    }
}
