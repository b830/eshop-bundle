<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\Category;
use App\Akip\EshopBundle\Entity\CategoryTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CategoryTranslation|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategoryTranslation|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategoryTranslation[]    findAll()
 * @method CategoryTranslation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryTranslationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategoryTranslation::class);
    }

    /**
     * @param Category $category
     * @param string $locale
     * @return CategoryTranslation|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findTranslation(Category $category, string $locale)
    {
        return $this->createQueryBuilder('ct')
                ->where('ct.category = :category')
                ->andWhere('ct.locale = :locale')
                ->setParameters([
                    'category' => $category,
                    'locale' => $locale
                ])
                ->getQuery()
                ->getOneOrNullResult();
    }

    /**
     * @param string $slug
     * @return CategoryTranslation|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findTranslationBySlug(string $slug)
    {
        return $this->createQueryBuilder('ct')
                ->andWhere('ct.slug = :slug')
                ->setParameter('slug', $slug)
                ->getQuery()
                ->setFetchMode(CategoryTranslation::class, 'category', ClassMetadata::FETCH_EAGER)
                ->getOneOrNullResult();
    }

    // /**
    //  * @return CategoryTranslation[] Returns an array of CategoryTranslation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CategoryTranslation
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
