<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\CmsBundle\Repository\BaseEntityRepository;
use App\Akip\EshopBundle\Entity\DeliveryPrice;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DeliveryPrice|null find($id, $lockMode = null, $lockVersion = null)
 * @method DeliveryPrice|null findOneBy(array $criteria, array $orderBy = null)
 * @method DeliveryPrice[]    findAll()
 * @method DeliveryPrice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DeliveryPriceRepository extends BaseEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DeliveryPrice::class);
    }

    // /**
    //  * @return DeliveryPrice[] Returns an array of DeliveryPrice objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DeliveryPrice
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public static function searchedColumns(): array
    {
        return [
            'id',
        ];
    }
}

