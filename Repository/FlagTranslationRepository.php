<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\FlagTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FlagTranslation|null find($id, $lockMode = null, $lockVersion = null)
 * @method FlagTranslation|null findOneBy(array $criteria, array $orderBy = null)
 * @method FlagTranslation[]    findAll()
 * @method FlagTranslation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FlagTranslationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FlagTranslation::class);
    }

    // /**
    //  * @return FlagTranslation[] Returns an array of FlagTranslation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FlagTranslation
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
