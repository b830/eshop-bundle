<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\OrderPaymentLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrderPaymentLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderPaymentLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderPaymentLog[]    findAll()
 * @method OrderPaymentLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderPaymentLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderPaymentLog::class);
    }

    // /**
    //  * @return OrderPaymentLog[] Returns an array of OrderPaymentLog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrderPaymentLog
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
