<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\Order;
use App\Akip\EshopBundle\Entity\OrderPaymentLog;
use App\Akip\EshopBundle\Entity\OrderStatus;
use App\Akip\MailerBundle\Services\Mailer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Heureka\ShopCertification;
use Heureka\ShopCertificationTest;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends BaseEntityRepository
{
    private $mailer;

    public function __construct(ManagerRegistry $registry, Mailer $mailer)
    {
        parent::__construct($registry, Order::class);
        $this->mailer = $mailer;
    }

    // /**
    //  * @return order[] Returns an array of Order objects
    //  */

    public function findTodayDemans()
    {
        $orders = $this->findAll();
        $data = [];
        foreach ($orders as $order) {
            if ($order->getCreatedAt()->format("Y-m-d") === date("Y-m-d")) {
                $data[] = $order;
            }
        }
        return $data;
    }

    public function updateOrderStatus(Order $order, OrderStatus $newStatus)
    {
        $oldStatus = $order->getOrderStatus();
        $order->setOrderStatus($newStatus);
        if ($oldStatus !== null && $oldStatus->getSlug() !== $newStatus->getSlug()) {
            $this->mailer->sendMail(Mailer::TEMPLATE_ORDER_STATUS_UPDATE, $order->getCustomerEmail(), 'Změna stavu objednávky č.: ' . $order->getOrderNumber(), ['order' => $order]);
        }
        $this->_em->flush();
    }

    public function updateOrderPaymentDate(Order &$order, $paymentDate)
    {
        $order->setPaymentDate($paymentDate);
        if ($order->getPayment()->isSendEmail() && $paymentDate !== null) {
            $paymentNumber = $order->getNextPaymentNumber();
            foreach ($order->getPaymentLogs() as $log) {
                if ($log->getStatus() === OrderPaymentLog::PAID) {
                    $paymentNumber = $log->getPaymentNumber();
                }
            }
            $this->mailer->sendMail(Mailer::TEMPLATE_ORDER_PAYMENT_CONFIRM, $order->getCustomerEmail(), 'Objednávka č.: ' . $order->getOrderNumber() . ' byla úspěšně zaplacena', ['order' => $order, 'fullPrice' => $order->getFullPrice(), 'paymentNumber' => $paymentNumber]);
        }
        $this->_em->flush();
    }

    /*
    public function findOneBySomeField($value): ?order
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public static function searchedColumns(): array
    {
        return [
            'id',
            'contactName',
            'contactSurname',
            'contactPhone',
            'contactEmail',
            'companyName',
            'cin',
        ];
    }
}
