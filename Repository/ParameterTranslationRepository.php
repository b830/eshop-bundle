<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\ParameterTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ParameterTranslation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ParameterTranslation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ParameterTranslation[]    findAll()
 * @method ParameterTranslation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParameterTranslationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ParameterTranslation::class);
    }

    // /**
    //  * @return ParameterTranslation[] Returns an array of ParameterTranslation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ParameterTranslation
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
