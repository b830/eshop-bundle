<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\ProductAccessory;
use App\Akip\EshopBundle\Entity\ProductInspiration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductAccessory|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductAccessory|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductAccessory[]    findAll()
 * @method ProductAccessory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductAccessoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductAccessory::class);
    }

    public function build($product)
    {
        $data = $this->findBy(['product' => $product], ['sort' => 'ASC']);
        $accessory = [];
        foreach ($data as $item) {
            $accessory[] = [
                'id' => $item->getAccessoryId(),
                "name" => $item->getAccessory()->getName(),
                "sort" => $item->getSort(),
                "display" => $item->getAccessory()->getDisplay(),
                'url' => $item->getAccessory()->getMainPhoto() ? $item->getAccessory()->getMainPhoto()->getPhoto()->getChildren()[1]->getUrl() : 'https://i.redd.it/s8lk86v3r2m11.png',
            ];
        }
        return $accessory;
    }

    public function findByProduct(Product $product) {
        return $this->createQueryBuilder('accessory')
            ->select(['accessory', 'product', 'productTranslations'])
            ->leftJoin('accessory.product', 'product')
            ->leftJoin('accessory.accessory', '_product')
            ->leftJoin('product.translations', 'productTranslations')
            ->where('accessory.product = :product')
            ->andWhere('_product.enabled = true')
            ->setParameter('product', $product)
            ->getQuery()->execute();
    }
    // /**
    //  * @return ProductInspiration[] Returns an array of ProductInspiration objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductInspiration
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
