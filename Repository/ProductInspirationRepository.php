<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\ProductInspiration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductInspiration|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductInspiration|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductInspiration[]    findAll()
 * @method ProductInspiration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductInspirationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductInspiration::class);
    }

    public function build($product)
    {
        $data = $this->findBy(['product' => $product], ['sort' => 'ASC']);
        $inspiration = [];
        foreach ($data as $item) {
            $inspiration[] = [
                'id' => $item->getId(),
                'uuid' => $item->getFileUuid(),
                "originalName" => $item->getFile()->getOriginalName(),
                "originalSuffix" => $item->getFile()->getSuffix(),
                "name" => $item->getFile()->getName(),
                "type" => $item->getFile()->getType(),
                "size" => $item->getFile()->getSize(),
                "suffix" => $item->getFile()->getSuffix(),
                "children" => $item->getFile()->getChildren(),
                'url' => $item->getFile()->getUrl(),
                "sort" => $item->getSort(),
                'translations' => $item->getTranslations(),
            ];
        }
        return $inspiration;
    }
    // /**
    //  * @return ProductInspiration[] Returns an array of ProductInspiration objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductInspiration
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
