<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\ProductInspirationTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductInspirationTranslation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductInspirationTranslation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductInspirationTranslation[]    findAll()
 * @method ProductInspirationTranslation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductInspirationTranslationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductInspirationTranslation::class);
    }

    // /**
    //  * @return ProductInspirationTranslation[] Returns an array of ProductInspirationTranslation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductInspirationTranslation
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
