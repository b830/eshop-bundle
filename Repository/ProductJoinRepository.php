<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\ProductAccessory;
use App\Akip\EshopBundle\Entity\ProductInspiration;
use App\Akip\EshopBundle\Entity\ProductJoin;
use App\Akip\EshopBundle\Entity\ProductRelated;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductJoin|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductJoin|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductJoin[]    findAll()
 * @method ProductJoin[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductJoinRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductJoin::class);
    }

}
