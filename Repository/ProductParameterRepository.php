<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\Parameter;
use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\ProductParameter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\Persistence\ManagerRegistry;
use http\Exception\UnexpectedValueException;

/**
 * @method ProductParameter|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductParameter|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductParameter[]    findAll()
 * @method ProductParameter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductParameterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductParameter::class);
    }

    public function findByProductAndParameter(Product $product, Parameter $parameter)
    {
        try {

            $items = $this->findBy(['product' => $product, 'parameter' => $parameter], ['sort' => 'ASC']);
            if (count($items) === 1) {
                $data = $this->findOneBy(['product' => $product, 'parameter' => $parameter]);
                return $this->buildOneValue($data);
            } elseif (count($items) > 1) {
                return $this->build($items);
            }
        } catch (EntityNotFoundException $e) {

        }

//        return $items;
    }

    public function build($data)
    {
        $valueItems = array();
        foreach ($data as $item) {
            if ($item->getValues() === null) {
                continue;
            }
            array_push($valueItems, $item->getValues());
        }
        return [
            'sort' => $data[0]->getSort(),
            'parameterId' => $data[0]->getParameter()->getId(),
            'name' => $data[0]->getName(),
            'values' => $valueItems,
            'type' => $data[0]->getType(),
            'translations' => $data[0]->getTranslations(),
            'valuesList' => $data[0]->getValuesList()
        ];
    }

    public function buildOneValue($data)
    {
        $valueItems = $data->getValues() === null ? [] : [$data->getValues()];

        return [
            'sort' => $data->getSort(),
            'parameterId' => $data->getParameter()->getId(),
            'name' => $data->getName(),
            'values' => $valueItems,
            'type' => $data->getType(),
            'translations' => $data->getTranslations(),
            'valuesList' => $data->getValuesList()
        ];
    }

    public function getAllProductParams(Product $product)
    {
        return $this->createQueryBuilder('productParameter')
            ->select(['productParameter', 'product' ,'parameter', 'parameterTranslations', 'parameterValue', 'parameterValueTranslations', 'GROUP_CONCAT(parameterValue.name ORDER BY parameterValue.name) as values'])
            ->leftJoin('productParameter.product', 'product')
            ->leftJoin('productParameter.parameter', 'parameter')
            ->leftJoin('parameter.translations', 'parameterTranslations')
            ->leftJoin('productParameter.parameterValue', 'parameterValue')
            ->leftJoin('parameterValue.translations', 'parameterValueTranslations')
            ->where('parameter.deletedAt is null')
            ->andWhere('product = :product')
            ->andWhere('parameterValue.deletedAt is null')
            ->groupBy('parameter.id')
            ->orderBy('productParameter.sort')
            ->setParameter('product', $product)
            ->getQuery()->getResult();
    }

    // /**
    //  * @return ProductParameter[] Returns an array of ProductParameter objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductParameter
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
