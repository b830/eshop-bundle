<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\ProductPhoto;
use App\Akip\FileManagerBundle\Repository\FileRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\File;

/**
 * @method ProductPhoto|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductPhoto|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductPhoto[]    findAll()
 * @method ProductPhoto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductPhotoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductPhoto::class);
    }


    public function build(FileRepository $fileRepository, Product $product)
    {
        $photos = $this->findBy(['product' => $product], ['sort' => 'ASC']);
        $data = [];
        foreach ($photos as $photo) {

            $data[] = [
                'id' => $photo->getId(),
                'main' => $photo->getMain(),
                "sort" => $photo->getSort(),
                'uuid' => $photo->getPhoto() ? $photo->getPhoto()->getUuid() : '',
                "originalName" =>  $photo->getPhoto() ? $photo->getPhoto()->getOriginalName() : '',
                "originalSuffix" =>  $photo->getPhoto() ? $photo->getPhoto()->getOriginalSuffix() : '',
                "name" =>  $photo->getPhoto() ? $photo->getPhoto()->getName() : '',
                "type" =>  $photo->getPhoto() ? $photo->getPhoto()->getType() : '',
                "size" =>  $photo->getPhoto() ? $photo->getPhoto()->getSize() : '',
                "suffix" =>  $photo->getPhoto() ? $photo->getPhoto()->getSuffix() : '',
                "children" =>  $photo->getPhoto() ? $photo->getPhoto()->getChildren() : '',
                'url' =>  $photo->getPhoto() ? $photo->getPhoto()->getUrl() : '',
                'translations' => $photo->getTranslations()
            ];
        }
        return $data;
    }

    // /**
    //  * @return ProductPhoto[] Returns an array of ProductPhoto objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductPhoto
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
