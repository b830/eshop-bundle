<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\ProductAccessory;
use App\Akip\EshopBundle\Entity\ProductInspiration;
use App\Akip\EshopBundle\Entity\ProductRelated;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductRelated|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductRelated|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductRelated[]    findAll()
 * @method ProductRelated[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRelatedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductRelated::class);
    }

    public function build($product)
    {
        $data = $this->findBy(['product' => $product], ['sort' => 'ASC']);
        $related = [];
        foreach ($data as $item) {
            $related[] = [
                'id' => $item->getRelatedId(),
                "name" => $item->getRelated()->getName(),
                "sort" => $item->getSort(),
                "display" => $item->getRelated()->getDisplay(),
                'url' => $item->getRelated()->getMainPhoto() ? $item->getRelated()->getMainPhoto()->getPhoto()->getChildren()[1]->getUrl() : 'https://i.redd.it/s8lk86v3r2m11.png'
            ];
        }
        return $related;
    }

    public function findByProduct(Product $product)
    {
        return $this->createQueryBuilder('related')
            ->select(['related', 'product','_product', 'variant', 'price', 'currency', 'translations', 'productPhoto', 'variantPhoto', 'productFile', 'variantFile', 'productFlags', 'variantFlags'
                , 'productFlagsTranslations', 'variantFlagsTranslations'])
            ->leftJoin('related.product', '_product')
            ->leftJoin('related.related', 'product')
            ->leftJoin('product.productVariants', 'variant')
            ->leftJoin('variant.price', 'price')
            ->leftJoin('price.currency', 'currency')
            ->leftJoin('product.translations', 'translations')
            ->leftJoin('product.photos', 'productPhoto')
            ->leftJoin('variant.photos', 'variantPhoto')
            ->leftJoin('productPhoto.photo', 'productFile')
            ->leftJoin('variantPhoto.photo', 'variantFile')
            ->leftJoin('product.flags', 'productFlags')
            ->leftJoin('variant.flags', 'variantFlags')
            ->leftJoin('productFlags.translations', 'productFlagsTranslations')
            ->leftJoin('variantFlags.translations', 'variantFlagsTranslations')
            ->where('variant.main = true')
            ->andWhere('related.product = :product')
            ->andWhere('product.enabled = true')
            ->setParameter('product', $product)
            ->orderBy('related.sort')
            ->getQuery()->getResult();

    }

    // /**
    //  * @return ProductInspiration[] Returns an array of ProductInspiration objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductInspiration
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
