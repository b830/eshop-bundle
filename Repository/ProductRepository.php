<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\Brand;
use App\Akip\EshopBundle\Entity\Category;
use App\Akip\EshopBundle\Entity\Flag;
use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\StockStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends BaseEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    private function buildQuery($filter, $ids) {
        $productsInCategory = $this->createQueryBuilder('product')
            ->select([
                'product', 'variants', 'pr', 'currency', 'productPhotos', 'variantsPhotos', 'brand', 'productFlags', 'variantFlags', 'productTranslations', 'variantsTranslations',
//                'productParameters', 'variantParameters',
                'children', 'photo',
                'mainVariant', 'mainPrice' ,'mainVat'
            ]);
        $this->addCalculatedPriceVariants($productsInCategory);
        $productsInCategory
            ->innerJoin('product.productVariants', 'variants')
            ->innerJoin('product.productVariants', 'mainVariant')
            ->innerJoin('product.itemsCategory', 'pc')
            ->leftJoin('variants.price', 'pr')
            ->leftJoin('mainVariant.price', 'mainPrice')
            ->leftJoin('product.photos', 'productPhotos')
            ->leftJoin('productPhotos.photo', 'photo')
            ->leftJoin('photo.children', 'children')
            ->leftJoin('variants.photos', 'variantsPhotos')
            ->leftJoin('pr.currency', 'currency')
            ->leftJoin('product.brand', 'brand')
            ->leftJoin('variants.flags', 'variantFlags')
            ->leftJoin('product.flags', 'productFlags')
            ->leftJoin('product.translations', 'productTranslations')
            ->leftJoin('variants.translations', 'variantsTranslations')
//            ->leftJoin('product.parameters', 'productParameters')
//            ->leftJoin('variants.parameters', 'variantParameters')
            ->leftJoin('mainPrice.vat2', 'mainVat')
            ->where('product.enabled = true')
            ->andWhere('variants.enabled = true')
            ->andWhere('mainVariant.main = true');
        if ($ids !== null && (is_array($ids))) {
            $strIds = implode(',', $ids);
            $productsInCategory
                ->andWhere('product.id IN (:ids)')
                ->setParameter('ids', $ids);
            // Add the new orderBy clause specifying the order
            if ($strIds !== '') {
                $productsInCategory->orderBy("FIELD(product.id,{$strIds})");
            }
        }

        return $productsInCategory;
    }

    public function getQueryByCategory(Category $category = null, $ids = null)
    {
        $productsInCategory = $this->buildQuery($category, $ids);
        if (!$category && $ids === null) {
            $productsInCategory->addSelect('(CASE WHEN product.sort = 0 THEN 2147483647 ELSE product.sort END) as HIDDEN ordCol');
            $productsInCategory->orderBy('ordCol, product.sort');
        }
        if ($category && !$ids) {
            $productsInCategory->addSelect('(CASE WHEN pc.sort = 0 THEN 2147483647 ELSE pc.sort END) as HIDDEN ordCol2');
            $productsInCategory->orderBy('ordCol2, pc.sort');
            $productsInCategory->andWhere('pc.category = :category')
                ->setParameter('category', $category);
        }


        return $productsInCategory;
    }

    private function buildMaxPriceQuery() {
        $query = $this->createQueryBuilder('product')
            ->select([
                'MAX(CASE
                    WHEN mainPrice.actualPrice > 0 AND mainPrice.vat = 0
                        THEN
                        CASE 
                            WHEN mainVat.rate = 1
                                THEN mainPrice.actualPrice 
                            ELSE 
                                mainPrice.actualPrice * ((1+mainVat.rate))
                        END
                    WHEN mainPrice.actualPrice > 0 AND mainPrice.vat = 1
                        THEN
                        CASE 
                            WHEN mainVat.rate = 1
                                THEN mainPrice.actualPrice 
                            ELSE 
                                mainPrice.actualPrice
                        END
                    WHEN mainPrice.vat = 0
                        THEN
                        CASE 
                            WHEN mainVat.rate = 1
                                THEN mainPrice.price
                            ELSE
                                mainPrice.price * ((1+mainVat.rate))
                        END
                    WHEN mainPrice.vat = 1
                        THEN
                        CASE 
                            WHEN mainVat.rate = 1
                                THEN mainPrice.price
                            ELSE
                                mainPrice.price
                        END
                    ELSE 
                        -1
                END)
                AS price'
            ]);
        $query
            ->innerJoin('product.productVariants', 'variants')
            ->innerJoin('product.productVariants', 'mainVariant')
            ->innerJoin('product.itemsCategory', 'pc')
            ->leftJoin('product.brand', 'brand')
            ->leftJoin('variants.price', 'pr')
            ->leftJoin('mainVariant.price', 'mainPrice')
            ->leftJoin('pr.currency', 'currency')
            ->leftJoin('mainPrice.vat2', 'mainVat')
            ->where('product.enabled = true')
            ->andWhere('variants.enabled = true')
            ->andWhere('mainVariant.main = true');
        return $query;
    }

    public function getMaxPriceInBrand(Brand $brand) {
        $query = $this->buildMaxPriceQuery();

        if ($brand) {
            $query->andWhere('product.brand = :brand')
                ->setParameter('brand', $brand);
        }
        return $query->getQuery()->getOneOrNullResult()['price'];
    }

    public function getMaxPriceInCategory(Category  $category = null) {
        $query = $this->buildMaxPriceQuery();

        if ($category) {
            $query->andWhere('pc.category = :category')
                ->setParameter('category', $category);
        }
        return $query->getQuery()->getOneOrNullResult()['price'];
    }

    public function getQueryByBrand(Brand $brand = null, $searchQuery = null)
    {
        $productsInCategory = $this->buildQuery($brand, $searchQuery);

        if ($brand && !$searchQuery) {
            $productsInCategory->andWhere('product.brand = :brand')
                ->setParameter('brand', $brand);
        }


        return $productsInCategory;
    }

    /**
     * @param QueryBuilder $query
     */
    private function addCalculatedPrice(QueryBuilder &$query) {
        return  $query
            ->addSelect('CASE
                    WHEN mainPrice.actualPrice > 0 AND mainPrice.vat = 0
                        THEN
                        CASE 
                            WHEN mainVat.rate = 1
                                THEN mainPrice.actualPrice 
                            ELSE 
                                mainPrice.actualPrice * ((1+mainVat.rate))
                        END
                    WHEN mainPrice.actualPrice > 0 AND mainPrice.vat = 1
                        THEN
                        CASE 
                            WHEN mainVat.rate = 1
                                THEN mainPrice.actualPrice 
                            ELSE 
                                mainPrice.actualPrice
                        END
                    WHEN mainPrice.vat = 0
                        THEN
                        CASE 
                            WHEN mainVat.rate = 1
                                THEN mainPrice.price
                            ELSE
                                mainPrice.price * ((1+mainVat.rate))
                        END
                    WHEN mainPrice.vat = 1
                        THEN
                        CASE 
                            WHEN mainVat.rate = 1
                                THEN mainPrice.price
                            ELSE
                                mainPrice.price
                        END
                    ELSE 
                        -1
                END
                AS HIDDEN orderPrice');
    }

    /**
     * @param QueryBuilder $query
     */
    private function addCalculatedPriceVariants(QueryBuilder &$query) {
        return  $query
            ->addSelect('CASE
                    WHEN pr.actualPrice > 0 AND pr.vat = 0
                        THEN
                        CASE 
                            WHEN mainVat.rate = 1
                                THEN pr.actualPrice 
                            ELSE 
                                pr.actualPrice * ((1+mainVat.rate))
                        END
                    WHEN pr.actualPrice > 0 AND pr.vat = 1
                        THEN
                        CASE 
                            WHEN mainVat.rate = 1
                                THEN pr.actualPrice 
                            ELSE 
                                pr.actualPrice
                        END
                    WHEN pr.vat = 0
                        THEN
                        CASE 
                            WHEN mainVat.rate = 1
                                THEN pr.price
                            ELSE
                                pr.price * ((1+mainVat.rate))
                        END
                    WHEN pr.vat = 1
                        THEN
                        CASE 
                            WHEN mainVat.rate = 1
                                THEN pr.price
                            ELSE
                                pr.price
                        END
                    ELSE 
                        -1
                END
                AS HIDDEN orderPrice');
    }

    /**
     * @param array $filters
     * @param QueryBuilder $query
     * @return mixed
     */
    public function applyFilters($filters, &$query)
    {
        if (isset($filters['priceFrom'])) {
            $query
                ->andHaving(
                    'orderPrice >= :priceFrom'
                )
                ->setParameter('priceFrom', $filters['priceFrom']);
        }
        if (isset($filters['priceTo'])) {
            $query
                ->andHaving(
                    'orderPrice <= :priceTo'
                )
                ->setParameter('priceTo', $filters['priceTo']);
        }
        if (isset($filters['size']) && $filters['size'] !== '' && (!isset($filters['color']) || count($filters['color']) < 1)) {
            $sizeProducts = $this->createQueryBuilder('product')
                ->select('variants.id')
                ->leftJoin('product.productVariants', 'variants')
                ->leftJoin('product.parameters', 'productParameters')
                ->leftJoin('variants.parameters', 'variantParameters')
                ->where('((variantParameters.parameterValue IN (:size)) or (productParameters.parameterValue IN (:size)))')
                ->setParameter('size', $filters['size']);

            if (isset($filters['inStock'])) {
                $sizeProducts
                    ->leftJoin('variants.actualStockStatus', 'actualStockStatus')
                    ->andWhere('actualStockStatus.slug = :slug and variants.inStock > 0')
                    ->setParameter('slug', StockStatus::IN_STOCK);
            }
            $sizeProducts = $sizeProducts->getQuery()->getResult();
            $query->andWhere('variants.id in (:sizes)')
                ->setParameter('sizes', $sizeProducts);
        }
        if (isset($filters['inStock'])) {
            $inStockVariants = $this->createQueryBuilder('product')
                ->select('product.id')
                ->leftJoin('product.productVariants', 'variants')
                ->leftJoin('variants.actualStockStatus', 'actualStockStatus')
                ->where('(actualStockStatus.slug = :slug)')
                ->setParameter('slug', StockStatus::IN_STOCK)
                ->getQuery()->getResult();
            $query
                ->andWhere('product in (:inStockProducts)')
                ->setParameter('inStockProducts', $inStockVariants);
        }
        if (isset($filters['sale'])) {
            $filters['sale'] = (boolean)$filters['sale'];
            $query->andWhere('(mainPrice.actualPrice < mainPrice.price and mainPrice.actualPrice <> 0)');
        }
        if (isset($filters['new'])) {
            $query->andWhere('(productFlags.slug = :flagSlug or variantFlags.slug = :flagSlug)')
                ->setParameter('flagSlug', Flag::NEW);
        }
        if (isset($filters['type']) && count($filters['type']) > 0) {
            $query->andWhere('product in (:type)')
                ->setParameter('type',
                    $this->applyParameterFilter($filters['type'], isset($filters['inStock']))
                );
        }
        if (isset($filters['season']) && count($filters['season']) > 0) {
            $query->andWhere('product in (:season)')
                ->setParameter('season',
                    $this->applyParameterFilter($filters['season'], isset($filters['inStock']))
                );
        }
        if (isset($filters['druh']) && count($filters['druh']) > 0) {
            $query->andWhere('product in (:druh)')
                ->setParameter('druh',
                    $this->applyParameterFilter($filters['druh'], isset($filters['inStock']))
                );
        }
        if (isset($filters['brands']) && count($filters['brands']) > 0) {
            $query->andWhere('brand IN (:brands)')
                ->setParameter('brands', $filters['brands']);
        }
        if (isset($filters['color']) && count($filters['color']) > 0 && (!isset($filters['size']) || $filters['size'] === '')) {
            $query->andWhere('product in (:colors)')
                ->setParameter('colors',
                    $this->applyParameterFilter($filters['color'], isset($filters['inStock']))
                );
        }
        if (isset($filters['size']) && $filters['size'] !== '' && isset($filters['color']) && count($filters['color']) > 0) {
            $colorAndSize = array_merge([$filters['size']], $filters['color']);

            $variants = $this->createQueryBuilder('product')
                ->leftJoin('product.productVariants', 'variant')
                ->leftJoin('variant.parameters', 'variantParameters')
                ->select(['COUNT(variantParameters.productVariant) as HIDDEN cnt', 'variant.id', 'product.id'])
                ->where('variantParameters.parameterValue in (:params)')
                ->setParameter('params', $colorAndSize)
                ->groupBy('variantParameters.productVariant')
                ->having('cnt > 1')
                ->getQuery()->getResult();

            $products = $this->createQueryBuilder('product')
                ->leftJoin('product.parameters', 'productParameters')
                ->select(['COUNT(productParameters.product) as HIDDEN cnt', 'product.id'])
                ->where('productParameters.parameterValue in (:params)')
                ->setParameter('params', $colorAndSize)
                ->groupBy('productParameters.product')
                ->having('cnt > 1')
                ->getQuery()->getResult();
            $productsIds = array_merge($products, $variants);

            if (count($productsIds) < 1) {
                $variants = $this->createQueryBuilder('product')
                    ->select('variants.id')
                    ->leftJoin('product.productVariants', 'variants')
                    ->leftJoin('product.parameters', 'productParameters')
                    ->leftJoin('variants.parameters', 'variantParameters')
                    ->where('((variantParameters.parameterValue IN (:size)) or (productParameters.parameterValue IN (:size)))')
                    ->setParameter('size', $filters['size'])
                    ->getQuery()->getResult();

                $products = $this->createQueryBuilder('product')
                    ->select('product.id')
                    ->leftJoin('product.parameters', 'productParameters')
                    ->where('productParameters.parameterValue in (:params)')
                    ->setParameter('params', $filters['color'])
//                    ->groupBy('productParameters.product')
//                    ->having('cnt > 1')
                    ->getQuery()->getResult();

//                exit(var_dump($products));
                $query->andWhere('variants.id in (:variantsIds)')
                    ->setParameter('variantsIds', $variants);

                $query->andWhere('product.id in (:productsIds)')
                    ->setParameter('productsIds', $products);

            } else {
                $query->andWhere('variants.id in (:variatsId)')
                    ->setParameter('variatsId', $productsIds);
            }
        }
        return $query;
    }

    public function getVisibleOnHomepage()
    {
        return $this->createQueryBuilder('product')
            ->select([
                'product', 'variants', 'price', 'productPhotos', 'variantsPhotos', 'currency', 'productFlags', 'variantFlags',
                'children', 'photo',
            ])
            ->innerJoin('product.productVariants', 'variants')
            ->innerJoin('product.itemsCategory', 'pc')
            ->innerJoin('variants.price', 'price')
            ->leftJoin('product.photos', 'productPhotos')
            ->leftJoin('productPhotos.photo', 'photo')
            ->leftJoin('photo.children', 'children')
            ->leftJoin('variants.photos', 'variantsPhotos')
            ->leftJoin('price.currency', 'currency')
            ->leftJoin('variants.flags', 'variantFlags')
            ->leftJoin('product.flags', 'productFlags')
            ->where('variants.main = true')
            ->andWhere('product.displayOnHomepage = true')
            ->andWhere('product.enabled = true')
            ->orderBy('product.homePageSort', 'ASC')
            ->getQuery()
            ->execute();
    }

    public function findBySlug(string $slug)
    {
        return $this->createQueryBuilder('product')
            ->select(['product', 'variants', 'translations',
                'variantTranslations', 'price', 'brand', 'page',
                'actualStockStatus', 'outOfStockStatus', 'productPhotos',
                'variantPhotos', 'currency', 'productParameters', 'variantsParameters',
                'stockTranslations', 'outOfStockTranslations'
            ])
            ->join('product.translations', 'translations')
            ->join('product.productVariants', 'variants')
            ->join('variants.translations', 'variantTranslations')
            ->join('variants.price', 'price')
            ->join('price.currency', 'currency')
            ->leftJoin('product.brand', 'brand')
            ->leftJoin('product.page', 'page')
            ->leftJoin('variants.actualStockStatus', 'actualStockStatus')
            ->leftJoin('actualStockStatus.translations', 'stockTranslations')
            ->leftJoin('variants.outOfStockStatus', 'outOfStockStatus')
            ->leftJoin('outOfStockStatus.translations', 'outOfStockTranslations')
            ->leftJoin('product.photos', 'productPhotos')
            ->leftJoin('variants.photos', 'variantPhotos')
            ->leftJoin('product.parameters', 'productParameters')
            ->leftJoin('variants.parameters', 'variantsParameters')
            ->where('product.enabled = true')
            ->andWhere('variants.enabled = true')
            ->andWhere('translations.slug = :slug')
            ->setParameter('slug', $slug)
            ->orderBy('variants.sort', 'ASC')
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByIdAndLocale($id, $locale) {
        return $this->createQueryBuilder('product')
            ->select('product', 'translation')
            ->leftJoin('product.translations', 'translation')
            ->where('product.id = :id')
            ->andWhere('translation.locale = :locale')
            ->setParameters(
                ['id' => $id, 'locale' => $locale]
            )
            ->getQuery()->getOneOrNullResult();
    }

    private function applyParameterFilter($parameterFilter, $inStock = false ) {
        $products = $this->createQueryBuilder('product')
            ->leftJoin('product.productVariants', 'variants')
            ->leftJoin('product.parameters', 'productParameters')
            ->leftJoin('variants.parameters', 'variantParameters')
            ->where('((variantParameters.parameterValue IN (:value)) or (productParameters.parameterValue IN (:value)))')
            ->setParameter('value', $parameterFilter);

        if ($inStock) {
            $products
                ->leftJoin('variants.actualStockStatus', 'actualStockStatus')
                ->andWhere('actualStockStatus.slug = :slug and variants.inStock > 0')
                ->setParameter('slug', StockStatus::IN_STOCK);
        }
        return $products->getQuery()->getResult();
    }
    public static function searchedColumns(): array
    {
        return ['name'];
    }
}
