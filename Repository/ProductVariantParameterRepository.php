<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\Parameter;
use App\Akip\EshopBundle\Entity\ParameterValue;
use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\ProductVariant;
use App\Akip\EshopBundle\Entity\ProductVariantParameter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductVariantParameter|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductVariantParameter|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductVariantParameter[]    findAll()
 * @method ProductVariantParameter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductVariantParameterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductVariantParameter::class);
    }

    public function findByProductVariantAndParameter(ProductVariant $variant, Parameter $parameter, $build = true)
    {
        $items = $this->findBy(['productVariant' => $variant, 'parameter' => $parameter], ['sort' => 'ASC']);
        if ($build) {
            if (count($items) === 1) {
                $data = $this->findOneBy(['productVariant' => $variant, 'parameter' => $parameter]);
                return $this->buildOneValue($data);
//            return $this->findOneBy(['productVariant' => $variant, 'parameter' => $parameter]);
            } elseif (count($items) > 1) {
                return $this->build($items);
            } else {
                return null;
            }
        } else {
            return $items;
        }
//        return $items;
    }

    public function findByVariant($variants, $parameter) {
        return $this->createQueryBuilder('variantParams')
            ->select('variantParams')
            ->where('variantParams.parameter = :parameter')
            ->andWhere('variantParams.productVariant in (:variants)')
            ->setParameter('parameter', $parameter)
            ->setParameter('variants', $variants)
            ->distinct()
            ->groupBy('variantParams.parameterValue')
            ->getQuery()
            ->getResult();
    }

    public function build($data)
    {
        foreach ($data as $item) {
            if ($item->getValues() === null) {
                continue;
            }
            $valueItems[] = $item->getValues();
        }
        return [
            'sort' => $data[0]->getSort(),
            'parameterId' => $data[0]->getParameter()->getId(),
            'name' => $data[0]->getName(),
            'values' => $valueItems,
            'type' => $data[0]->getType(),
            'translations' => $data[0]->getTranslations(),
            'valuesList' => $data[0]->getValuesList(),
            'required' => $data[0]->getRequired()
        ];
    }

    public function buildOneValue($data)
    {
        $valueItems = [$data->getValues()];
        return [
            'sort' => $data->getSort(),
            'parameterId' => $data->getParameter()->getId(),
            'name' => $data->getName(),
            'values' => $valueItems,
            'type' => $data->getType(),
            'translations' => $data->getTranslations(),
            'valuesList' => $data->getValuesList(),
            'required' => $data->getRequired()
        ];
    }

    public function findAllVariantParameters(Product $product, Parameter $parameter, ProductVariant $productVariant, ParameterValue $parameterValue)
    {
        return $this->createQueryBuilder('productVariantParameter')
            ->select(['productVariantParameter'])
            ->innerJoin('productVariantParameter.productVariant', 'productVariant')
            ->where('productVariant.product = :product')
            ->andWhere('productVariantParameter.parameter = :parameter')
            ->andWhere('productVariant <> :variant')
            ->andWhere('productVariantParameter.parameterValue = :value')
            ->setParameters(['product' => $product, 'parameter' => $parameter, 'variant' => $productVariant, 'value' => $parameterValue])
            ->getQuery()->execute();
    }

    public function findAllVariantParametersByProductAndVariant(Product $product, Parameter $parameter, ProductVariant $productVariant = null)
    {
        $query = $this->createQueryBuilder('productVariantParameter')
            ->select(['productVariantParameter'])
            ->innerJoin('productVariantParameter.productVariant', 'productVariant')
            ->where('productVariant.product = :product')
            ->andWhere('productVariantParameter.parameter = :parameter')
            ->setParameters(['product' => $product, 'parameter' => $parameter]);
        if ($productVariant !== null) {
            $query
                ->andWhere('productVariant <> :variant')
                ->setParameter('variant', $productVariant);
        }
        return $query->getQuery()->execute();
    }

    public function findAllParametersInProductByParamSlug(Product $product, $parameterSlug, $distinct = false)
    {
        $query = $this->createQueryBuilder('productVariantParameter')
            ->select(['productVariantParameter', 'parameter', 'parameterTranslations', 'parameterValue'])
            ->innerJoin('productVariantParameter.parameterValue', 'parameterValue')
            ->innerJoin('productVariantParameter.productVariant', 'productVariant')
            ->innerJoin('productVariantParameter.parameter', 'parameter')
            ->innerJoin('parameter.translations', 'parameterTranslations')
            ->where('productVariant.product = :product')
            ->andWhere('parameterTranslations.slug = :parameterSlug')
            ->setParameters(['product' => $product, 'parameterSlug' => $parameterSlug])
            ->orderBy('parameterValue.name');
        if ($distinct) {
            $query->addSelect('productVariant');
            $query->addSelect('GROUP_CONCAT(productVariant.id) as variantId');
            $query->groupBy('parameterValue.id');
        }
        return $query->getQuery()->getResult();
    }

    public function getAllProductParams(Product $product)
    {
        return $this->createQueryBuilder('productVariantParameter')
            ->select(['productVariantParameter', 'product', 'productVariant' ,'parameter', 'parameterTranslations', 'parameterValue', 'parameterValueTranslations', 'GROUP_CONCAT(parameterValue.name ORDER BY parameterValue.name) as values'])
            ->leftJoin('productVariantParameter.parameter', 'parameter')
            ->leftJoin('parameter.translations', 'parameterTranslations')
            ->leftJoin('productVariantParameter.parameterValue', 'parameterValue')
            ->leftJoin('parameterValue.translations', 'parameterValueTranslations')
            ->leftJoin('productVariantParameter.productVariant', 'productVariant')
            ->leftJoin('productVariant.product', 'product')
            ->where('parameter.deletedAt is null')
            ->andWhere('product = :product')
            ->andWhere('parameterValue.deletedAt is null')
            ->groupBy('parameter.id')
            ->orderBy('productVariantParameter.sort')
            ->setParameter('product', $product)
            ->getQuery()->getResult();
    }

    // /**
    //  * @return ProductVariantParameter[] Returns an array of ProductVariantParameter objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductVariantParameter
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
