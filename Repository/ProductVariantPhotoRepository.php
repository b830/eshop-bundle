<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\ProductVariant;
use App\Akip\EshopBundle\Entity\ProductVariantPhoto;
use App\Akip\FileManagerBundle\Repository\FileRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductVariantPhoto|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductVariantPhoto|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductVariantPhoto[]    findAll()
 * @method ProductVariantPhoto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductVariantPhotoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductVariantPhoto::class);
    }

    public function build(FileRepository $fileRepository, ProductVariant $productVariant)
    {
        $photos = $this->findBy(['productVariant' => $productVariant], ['sort' => 'ASC']);
        $data = [];
        foreach ($photos as $photo) {
            $data[] = [
                'id' => $photo->getId(),
                'main' => $photo->getMain(),
                "sort" => $photo->getSort(),
                'uuid' => $photo->getPhoto() ? $photo->getPhoto()->getUuid() : '',
                "originalName" =>  $photo->getPhoto() ? $photo->getPhoto()->getOriginalName() : '',
                "originalSuffix" =>  $photo->getPhoto() ? $photo->getPhoto()->getOriginalSuffix() : '',
                "name" =>  $photo->getPhoto() ? $photo->getPhoto()->getName() : '',
                "type" =>  $photo->getPhoto() ? $photo->getPhoto()->getType() : '',
                "size" =>  $photo->getPhoto() ? $photo->getPhoto()->getSize() : '',
                "suffix" =>  $photo->getPhoto() ? $photo->getPhoto()->getSuffix() : '',
                "children" =>  $photo->getPhoto() ? $photo->getPhoto()->getChildren() : '',
                'url' =>  $photo->getPhoto() ? $photo->getPhoto()->getUrl() : '',
                'translations' => $photo->getTranslations()
            ];
        }
        return $data;
    }

    // /**
    //  * @return ProductVariantPhoto[] Returns an array of ProductVariantPhoto objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductVariantPhoto
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
