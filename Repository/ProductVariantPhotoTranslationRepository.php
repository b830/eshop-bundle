<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\ProductVariantPhotoTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductVariantPhotoTranslation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductVariantPhotoTranslation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductVariantPhotoTranslation[]    findAll()
 * @method ProductVariantPhotoTranslation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductVariantPhotoTranslationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductVariantPhotoTranslation::class);
    }

    // /**
    //  * @return ProductVariantPhotoTranslation[] Returns an array of ProductVariantPhotoTranslation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductVariantPhotoTranslation
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
