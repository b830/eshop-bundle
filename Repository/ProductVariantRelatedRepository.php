<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\ProductAccessory;
use App\Akip\EshopBundle\Entity\ProductInspiration;
use App\Akip\EshopBundle\Entity\ProductRelated;
use App\Akip\EshopBundle\Entity\ProductVariantRelated;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductVariantRelated|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductVariantRelated|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductVariantRelated[]    findAll()
 * @method ProductVariantRelated[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductVariantRelatedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductVariantRelated::class);
    }

    public function build($variant)
    {
        $data = $this->findBy(['productVariant' => $variant], ['sort' => 'ASC']);
        $related = [];
        foreach ($data as $item) {
            $related[] = [
                'id' => $item->getRelatedId(),
                "name" => $item->getRelated()->getName(),
                "sort" => $item->getSort(),
                "display" => $item->getRelated()->getDisplay(),
                'url' => $item->getRelated()->getMainPhoto() ? $item->getRelated()->getMainPhoto()->getPhoto()->getChildren()[1]->getUrl() : 'https://i.redd.it/s8lk86v3r2m11.png',
            ];
        }
        return $related;
    }
    // /**
    //  * @return ProductInspiration[] Returns an array of ProductInspiration objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductInspiration
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
