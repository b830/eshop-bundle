<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\Category;
use App\Akip\EshopBundle\Entity\Parameter;
use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\ProductVariant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use App\Akip\CmsBundle\Repository\BaseEntityRepository;

/**
 * @method ProductVariant|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductVariant|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductVariant[]    findAll()
 * @method ProductVariant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductVariantRepository extends BaseEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductVariant::class);
    }

    public function getQueryByCategory(Category $category = null, $searchQuery = null)
    {
        $variantsInCategory = $this->createQueryBuilder('pv')
            ->select([
                'pv'
            ])
            ->innerJoin('pv.product', 'p')
            ->innerJoin('p.itemsCategory', 'pc')
            ->innerJoin('pv.price', 'pr');

        if ($category && !$searchQuery) {
            $variantsInCategory->where('pc.category = :category')
                ->setParameter('category', $category);
        }
        if ($searchQuery) {
            $searches = explode(' ', $searchQuery);
            $searches = array_filter($searches, function ($s) {
                return !!trim($s);
            });
            $fullSearch = '';
            foreach ($searches as $index => $item) {
                $fullSearch .= $item . ' ';
            }
            $fullSearch = rtrim($fullSearch, ' ');
            if($category) {
                $variantsInCategory->andWhere("CONCAT(p.name, ' ', pv.name) LIKE :q")
                    ->setParameter("q", "%" . $fullSearch . "%");
            } else {
                $variantsInCategory->where("CONCAT(p.name, ' ', pv.name) LIKE :q")
                    ->setParameter("q", "%" . $fullSearch . "%");
            }
            $criteria = Criteria::create();
            foreach ($searches as $i => $s) {
                $criteria->orWhere(Criteria::expr()->contains('pv.name', $s));
                $criteria->orWhere(Criteria::expr()->contains('p.name', $s));
            }
            $variantsInCategory->addCriteria($criteria);
        }
        

        return $variantsInCategory;
    }

    public function getVariantsIdsByCategory($category) {
        $variantsInCategory = $this->getQueryByCategory($category);
        return $variantsInCategory->select('pv.id')
            ->getQuery()
            ->getResult();
    }

    public function getProductVariantsByCategoryAndProduct(Category $category, Product $product, $limit = 10)
    {
        $variantsInCategory = $this->getQueryByCategory($category);
        $variantsInCategory->andWhere('p <> :product')
            ->setParameter('product', $product);
        $variantsInCategory->orderBy('pc.sort');
        $criteria = Criteria::create();
//        $criteria->setMaxResults($limit);
//        $variantsInCategory->addCriteria($criteria);
        return $variantsInCategory
            ->getQuery()
            ->getResult();
    }

    public function getSearch($searchQuery, $locale)
    {
        $strings = explode(' ', $searchQuery);
        /**
         * @var $qb QueryBuilder
         */
        $qb = $this->createQueryBuilder('variant');
        $qb->innerJoin('variant.product', 'product')
            ->leftJoin('variant.translations', 'vt')
            ->leftJoin('product.translations', 't')
            ->where('vt.locale = :loc')
            ->setParameter('loc', $locale);
        $qb->select("CONCAT(t.name, ' ', vt.name) AS fullName, variant.id");
        foreach ($strings as $key => $str) {
            $qb->orHaving("fullName LIKE :str{$key}")
                ->setParameter("str{$key}","%".$str."%");
        }
        return $qb->getQuery()->execute();

    }

    public function findMainVariant(Product $product) {
        return $this->createQueryBuilder('variant')
            ->select(['variant', 'flags', 'price', 'currency', 'translations'])
            ->leftJoin('variant.flags', 'flags')
            ->leftJoin('variant.price', 'price')
            ->leftJoin('price.currency', 'currency')
            ->leftJoin('variant.translations', 'translations')
            ->where('variant.product = :product')
            ->andWhere('variant.main = true')
            ->setParameter('product', $product)
            ->getQuery()->getOneOrNullResult();
    }

    // /**
    //  * @return ProductVariant[] Returns an array of ProductVariant objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductVariant
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public static function searchedColumns(): array
    {
        return [
            'name'
        ];
    }
}
