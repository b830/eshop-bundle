<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\FlagTranslation;
use App\Akip\EshopBundle\Entity\SignTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SignTranslation|null find($id, $lockMode = null, $lockVersion = null)
 * @method SignTranslation|null findOneBy(array $criteria, array $orderBy = null)
 * @method SignTranslation[]    findAll()
 * @method SignTranslation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SignTranslationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SignTranslation::class);
    }

    // /**
    //  * @return FlagTranslation[] Returns an array of FlagTranslation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FlagTranslation
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
