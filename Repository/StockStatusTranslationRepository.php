<?php

namespace App\Akip\EshopBundle\Repository;

use App\Akip\EshopBundle\Entity\StockStatusTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StockStatusTranslation|null find($id, $lockMode = null, $lockVersion = null)
 * @method StockStatusTranslation|null findOneBy(array $criteria, array $orderBy = null)
 * @method StockStatusTranslation[]    findAll()
 * @method StockStatusTranslation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StockStatusTranslationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StockStatusTranslation::class);
    }

    // /**
    //  * @return StockStatusTranslation[] Returns an array of StockStatusTranslation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StockStatusTranslation
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
